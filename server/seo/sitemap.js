'use strict';


var Pro = require('../api/user/pro/pro.model');
var Category = require('../api/category/category.model');
var Promise = require('bluebird');
var sm = require('sitemap');
var _ = require('lodash');

var locationsJson = require('./locations.json');


function convertToSlug(text) {
  return text
    .toLowerCase()
    .replace(/ /g,'-')
    .replace(/[^\w-]+/g,'');
};

function generateSitemap() {
  var sitemap = sm.createSitemap ({
    hostname: 'https://tasky.me',
    cacheTime: 3600000,        // 1 Hour - cache purge period
    urls: [
      { url: '/',  changefreq: 'daily', priority: 1.0 },
      { url: '/pros',  changefreq: 'daily', priority: 0.9 },
      { url: '/pros/about-credits',  changefreq: 'monthly', priority: 0.9 },
      { url: '/pros/credits',  changefreq: 'monthly', priority: 0.8 },
      { url: '/signup/customer',  changefreq: 'monthly', priority: 0.6 },
      { url: '/signup/pro/category',  changefreq: 'monthly', priority: 0.6 },
      { url: '/signup',  changefreq: 'monthly', priority: 0.6 },
      { url: '/login',  changefreq: 'monthly', priority: 0.6 }
    ]
  });





  return Pro.find({
    _accountType: 'Pro'
  }).lean().execAsync()
    .then(function(pros) {
      pros.forEach(function(pro) {
        var profileUrl = '/pro/' + pro._id;
        sitemap.add({url: profileUrl, changefreq: 'weekly', priority: 0.7});
      });


      return Category.getRootCategories()
        .then(function(categories) {
          categories = _.filter(categories, 'route');
          _.each(categories, function(category) {
            // For Each Province
            _.each(_.keys(locationsJson), function(province) {

              // Iterate over Cities
              _.each(locationsJson[province], function(city) {
                var citySlug = convertToSlug(city.name);
                sitemap.add({ url: '/' + province + '/' + citySlug + '/' + category.route,
                  changefreq: 'weekly', priority: 0.6
                });

              });
            });
          });

          return sitemap;
        });

    })
    .error(function(err) {
      console.error(err);
      throw new Error('Error while trying to generate sitemap');
    });
}


module.exports = function (app) {
  var sitemapPromise = generateSitemap();

  app.get('/sitemap.xml', function(req, res) {
    sitemapPromise.then( function(sitemap) {
      sitemap.toXML( function (xml) {
        res.header('Content-Type', 'application/xml');
        res.send(xml);
      });
    });
  });
};