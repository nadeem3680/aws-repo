/**
 * Setting up html snapshots for crawlers
 */

'use strict';

var config = require('../config/environment');

module.exports = function(app) {
  var prerender = require('prerender-node')
    .set('prerenderToken', config.prerender.token )
    .set('protocol', 'https');

  require('./sitemap')(app);
  prerender.blacklisted([
    '^/admin.*',
    '.*\?category=[0-9]*',
    '^/leads/.*',
    '^/settings.*',
    '^/welcome.*',
    '^/requests/.*',
    '^/pro/.*/?\?show_review_modal.*'
  ]);

  if (process.env.NODE_ENV == 'production') {
    app.use(prerender);
  }
};




function ZombieCrawl(req, res, next){
  var Browser = require('zombie');
  var fs = require('fs');
  var url = require('url');
  var frag = req.query._escaped_fragment_;
  var filename;

  if (typeof frag === 'undefined') {
    return next();
  }

  var _url = url.parse(req.url);
  var pathname = _url.pathname;

  if (pathname === "" || pathname === "/")
    filename = "/index.html";
  else
    filename =  pathname + '.html';

  // Serve the static html snapshot
  var file = __dirname + "/snapshots" + filename;
  fs.exists(file, function (exists) {
    if (!exists) {
      var hostname = req.protocol + '://' + req.host + ':'  + config.port;

      var browser = new Browser({
        loadCSS: true,
        waitFor: 5000
      });

      browser.visit( hostname  + pathname )
        .then(function() {
          var html = browser.html();
          fs.open(file, 'w', function(e, fd) {
            if (e) return;
            fs.write(fd, html);
          });
          res.send(html);
        });
    }
    else {
      res.sendfile(file);
    }
  });
}