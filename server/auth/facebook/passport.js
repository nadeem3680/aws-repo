var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

// IMPORTANT!
// This creates an account of type CUSTOMER
exports.setup = function (Customer, config) {
  passport.use(new FacebookStrategy({
      clientID: config.facebook.clientID,
      clientSecret: config.facebook.clientSecret,
      callbackURL: config.facebook.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
      Customer.findOne({
        'facebook.id': profile.id
      },
      function(err, customer) {
        if (err) {
          return done(err);
        }
        if (!customer) {
          customer = new Customer({
            full_name: profile.displayName,
            image: {
              url: "http://graph.facebook.com/" + profile.id + "/picture?type=square&width=200",
              thumb: "http://graph.facebook.com/"+profile.id+"/picture?type=square&width=100"
            },
            email: profile.emails[0].value,
            role: 'user',
            username: profile.username,
            provider: 'facebook',
            facebook: profile._json
          });
          customer.save(function(err) {
            if (err) {
              console.log('User %s trying to login with facebook existing email', profile.emails[0].value);
              done(null, false);
            }
            return done(err, customer);
          });
        } else {
          return done(err, customer);
        }
      })
    }
  ));
};