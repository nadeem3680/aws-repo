'use strict';

var _ = require('lodash');
var Payment = require('./payment.model');
var Pro = require('../user/pro/pro.model');
var envConfig = require('../../config/environment');
var Stripe = require("stripe")(envConfig.stripe.secret_key);

//console.log('Using string secret Key', envConfig.stripe.secret_key);

// In Cents
var AMOUNTS = {
  small: 1999,
  medium: 3499,
  large: 7999
};

var CREDITS ={
  small: 10,
  medium: 20,
  large: 50
};
// Creates a new payment via Stripe
exports.create = function(req, res) {
  var pack = req.body.pack,
    description = req.body.description,
    token = req.body.token,
    currency = 'cad',
    proId = req.body.proId;

  var charge = Stripe.charges.create({
      amount: AMOUNTS[pack.name],
      description: description,
      card: token,
      currency: currency
    },
    function(err, charge) {
      if (err) {
        console.error('Stripe Error:', err);
        return handleError(res, err);
      }

      Pro.findByIdAndUpdateAsync( proId, {
        $inc: {
          credits: CREDITS[pack.name]
        }
      })
        .then(function(pro){
          return res.status('200').json({ credits: pro.credits });
        },function(err){
          return handleError(res, err);
        })
  });
};

function handleError(res, err) {
  return res.send(500, err);
}