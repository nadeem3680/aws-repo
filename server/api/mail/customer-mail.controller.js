'use strict';

var _ = require('lodash');
var key = require('../../config/environment/index').mandrill.apiKey;
var Promise = require('bluebird');
var Category = require('../category/category.model');
var Quote = require('../quote/quote.model');
var User = require('../user/user.model');
var Pro = require('../user/pro/pro.model');
var hostname = 'http://localhost:9000';

if (process.env.NODE_ENV === 'production') { hostname = 'https://tasky.me' }
if (process.env.NODE_ENV === 'staging' ){ hostname = 'https://staging.tasky.me' }

var mandrill =require('node-mandrill')(key);

// Sends an email to customer when user receives a new quote
exports.newQuote = function (requestObj, quote, pro, res) {
  var requestingUser;
  User.findOneAsync({ _id: requestObj.requested_by })
    .then(
    function (user) {
      requestingUser = user;

      return Category.findOneAsync({ _id: requestObj.category })
    })
    .error(
    function(err) {
      return handleError(res, 'Failed while retreiving the category of request for [New Quote] email notifications');
    })
    .then (
    function (service) {
      // Only send quote if user preferences allow it
      if (!requestingUser.preferences.quote) {
        return;
      }
      var mergeVars = [{
        rcpt: requestingUser.email,
        vars: [
          {
            name: 'FIRST_NAME',
            content: requestingUser.firstName
          },
          {
            name: 'SERVICE_NAME',
            content: service.name
          },
          {
            name: 'LAST_NAME',
            content: requestingUser.lastName
          },
          {
            name: 'PRO_NAME',
            content: pro.name
          }
        ]
      }];

      mandrill('/messages/send-template', {
        template_name: 'customer-new-quote-notification',
        template_content: [],
        message: {
          to: [{
            name: requestingUser.firstName + ' ' + requestingUser.lastName,
            email: requestingUser.email
          }],
          preserve_recipients: false,

          from_email: 'notifications@tasky.me',
          subject: 'New quote received for ' + service.name + '!',

          track_opens: true,
          track_clicks: true,

          autotext: true,
          merge: true,
          merge_language: 'handlebars',
          merge_vars: mergeVars,


          tags: [
            'customer',
            'new-quote',
              'category-' + service.name,
              'location-' + requestObj.location.subLocality
          ]
        }
      }, function (err, response) {
        if (err) { throw new Error('Could not send notification email for new quote to customer' ) }
      });
    });
};


// Sends an email to customer when user receives a new quote
exports.sendQuoteReminder = function (r) {
  if (!r.requested_by.preferences.quote) {
    return Promise.resolve(false);
  }

  return Quote.populateAsync(r, {
    path: 'quotes.from',
    model: User,
    select: 'name firstName lastName'
  }).then( function(request) {
    var requestingUser = request.requested_by;
    var quotes = request.quotes;
    var category = request.category;

    var proNames = _.map(quotes, function(quote) {
      return quote.from._doc.name;
    });

    proNames = _.rest(proNames).join(', ') + " and " + _.first(proNames);

    var mergeVars = [{
      rcpt: requestingUser.email,
      vars: [
        {
          name: 'FIRST_NAME',
          content: requestingUser.firstName
        },
        {
          name: 'SERVICE_NAME',
          content: category.name
        },
        {
          name: 'LAST_NAME',
          content: requestingUser.lastName
        },
        {
          name: 'PROS_WHO_SENT_QUOTES',
          content: proNames
        },
        {
          name: 'VIEW_QUOTES_LINK',
          content: hostname + '/requests/' + request._id + '/bids/' + quotes[0]._id
        },
        {
          name: 'UPDATE_STATUS_LINK',
          content: hostname + '/requests/' + request._id  + '/close'
        }
      ]
    }];


    return new Promise(function(resolve,reject) {
      mandrill('/messages/send-template', {
        template_name: 'customer-reminder-email',
        template_content: [],
        message: {
          to: [{
            name: requestingUser.firstName + ' ' + requestingUser.lastName,
            email: requestingUser.email
          }],
          preserve_recipients: false,

          from_email: 'notifications@tasky.me',
          subject: 'Reminder: ' + proNames + ' are interested and available to do your project!',

          track_opens: true,
          track_clicks: true,

          autotext: true,
          merge: true,
          merge_language: 'handlebars',
          merge_vars: mergeVars,

          tags: [
            'customer',
            'quote-reminder',
              'category-' + category.name,
              'location-' + request.location.subLocality
          ]
        }
      }, function (err, response) {
        if (err) {
          return reject(new Error('Could not send notification email for new quote to customer' ));
        }
        return resolve(response);
      });

    });
  });
};



function handleError(res, err) {
  return res.send(500, err);
}