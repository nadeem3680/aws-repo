'use strict';

var _ = require('lodash');
var key = require('../../config/environment/index').mandrill.apiKey;
var mandrill = require('node-mandrill')(key);
var Category = require('../category/category.model');
var Promise = require('bluebird');

/**
 * Send email notif when pro receives new request
 *
 * @param requestObj: type Request or lean object
 * @param pros: Array of professionals to check and send email notifications to
 * @returns {!Promise.<boolean>}
 */
exports.newRequest = function (requestObj, pros) {
  _.remove(pros, function(pro){
    var doesntWantEmails = !pro.preferences.leads;
    var notActivated = !pro.status.active;
    return  notActivated || doesntWantEmails
  });

  // Isolate only the name and emails
  var mappedPros = _.map(pros, function (pro) {
    return {
      name: pro.firstName + ' ' + pro.lastName,
      email: pro.email
    }
  });

  if (!mappedPros.length) {
    console.warn('WARNING: request %s was email notified to 0 pros', requestObj._id);
    return Promise.resolve(false);
  }


  return Category.findOneAsync({ _id: requestObj.category })
    .then (function (service) {
    var mergeVars = _.map(pros, function (pro) {
      return {
        rcpt: pro.email,
        vars: [
          {
            name: 'FIRST_NAME',
            content: pro.firstName
          },
          {
            name: 'SERVICE_NAME',
            content: service.name
          },
          {
            name: 'LAST_NAME',
            content: pro.lastName
          },
          {
            name: 'LOCATION',
            content: requestObj.location.subLocality
          },
          {
            name: 'REQUEST_DETAILS',
            content: requestObj.asHumanString(true)
          }
        ]
      };
    });

    return new Promise(function(resolve, reject) {
      mandrill('/messages/send-template', {
          template_name: 'pro-notification-new-request',
          template_content: [],
          message: {
            to: mappedPros,
            preserve_recipients: false,

            from_email: 'notifications@tasky.me',
            subject: 'New lead for ' + service.name + '!',


            track_opens: true,
            track_clicks: true,

            autotext: true,
            merge: true,
            merge_language: 'handlebars',
            merge_vars: mergeVars,


            tags: [
              'pro',
              'new-request',
                'category-'+ service.route ,
                'location-' + requestObj.location.subLocality ,
              'subject-potential-customer'
            ]
          }
        }
        ,function(err, response) {
          if (err) { return reject(err); }
          resolve(response);
        });
    });
  });


};

// Exposed via /mail/pros/hired
// Sends a "you've been hired notification to pro's mail"
exports.hired = function(req, res) {
  var pro = req.body.pro;
  var request = req.body.request;

  if (!pro.preferences.hired) {
    return;
  }

  var mergeVars =  [{
    rcpt: pro.email,
    vars: [
      {
        name: 'FIRST_NAME',
        content: pro.firstName
      },
      {
        name: 'LAST_NAME',
        content: pro.lastName
      },
      {
        name: 'SERVICE_NAME',
        content: request.category.name
      }
    ]
  }];

  mandrill('/messages/send-template', {
      template_name: 'pro-hired-notification',
      template_content: [],
      message: {
        to: [{
          name: pro.firstName + ' ' + pro.lastName,
          email: pro.email
        }],
        preserve_recipients: true,

        from_email: 'notifications@tasky.me',
        subject: "You've just been hired on Tasky!",


        track_opens: true,
        track_clicks: true,

        autotext: true,
        merge: true,
        merge_language: 'handlebars',
        merge_vars: mergeVars,
        tags: [
          'pro',
          'hired'
        ]
      }
    }
    , function(err, response) {
      if (err) {
        return handleError(res, 'Error while sending notification emails to pro(s)');
      }
      res.status(200).send('Mail Sent');

    });

};


exports.welcome = function(pro) {

  var mergeVars =  [{
    rcpt: pro.email,
    vars: [
      {
        name: 'FIRST_NAME',
        content: pro.firstName
      },
      {
        name: 'LAST_NAME',
        content: pro.lastName
      }
    ]
  }];

  mandrill('/messages/send-template', {
      template_name: 'pro-welcome',
      template_content: [],
      message: {
        to: [{
          name: pro.firstName + ' ' + pro.lastName,
          email: pro.email
        }],
        preserve_recipients: true,

        from_email: 'noreply@tasky.me',
        subject: "Welcome to Tasky!",


        track_opens: true,
        track_clicks: true,

        autotext: true,
        merge: true,
        merge_language: 'handlebars',
        merge_vars: mergeVars,

        tags: [
          'pro',
          'welcome'
        ]
      }
    }
    ,function(err, response) {
      if (err) {
        console.error(err);
      }
    });

};



/**
 * Send email notif when pro gets a refund cause
 * customer hasn't viewed his quote in 48 hours
 *
 * @param requestObj: type Request:lean object (should be populated with category)
 * @param quote: the quote in question
 * @param pro: the pro to send the email to
 * @returns {!Promise.<boolean>}
 */
exports.refunded = function (requestObj, quote, pro) {
  // TODO: Add flag for email/text when refund
  // Isolate only the name and emails
  var mappedPro = {
    name: pro.firstName + ' ' + pro.lastName,
    email: pro.email
  };

  var category = requestObj.category;
  var mergeVars = [{
    rcpt: pro.email,
    vars: [
      {
        name: 'FIRST_NAME',
        content: pro.firstName
      },
      {
        name: 'LAST_NAME',
        content: pro.lastName
      },
      {
        name: 'CATEGORY',
        content: category.name
      },
      {
        name: 'NUM_CREDITS_REFUNDED',
        content: category.credits_required
      },
      {
        name: 'NEW_NUM_PRO_CREDITS',
        content: pro.credits
      }
    ]
  }];

  return new Promise(function(resolve, reject) {
    mandrill('/messages/send-template', {
        template_name: 'pro-refund-notification',
        template_content: [],
        message: {
          to: [mappedPro],
          preserve_recipients: false,

          from_email: 'notifications@tasky.me',
          subject: "You've been refunded for a quote you sent!",

          track_opens: true,
          track_clicks: true,

          autotext: true,
          merge: true,
          merge_language: 'handlebars',
          merge_vars: mergeVars,

          tags: [
            'pro',
            'refund',
              'category-'+ category.route ,
              'location-' + requestObj.location.subLocality
          ]
        }
      }
      ,function(err, response) {
        if (err) { return reject(err); }
        resolve(response);
      });
  });
};


/**
 * Send an activation email to pro who was activated in the backend
 * @param pro : Pro
 */
exports.activate = function(pro) {
  var mappedPro = {
    name: pro.firstName + ' ' + pro.lastName,
    email: pro.email
  };

  var mergeVars = [{
    rcpt: pro.email,
    vars: [
      {
        name: 'FIRST_NAME',
        content: pro.firstName
      },
      {
        name: 'LAST_NAME',
        content: pro.lastName
      }
    ]
  }];

  return new Promise(function(resolve, reject) {
    mandrill('/messages/send-template', {
        template_name: 'pro-activated-notification',
        template_content: [],
        message: {
          to: [mappedPro],
          preserve_recipients: false,

          from_email: 'notifications@tasky.me',
          subject: "Your Tasky account has been activated",

          track_opens: true,
          track_clicks: true,

          autotext: true,
          merge: true,
          merge_language: 'handlebars',
          merge_vars: mergeVars,

          tags: [
            'pro',
            'activation'
          ]
        }
      }
      ,function(err, response) {
        if (err) { return reject(err); }
        resolve(response);
      });
  });
};