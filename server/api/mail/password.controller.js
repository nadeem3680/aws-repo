'use strict';

var _ = require('lodash');
var config = require('../../config/environment');
var key = config.mandrill.apiKey;
var mandrill = require('node-mandrill')(key);
var User = require('../user/user.model');
var jwt = require('jsonwebtoken');

// Sends an email to EMAIL_TO for use in contact us section
exports.forgotPassword = function (req, res, next) {
  var email = req.body.email;


  User.findOneAsync({
    email: email
  })
    .then(function(user) {
      if (!user) {
        return res.status(404).send('Could not find user with email '+ email);
      }

      var token = jwt.sign({
        id: user._id
      }, config.secrets.session, { expiresInMinutes: 120 });


      var mergeVars = [
        {
          rcpt: email,
          vars: [
            {
              name: 'FIRST_NAME',
              content: user.firstName
            },
            {
              name: 'TOKEN',
              content: token
            }
          ]
        }
      ];


      mandrill('/messages/send-template', {
        template_name: 'password-reset',
        template_content: [],
        message: {
          to: [{
            email: email
          }],
          from_email: 'noreply@tasky.me',
          subject: 'Reset your password',
          autotext: true,
          merge: true,
          merge_language: 'handlebars',
          merge_vars: mergeVars,

          tags: [
            'password'
          ]
        }
      }, function(err, response)
      {

        //uh oh, there was an error
        if (err) return handleError(res, err);

        //everything's good, lets see what mandrill said
        else {
          return res.json(200).send('Password Successfully Sent');
        }
      });
    });
};


function handleError(res, err) {
  return res.send(500, err);
}