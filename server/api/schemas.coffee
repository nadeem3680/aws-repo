'use strict'

# This file is used for global mongoose-repl
# ran like mongoose --schemas PATH_TO_SCHEMAS CONNECTION_URL
module.exports =
  User: require('./user/user.model').schema
  Pro: require('./user/pro/pro.model').schema
  Customer: require('./user/customer/customer.model').schema
  Category: require('./category/category.model').schema
  Question: require('./question/question.model').schema
  Quote: require('./quote/quote.model').schema
  Request: require('./request/request.model').schema
  Review: require('./review/review.model').schema
  Payment: require('./payment/payment.model').schema