'use strict';

var _ = require('lodash');
var Quote = require('./quote.model');
var Category = require('../category/category.model');
var Request = require('../request/request.model');
var Pro = require('../user/pro/pro.model');
var custNotifier = require('../mail/customer-mail.controller');
var textNotifier = require('../text/text.controller');

// Get list of quotes
exports.index = function(req, res) {
  var from = req.query.from;
  var query = Quote.find();

  if (from) {
    query = query.where({
      from: from
    });
  }

  query
    .populate('from')
    .sort({sent_at: -1})
    .execAsync()
    .then(function (quotes) {
      return res.json(200, quotes);
    })
    .error(function(err) {
      return handleError(res, err);
    });
};

// Get a single quote
exports.show = function(req, res) {
  Quote.findById(req.params.id)
    .execAsync()
    .then(function (quote) {
      if(!quote) { return res.send(404); }
      return res.json(quote);
    })
    .error(function(err) {
      return handleError(res, err);
    });
};

// Gets the quote object based on the pro and request ID's
exports.getByRequestAndPro = function(req, res) {
  var proId = req.params.proId;
  var reqId = req.params.requestId;
  Quote.findOne({
      from: proId,
      request: reqId
    },
    function(err, quote) {
      if(err) { return handleError(res, err); }
      if(!quote) { return res.send(404); }
      return res.json(quote);
    });
};

// Creates a new quote in the DB.
exports.create = function(req, res) {
  // BLOG: Promise chain value management
  var categoryId = req.body.categoryId;
  var proId = req.body.from;
  var newCredits;

  // Get the number of credits required from Category ID
  var creditsRequiredPromise = Category.findByIdAsync(categoryId).then(function(cat){
    return cat.credits_required;
  });

  var proPromise = creditsRequiredPromise.then(function(credits_required) {
    // Deduct Amount from pro's credits
    return Pro.findByIdAsync(proId).then(function (pro) {
      newCredits = pro.credits -= credits_required;
      return pro.saveAsync();
    });
  });


  var quotePromise = proPromise.spread(function(pro, numAffected) {
    return Quote.createAsync(req.body);
  }).then( function(quote) {
      quote.last_message_seen[quote.from] = quote.messages[0]._id;
      quote.markModified('last_message_seen');
      return quote.saveAsync();
    });

  quotePromise.spread(function(quote, numAffected) {
    return Request.findOneAsync({ _id: quote.request })
  }).then(function (request) {
    request.quotes.addToSet(quotePromise.value()[0]._id);
    return request.saveAsync()
  })
    .spread(function (request, numAffected) {
      var quoteSent = quotePromise.value()[0];
      var pro = proPromise.value()[0];
      try {
        custNotifier.newQuote(request, quoteSent, pro , res);
        textNotifier.newQuote(pro, request.requested_by, request);
      }
      catch (e) {
        console.error('Caught Error ', e);
        return handleError(res, e);
      }

      return res.status(201).json({
        quote: quoteSent,
        request: request,
        credits: newCredits
      });
    })
    .error(function(err) { /* Catch-all Error handler for quote and request */
      return handleError(res, err);
    })
    .catch(function(err) {
      return handleError(res, err);
    });
};

// Updates an existing quote in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Quote.findById(req.params.id, function (err, quote) {
    if (err) { return handleError(res, err); }
    if(!quote) { return res.send(404); }
    var updated = _.extend(quote, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, quote);
    });
  });
};


exports.registerEvent = function(req, res) {
  var quoteId = req.params.id,
    addOnce = req.query.once,
    eventName = req.body.event,
    eventData = req.body.data;

  if (!quoteId || !eventName) { return res.status(400).send('Malformed event registration request.'); }

  Quote.findOneAsync({ _id: quoteId })
    .then(function(q) {
      var newEvent = {
        event: eventName,
        data: eventData
      };

      if (addOnce && addOnce == 'true') {
        var eventExists = _.any(q.events, { event: eventName });
        if (!eventExists) {q.events.push(newEvent); }
      }
      else {  q.events.push(newEvent); }

      return q.saveAsync()
    })
    .spread(function(updated) {
      res.status(200).json(updated);
    })
    .error(function(err){
      res.status(500).json(err.message);
    });
};


exports.addMessage = function(req, res) {
  var qId = req.params.id;
  var message = req.body;
  Quote.findByIdAsync(qId)
    .then(function(quote) {
      quote.messages.push(message);
      return quote.saveAsync();
    })
    .spread( function(quote, numAffected) {

      var lastMessageId = quote.messages[quote.messages.length - 1]._id;
      quote.last_message_seen[req.user._id] = lastMessageId;
      quote.markModified('last_message_seen');
      quote.save(function(err, quote, numAffected){
        if (err) { throw err;  }

        try {
          var myId = req.user._id;
          var myFirstIndex = _.findIndex(quote.messages, { from: myId});
          var theirLastIndex = _.findLastIndex(quote.messages, function(m) {
            return m.from.toString() != myId.toString();
          });

          if (theirLastIndex !== -1) {
            // They have sent at least one message
            var theirId = quote.messages[theirLastIndex].from;
            var thisMessageIndex = quote.messages.length - 1;

            if (theirLastIndex < myFirstIndex && // Their last message is before my first AND
              thisMessageIndex == myFirstIndex) { // This message is my first message
              textNotifier.newMessage(req.user, theirId, message);
            }
          }
        }
        catch(e){
          console.error(e);
        }
        finally {
          return res.status(201).json(message);
        }
      })
    })
    .catch(function(err) {
      return handleError(res, err);
    });
};

// Deletes a quote from the DB.
exports.destroy = function(req, res) {
  Quote.findById(req.params.id, function (err, quote) {
    if(err) { return handleError(res, err); }
    if(!quote) { return res.send(404); }
    quote.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}