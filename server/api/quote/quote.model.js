'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    EventSchema = require('../helper-schemas').Event;


var QuoteSchema = new Schema({
  request: {
    type: Schema.Types.ObjectId,
    ref: 'Request'
  },
  from: {
    type: Schema.Types.ObjectId,
    ref: 'Pro'
  },
  sent_at: {
    type: Date,
    default: Date.now
  },
  status: {
    type: String,
    enum: ['pending', 'hired' , 'rejected'],
    default: 'pending'
  },
  status_changed_on: Date,
  last_status: {
    type: String,
    enum: ['pending', 'hired' , 'rejected']
  },
  reject_reason: {
    type: String,
    enum: [
      'need-more-reviews',
      'too-expensive',
      'not-right-fit',
      'quote-not-detailed'
    ]
  },
  messages:[
    {
      date: {
        type: Date,
        default: Date.now
      },
      from: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      message: String
    }
  ],
  // A hash of UserId: MessageId indicating the last message seen
  last_message_seen: {
    type: Schema.Types.Mixed,
    default: {}
  },
  rate: {
    method: {
      type: String,
      enum: ['hourly','fixed','contact'],
      default: 'hourly'
    },
    value: Number
  },
  refunded: {
    type: Boolean,
    default: false
  },
  events: {
    type: [ EventSchema ],
    default: []
  }
});

module.exports = mongoose.model('Quote', QuoteSchema);