'use strict';

var express = require('express');
var controller = require('./quote.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('admin'),  controller.index);
router.get('/request/:requestId/pro/:proId', controller.getByRequestAndPro);
router.get('/:id', controller.show);
router.post('/:id/messages', auth.isAuthenticated(), controller.addMessage);
router.post('/:id/events', auth.isAuthenticated(), controller.registerEvent);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;