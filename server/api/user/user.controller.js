'use strict';

var User = require('./user.model');
var proCtrl = require('./pro/pro.controller');
var fs = require('fs');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var _ = require('lodash');
var Promise = require('bluebird');
var gm = require('gm');
var mailchimpCtrl = require('../mailchimp/mailchimp.controller');
var inspect = require('util').inspect;

var validationError = function(res, err) {
  return res.json(422, err);
};

var handleError = function(res, err) {
  res.status(500).send(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.send(500, err);
    res.json(200, users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token, accountType: 'User' });
  });
};


/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};

exports.find = function(req, res, next) {
  var searchParams = req.query;
  User.findOneAsync(searchParams)
    .then(function(user) {
      return res.status(200).json(user);
    })
    .error(function(err) {
      return handleError(res,err);
    })
    .catch(function(err) {
      next(err);
    });
};


exports.uploadProfilePic = function (req, res) {
  var formidable = require('formidable');
  var form = new formidable.IncomingForm();
  var uuid = require('node-uuid').v4();
  var fullPromise, thumbPromise;
  var aws = require('aws-sdk');
  aws.config.update({
    accessKeyId: config.s3.accessKey,
    secretAccessKey: config.s3.secret
  });

  var DEBUG = false;

  function uploadToS3(streamOrBuffer, fileName) {
    return new Promise(function(resolve, reject) {
      var Uploader = new aws.S3.ManagedUpload({
        queueSize: 1,
        params: {Bucket: config.s3.bucket, Key: fileName, Body: streamOrBuffer }
      });

      if (DEBUG) {
        Uploader.on('httpUploadProgress', function(evt) {
          console.log('%s: %d',fileName, evt.loaded*100/evt.total);
        });
      }

      Uploader.send(function(err, s3res) {
        if (err) {
          return reject(err);
          console.error('Could not upload to S3: ', err.message)
        }
        resolve(s3res);
      });

    });
  }

  form.onPart = function(part) {
    if (!part.filename) {
      form.handlePart(part);
    }
    else {
      var ext = part.filename.split('.').pop();
      var fileName = 'profile-' + req.user._id + Date.now().toString() + '.' + ext;

      fullPromise = new Promise(function(resolve) {
        gm(part, part.filename)
          .options({imageMagick: true})
          .autoOrient()
          .compress('JPEG')
          .quality(40)
          .toBuffer(function(err, buffer) {
            if (err) console.error(err);
            if (DEBUG) console.log('UPLOADING FULL :' + fileName + ' to S3');
            fullPromise = uploadToS3(buffer, fileName).then(resolve);
          });
      });

      thumbPromise = new Promise(function(resolve) {
        gm(part, part.filename)
          .options({imageMagick: true})
          .autoOrient()
          .filter('Lanczos')
          .compress('JPEG')
          .resize(150,150, '^')
          .gravity('Center')
          .crop(150,150)
          .toBuffer(function(err, buffer) {
            if (err) console.error(err);
            if (DEBUG) console.log('UPLOADING THUMB:' + fileName + ' thumb to S3');
            uploadToS3(buffer, 'thumb-'+ fileName).then(resolve);
          });
      });



      Promise.join(fullPromise, thumbPromise).spread(function(s3FullResult, s3ThumbResult) {
          User.update(
            { _id: req.user._id },
            {
              image: {
                url: s3FullResult.Location,
                thumb: s3ThumbResult.Location
              }
            })
            .exec()
            .then(function(){
              return res.status(200).json({
                message: 'file uploaded successfully!',
                url: s3FullResult.Location,
                thumbUrl: s3ThumbResult.Location
              });
            }, function(err){
              return res.status(500).send(err);
            });
        },
        function onError(reason) {
          return handleError(res, reason);
        });
    }
  };

  form.on('aborted', function() {
    console.error('Profile photo upload aborted');
  });

  form.on('error', function(err) {
    console.error('Profile photo upload error: \n%s', err);
    res.status(500).json(err);
  });

  form.parse(req);
};


// Updates an existing User
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  User.findById(req.params.id, function (err, user) {
    if (err) { return handleError(res, err); }
    if(!user) { return res.send(404); }
    var oldEmail = user.email;
    var updated = _.extend(user, req.body);
    user.save(function (err) {
      if (err) { return handleError(res, err); }
      mailchimpCtrl.updateSubscriber(user, oldEmail);
      return res.json(200, user);
    });
  });
};


/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findById(req.params.id, function(err, user) {
    if(err) return res.send(500, err);
    user.remove(function(err) {
      mailchimpCtrl.removeSubscriber(user);
      return res.send(204);
    });

  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Change a users password from a reset link
 */
exports.resetPassword = function(req, res, next) {
  var newPass = String(req.body.password);
  var token = String(req.body.token);
  var decoded = jwt.decode(token, config.secrets.session);

  User.findById(decoded.id, function (err, user) {
    var now = new Date();
    if ( now > decoded.exp) {
      return res.status(422).send('Token has expired');
    }

    user.password = newPass;
    user.save(function(err) {
      if (err) return validationError(res, err);
      res.send(200);
    });

  });
};


/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(401);
    var type = user._accountType;
    if (type === 'Pro') {
      return proCtrl.me(req,res,next);
    }
    else {
      res.status(200).json(user);
    }
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};