var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  extend = require('mongoose-schema-extend'),
  Request = require('../../request/request.model'),
  Review = require('../../review/review.model');

var UserSchema = require('../user.model').schema;

// Note: Other attributes shared with USER model
var ProSchema = UserSchema.extend({
  // Did an admin make this pro? if yes, then it's unclaimed
  unclaimed: {
    type: Boolean,
    default: false
  },

  // Feature this pro on custom landing pages?
  featured: {
    type: Boolean,
    default: false
  },

  // Name of Business
  name: String,
  image: {
    url: {
      type: String,
      default: 'assets/images/profiles/empty-profile-pro.png'
    },
    thumb: {
      type: String,
      default: 'assets/images/profiles/thumb-empty-profile-pro.png'
    }
  },

  // Media images
  portfolio: [{
    full : {
      w: Number,
      h: Number,
      src: String
    },
    thumb: {
      w: Number,
      h: Number,
      src: String
    }
  }],

  // Services provided (subcategories)
  services: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Category'
    }
  ],

  suggested_services: String,
  address: {
    street: String,
    city: String,
    province: String,
    postal: String
  },
  location: {
    lngLat: {
      type: [ Number ],
      index: '2dsphere'
    }
  },
  companyPhone: String,
  travel: {
    incoming: Boolean,
    outgoing: Boolean,
    online: Boolean,
    distance: Number // If outgoing
  },
  website: {
    type: String,
    set: addHttpIfMissing
  },
  description: String,
  feedback : {
    average_rating: {
      type: Number,
      default: 0
    },
    reviews: [ Review.schema ]
  },
  incoming_requests: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Request'
    }
  ],
  // Counter for above
  requestCount: {
    type: Number,
    index: true,
    default: 0
  },
  credits: {
    type: Number,
    default: 0,
    min: 0
  },
  preferences: {
    hired: {
      type: Boolean,
      default : true
    },
    leads: {
      type: Boolean,
      default : true
    },
    text: {
      newLead: {
        type: Boolean,
        default: true
      },
      // Text when a customer first replies
      newMessage: {
        type: Boolean,
        default: true
      }
    }
  },
  status: {
    active: {
      type: Boolean,
      default: false
    },
    tutorial: {
      type: Boolean,
      default: false
    }
  },

  // Insurance and Licenses
  licenses: { type: [String], default: [] },
  insurance: {
    workerscompensation: { type: Boolean, default: false },
    liability: { type: Boolean, default: false },
    bonded: { type: Boolean, default: false }
  }
});


/**
 * Sanitizing
 */
function addHttpIfMissing(url) {
  var hasProtocol = /^https?:\/\//.test(url);
  if (url && url !== '' && !hasProtocol) {
    return 'http://' + url;
  }
  return url;
}


module.exports = mongoose.model('Pro', ProSchema);