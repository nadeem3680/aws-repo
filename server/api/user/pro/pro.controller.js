'use strict';

var Pro = require('./pro.model');
var _ = require('lodash');
var Customer = require('../customer/customer.model');
var Quotes = require('../../quote/quote.model');
var Category = require('../../category/category.model');
var Review = require('../../review/review.model');
var Request = require('../../request/request.model');
var config = require('../../../config/environment');
var jwt = require('jsonwebtoken');
var errors = require('../../../components/errors');
var Promise = require('bluebird');
var gm = require('gm');
var proMailCtrl = require('../../mail/pro-mail.controller');
var txtCtrl = require('../../text/text.controller');
var mailchimpCtrl = require('../../mailchimp/mailchimp.controller');
var aws = require('aws-sdk');

aws.config.update({
  accessKeyId: config.s3.accessKey,
  secretAccessKey: config.s3.secret
});

var userCtrl = require('../user.controller');


var validationError = function(res, err) {
  return res.json(422, err);
};

var handleError = function(res, err) {
  res.status(500).send(err);
};

/**
 * Get list of pros
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  Pro.find({
    _accountType: 'Pro'
  }, '-salt -hashedPassword')
    .populate('category services')
    .exec()
    .then(function (pros) {
      res.json(200, pros);
    }, function(err) {
      res.status(500).send(err);
    });
};


exports.getProsForRequest = function(req, res) {
  var requestId = req.params.requestId;

  Pro.findAsync({
    _accountType: 'Pro',
    incoming_requests: requestId
  }, '-salt -hashedPassword')
    .then(function(pros) {
      res.status(200).json(pros);
    })
    .error(function(err) {
      res.status(500).json(err.message);
    });
};

/**
 * Get list of pros in a given category
 * And grab from google results and cache if not enough
 */
exports.getListings = function(req, res) {
  // fetch google listings if my results are less than:
  var categoryRoute = req.params.categoryRoute;
  if (typeof categoryRoute === 'undefined') return handleError(res, 'Category route not defined.');

  var latLng = req.params.latLng.replace(/[)( ]/g,'');
  var lngLatAr = latLng.split(',').reverse();

  var thisCategoryPromise = Category.findOne({
    route: categoryRoute
  }).lean().execAsync();

  thisCategoryPromise.then(function(category) {
    if (category.parent) {
      // It's not a root category
      return [category];
    }
    else {
      return Category.findAsync({
        parent: category._id
      });
    }
  }).then(function(categories) {
    return Pro.find({
      _accountType: 'Pro',
      featured: true,
      services: { $in: categories },
      "location.lngLat": {
        $near: {
          $geometry: {
            type: "Point",
            coordinates: lngLatAr
          },
          $maxDistance: 50 * 1000
        }
      }
    }).sort({ 'feedback.average_rating': -1, featured: -1 }).lean().execAsync()
  })
    .then(function(pros) {
      return res.status(200).json(pros);
    })
    .error(function(err) {
      console.error(err);
      return handleError(res, err);
    })
    .catch (function(err) {
    console.error(err);
    return handleError(res, err);
  });
};



/**
 * Get a single pro
 */
exports.show = function (req, res, next) {
  var proId = req.params.id;

  Pro.findById(proId, '-salt -hashedPassword')
    // TODO: embed service name instead of populating
    .populate('incoming_requests services')
    .exec()
    .then(function (pro) {
      if (!pro) return errors[404](req,res);
      res.status(200).json(pro);
    },
    function(err) {
      return next(err);
    });
};

// Updates an existing Pro
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Pro.findById(req.params.id, function (err, user) {
    if (err) { return handleError(res, err); }
    if(!user) { return res.send(404); }
    var oldEmail = user.email;
    var updated = _.extend(user, req.body);
    user.markModified('portfolio');
    user.save(function (err) {
      if (err) { return handleError(res, err); }
      mailchimpCtrl.updateSubscriber(user, oldEmail);
      return res.json(200, user);
    });
  });
};


/**
 * Get a single pro
 */
exports.me = function (req, res, next) {
  var proId = req.user._id;

  Pro.findById(proId, '-salt -hashedPassword')
    .populate('incoming_requests services').exec()
    .then(
    function(pro) {
      return Category.populateAsync(pro, {
        path: 'incoming_requests.category',
        model: Category,
        select: 'credits_required name scheduling_type'
      });
    })
    .then(function(pro) {
      return Customer.populateAsync(pro, {
        path: 'incoming_requests.requested_by',
        model: Customer,
        select: '-hashedPassword -facebook -google -_accountType'
      });
    })
    .then(function (pro) {
      return res.status(200).json(pro);
    },
    function(err) {
      return next(err);
    });
};

// Returns quotes sent by pro
exports.myQuotes = function (req, res, next) {
  var proId = req.user._id;

  Quotes.findAsync({
    from: proId
  }).then(function(quotes) {
    return res.status(200).json(quotes);
  }).error(function(error) {
    return next(error);
  });
};

/**
 * Creates a new pro
 */
exports.create = function (req, res, next) {
  var moment = require('moment');
  // Give the pro all the requests in the last week
  Request.findAsync({
    status: 'active',
    category: { $in: req.body.services },
    $where: "this.quotes.length < 5",
    date_created: { $gte: moment().subtract(1,'week').toDate() }
  }).then(function(activeRequests) {
    var newPro = new Pro(req.body);
    newPro.provider = 'local';
    newPro.role = 'user';
    newPro.incoming_requests = activeRequests;

    newPro.save(function(err, pro) {
      if (err) return validationError(res, err);
      var token = jwt.sign({_id: pro._id }, config.secrets.session, { expiresInMinutes: 60*5 });

      if (!pro.unclaimed) {
        proMailCtrl.welcome(pro);
        mailchimpCtrl.addSubscriber(pro);

        Category.findAsync({ _id: { $in: pro.services }})
          .then(function(categories) {
            var services = _.pluck(categories,'name');
            var message = "Tasky: New pro registered.\n" +
              "email: "+ pro.email + "\n" +
              "name: "+ pro.firstName + pro.lastName + "\n" +
              "Phone: "+ pro.companyPhone + "\n" +
              "Services: "+ services.join(',');
            txtCtrl.notifyAdmins(message);
          });
      }

      res.json({ token: token, accountType: 'Pro' });
    });
  })
    .error(function(err){
      return res.status(500).send(err);
    });
};


exports.newReview = function(req, res, next) {
  var id = req.params.id;

  Pro.findByIdAsync(id).then(function(pro) {
    var approvedReviews = _.filter(pro.feedback.reviews, function(r) { return r.approved; }),
      newReview = new Review(req.body);

    newReview.approved = true;
    approvedReviews.push(newReview);

    //Calculate  rating sum
    var ratingSum = 0;
    for (var i=0; i<approvedReviews.length; i++) {
      ratingSum += approvedReviews[i].rating;
    }

    pro.feedback.reviews.push(newReview);
    pro.feedback.average_rating = ratingSum/approvedReviews.length;

    pro.save(function (err, professional) {
      if (err) return res.status(500).send(err);
      return res.status(201).json({
        review: newReview,
        rating: professional.feedback.average_rating
      });
    });
  })
    .catch (function(err) {
    return next(err);
  });
};

exports.newPendingReview = function(req, res) {
  var proId = req.params.id;
  var newPendingReview = new Review(req.body);

  newPendingReview.approved = false;

  Pro.updateAsync({ _id: proId}, {
    $push: { 'feedback.reviews': newPendingReview }
  })
    .then(function(){
      return res.status(201).json('OK');
    })
    .error(function(error) {
      console.error(error);
      return handleError(res, error);
    })
};

// Deletes a pending review submitted by past clients
// DELETE /api/pros/:id/pending-reviews/:reviewId
exports.deletePendingReview = function(req, res) {
  var proId = req.params.id,
    reviewId = req.params.reviewId;

  Pro.updateAsync({ _id: proId }, {
    $pull: { 'feedback.reviews': { _id: reviewId }}
  })
    .then(function(){
      return res.status(204).json('OK');
    })
    .error(function(error) {
      console.error(error);
      return handleError(res, error);
    })

};


// Approves a pending review submitted by past clients
// PUT /api/pros/:id/pending-reviews/:reviewId/approve
exports.approvePendingReview = function(req, res) {
  var proId = req.params.id,
    reviewId = req.params.reviewId;

  Pro.findOneAsync({ _id: proId })
    .then(function(pro){
      var reviews = pro.feedback.reviews;
      var pendingReview = _.find(reviews, function(r) {
        return r._id.toString() == reviewId;
      });
      if (!pendingReview) return rest.status(404).send('Could not find review');
      pendingReview.approved = true;
      var approvedReviews = _.filter(reviews, { approved: true});
      var numApprovedReviews = approvedReviews.length;
      var ratingSum = 0;
      for (var i=0; i<numApprovedReviews; i++) {
        ratingSum += approvedReviews[i].rating;
      }
      pro.feedback.average_rating = ratingSum/numApprovedReviews;
      return pro.saveAsync();
    })
    .spread( function(numAffected){
      if (numAffected) res.status(200).json('OK');
    })
    .error(function(error) {
      console.error(error);
      return handleError(res, error);
    })

};


exports.getPendingReviews = function(req, res) {
  Pro.find({
    _accountType: 'Pro',
    'feedback.reviews.approved': false
  })
    .select('feedback')
    .lean()
    .execAsync()
    .then(function(pros) {
      var reviews = [];
      pros.forEach(function(pro) {
        var unapprovedReviews = _.filter(pro.feedback.reviews, { approved: false });
        unapprovedReviews.forEach(function(r) {
          r.proId = pro._id;
          reviews.push(r);
        });
      });

      return res.status(200).json(reviews);
    })
    .error(function(err) {
      return res.status(500).send(err);
    });
};



exports.uploadPortfolioPics = function(req, res) {
  var formidable = require('formidable');
  var form = new formidable.IncomingForm();
  var fullPromise, thumbPromise;
  var DEBUG = false;

  function uploadToS3(streamOrBuffer, fileName) {
    return new Promise(function(resolve, reject) {
      var Uploader = new aws.S3.ManagedUpload({
        queueSize: 1,
        params: {Bucket: config.s3.bucket, Key: fileName, Body: streamOrBuffer }
      });

      if (DEBUG) {
        Uploader.on('httpUploadProgress', function(evt) {
          console.log('%s: %d',fileName, evt.loaded*100/evt.total);
        });
      }

      Uploader.send(function(err, s3res) {
        if (err) {
          console.error('Could not upload to S3: ', err.message);
          return reject(err);
        }
        resolve(s3res);

      });
    });
  }

  form.onPart = function(part) {
    if (!part.filename) {
      form.handlePart(part);
    }
    else {
      // Don't forget to look at orientation and return right height/width
      var ext = part.filename.split('.').pop();
      var fileName =  'portfolio-' + req.user._id + '-' + Date.now().toString()+'.' + ext;
      var sizeResolver = Promise.defer(), orientationResolver = Promise.defer();

      fullPromise = new Promise(function(resolve) {
        gm(part, part.filename)
          .options({imageMagick: true})
          .autoOrient()
          .size(function(err, size) {
            sizeResolver.resolve(size);
          })
          .orientation(function(err, o) {
            orientationResolver.resolve(o);
          })
          .compress('JPEG')
          .quality(40)
          .toBuffer(function(err, buffer) {
            if (err) console.error(err);
            if (DEBUG) console.log('UPLOADING FULL :' + fileName + ' to S3');
            uploadToS3(buffer, fileName).then(resolve);
          });
      });

      thumbPromise = new Promise(function(resolve) {
        gm(part,part.filename)
          .options({imageMagick: true})
          .autoOrient()
          .filter('Lanczos')
          .resize(150,150, '^')
          .gravity('Center')
          .crop(150,150)
          .toBuffer(function(err, buffer) {
            if (err) console.error(err);
            if (DEBUG) console.log('UPLOADING THUMB:' + fileName + ' thumb to S3');
            thumbPromise = uploadToS3(buffer, 'thumb-'+ fileName).then(resolve);
          });
      });


      Promise.join(fullPromise, thumbPromise, sizeResolver.promise, orientationResolver.promise).spread(
        function(s3FullResult, s3ThumbResult, size, orientation) {
          var flip = !!~['RightTop', 'LeftBottom'].indexOf(orientation);
          var height = flip ? size.width : size.height;
          var width = flip ? size.height : size.width;

          var newFileResults = {
            full: { w: width, h: height, src: s3FullResult.Location },
            thumb: { w: 150, h: 150, src: s3ThumbResult.Location }
          };

          return Pro.findByIdAndUpdateAsync(req.user._id,
            { $push: { portfolio : newFileResults }})
            .then(function(pro) {
              return res.status(200).json(_.last(pro.portfolio));
            });
        }).error(function(err){
          return res.status(500).send(err);
        });
    }
  };


  form.on('aborted', function() {
    console.error('Portfolio photo upload aborted');
  });

  form.on('error', function(err) {
    console.error('Portfolio photo upload error: \n%s', err);
    res.status(500).json(err);
  });

  form.parse(req);
};


exports.removePortfolioItem = function(req,res) {
  var user = req.user;
  var photoId = req.params.photoId;

  Pro.updateAsync({ _id: user._id}, {
    $pull: { portfolio: { _id: photoId }}
  })
    .then(function(pro) {
      var photo = _.find(user.portfolio, function(portfolioItem) {
        return portfolioItem._id.toString() === photoId
      });

      var fullKey = photo.full.src.slice(photo.full.src.lastIndexOf('/')+1);
      var thumbKey = photo.thumb.src.slice(photo.thumb.src.lastIndexOf('/')+1);

      // DELETE from AWS.
      var s3 = new aws.S3();
      s3.deleteObjects({
        Bucket: config.s3.bucket,
        Delete: {
          Objects: [  { Key: fullKey }, { Key: thumbKey }]
        }
      },function (err, s3res) {
        if (err) throw err;
        return res.status(204).send();
      });
    })
    .error(function(err) {
      return res.status(500).send(err.message);
    });
};

exports.resetCreditsIfUnlimited = function(req, res, next) {
  var id = req.params.id;

  Pro.findByIdAsync(id).then(function(pro) {
    if (!pro) return res.status(404).send('Could not find pro with id: ' + id);

    if (pro.credits == Infinity) {
      pro.credits = 0;
      pro.save(function(err) {
        if (err) {
          return res.status(500).send(err)
        }
        return res.status(200).send('Credits reset to 0');
      });
    }
  })
    .catch (function(err) {
    res.status(500).send(err);
  });
};

exports.destroy = function(req,res,next) {
  userCtrl.destroy(req,res,next);
};

exports.changePassword = function(req,res,next) {
  userCtrl.changePassword;
};


exports.activate = function (req, res, next) {
  var id = req.params.id;

  Pro.findByIdAsync(id)
    .then(function (pro) {
      if (pro.status.active)
        return null;
      pro.status.active = true;
      return pro.saveAsync();
    })
    .spread(function (pro) {
      if (!pro) return;
      // Send an activation mail
      return proMailCtrl.activate(pro);
    })
    .then(function () {
      return res.status(200).send('Pro activated');
    })
    .catch(function (err) {
      return res.status(500).send(err);
    });
};

exports.tutorial = function (req, res, next) {
  var id = req.params.id;

  var promise = Pro.findByIdAsync(id)
    .then(function (pro) {
      if (pro.status.tutorial)
        return null;
      pro.status.tutorial = true;
      return pro.saveAsync();
    })
    .then(function (result) {
      if (!result)
        return;
      // Send an activation mail ?
      // proMailCtrl.active(result[0]);
      return;
    })
    .then(function () {
      return res.status(200).send('Tutorial done');
    })
    .catch(function (err) {
      return res.status(500).send(err);
    });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};

module.exports = exports;