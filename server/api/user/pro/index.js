'use strict';

var express = require('express');
var controller = require('./pro.controller');
var auth = require('../../../auth/auth.service');
var cache = require('apicache').options({ debug: true}).middleware;

var router = express.Router();

// Pending Reviews from past clients
router.get('/category/:categoryRoute/latLng/:latLng', cache('15 minutes'), controller.getListings);

// Pending Reviews
router.get('/pending-reviews', auth.hasRole('admin'), controller.getPendingReviews);
router.post('/:id/pending-reviews',  controller.newPendingReview);
router.put('/:id/pending-reviews/:reviewId/approve',auth.hasRole('admin'), controller.approvePendingReview);
router.delete('/:id/pending-reviews/:reviewId', auth.hasRole('admin'), controller.deletePendingReview);
// Normal Reviews
router.post('/:id/review', controller.newReview);


router.get('/', auth.hasRole('admin'), controller.index);
router.get('/request/:requestId', auth.hasRole('admin'), controller.getProsForRequest);

router.put('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/me/quotes', auth.isAuthenticated(), controller.myQuotes);
router.post('/me/portfolio', auth.isAuthenticated(), controller.uploadPortfolioPics);
router.delete('/me/portfolio/:photoId', auth.isAuthenticated(), controller.removePortfolioItem);
router.get('/:id',controller.show);
router.put('/:id/resetCreditsIfUnlimited', controller.resetCreditsIfUnlimited);
router.post('/', controller.create);





router.put('/:id/activate', auth.hasRole('admin'), controller.activate);
router.put('/:id/tutorial', auth.isAuthenticated(), controller.tutorial);

module.exports = router;
