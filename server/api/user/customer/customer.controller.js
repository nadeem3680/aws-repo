'use strict';

var Customer = require('./customer.model');
var _ = require('lodash');
var config = require('../../../' +
  'config/environment');
var jwt = require('jsonwebtoken');
var mailchimpCtrl = require('../../mailchimp/mailchimp.controller');
var txtCtrl = require('../../text/text.controller');


var userCtrl = require('../user.controller');


var validationError = function(res, err) {
  return res.json(422, err);
};


/**
 * Get list of customers
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  Customer.find({
    _accountType: 'Customer'
  })
    .select('-salt -hashedPassword')
    .sort({ created_at: -1 })
    .exec(function (err, customers) {
      if(err) return res.send(500, err);
      res.json(200, customers);
    });
};

/**
 * Creates a new customer
 */
exports.create = function (req, res, next) {
  var newCustomer = new Customer(req.body);
  newCustomer.customervider = 'local';
  newCustomer.role = 'user';
  newCustomer.save(function(err, customer) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: customer._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    mailchimpCtrl.addSubscriber(customer);
//    txtCtrl.notifyAdmins("New Customer:\nname: "+ customer.firstName + customer.lastName);

    res.json({ token: token, accountType: 'Customer' });
  });
};

exports.show = function(req,res,next) {
  var custId = req.params.id;
  Customer
    .findOne({ _id: custId})
    .select('-hashedPassword -salt')
    .execAsync()
    .then(function(customer) {
      return res.status(200).json(customer);
    })
    .error(function(err) {
      return res.status(500).json(err);
    })
};

exports.me = function(req,res,next) {
  userCtrl.me(req,res,next);
};

exports.destroy = function(req,res,next) {
  userCtrl.destroy(req,res,next);
};

exports.changePassword = function(req,res,next) {
  userCtrl.changePassword;
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};

module.exports = exports
