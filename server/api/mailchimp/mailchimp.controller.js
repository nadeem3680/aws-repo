'use strict';

// DONT MODIFY MAILCHIMP UNLESS ITS PRODUCTION
var isProd = process.env.NODE_ENV == 'production'
// ===================================================

var _ = require('lodash');
var config = require('../../config/environment');

// Mailchimp Stuff
var MCapi = require('mailchimp').MailChimpAPI;
var mcConfig = config.mailchimp;
var apiKey = mcConfig.apiKey;

try {
  var api = new MCapi(apiKey, { version : '2.0' });
} catch (error) {
  console.error(error.message);
}



// Creates a new subscriber in the DB and mailchimp list.
exports.addSubscriber = function(user) {
  if (!isProd) return;

  var merge = {
    'FNAME': user.firstName,
    'LNAME': user.lastName,
    'RECEIVE_NEWS': user.preferences.news,
    'TYPE': user._accountType || 'OTHER',
    'BNAME' : user.name || 'N/A'
  };

  api.call('lists','subscribe', {
    id: mcConfig.registeredUsersListId,
    email: {
      email: user.email
    },
    merge_vars: merge
  }, function(error, data) {
    if (error) {
      console.log('Could not add user to registered subscribers in Mailchimp:', error);
    }
    else {
      console.log('Successfully added user %s to Mailchimp subscribers', user.email);
    }
  });
};

exports.updateSubscriber = function(user, oldEmail) {
  if (!isProd) return;

  var merge = {
    'FNAME': user.firstName,
    'LNAME': user.lastName,
    'RECEIVE_NEWS': user.preferences.news,
    'BNAME' : user.name || 'N/A',
    'EMAIL' : user.email
  };

  api.call('lists','update-member', {
    id: mcConfig.registeredUsersListId,
    email: {
      email: oldEmail
    },
    merge_vars: merge
  }, function(error, data) {
    if (error) {
      console.log('Could not update user in Mailchimp:', error);
    }
    else {
      console.log('Successfully user user %s to Mailchimp', user.email);
    }
  });
};

exports.removeSubscriber = function(user) {
  if (!isProd) return;

  api.call('lists', 'unsubscribe', {
      id: mcConfig.registeredUsersListId,
      email: {
        email: user.email
      },
      "delete_member": true,
      "send_goodbye": false,
      "send_notify": false
    },
    function(err, data) {
      if (err) {
        console.error('Problem removing user %s', user.email, err);
        return;
      }
      console.log('Successfully unsubscribed user %s', user.email);
    })
};