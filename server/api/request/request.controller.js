'use strict';

var _ = require('lodash');
var Request = require('./request.model');
var Category = require('../category/category.model');
var Pro = require('../user/pro/pro.model');
var User = require('../user/user.model');
var Quote = require('../quote/quote.model');
var emailNotifier = require('../mail/pro-mail.controller');
var textNotifier = require('../text/text.controller');
var Promise = require('bluebird');


// Get list of requests
exports.index = function(req, res) {
  Request.find({})
    .populate('requested_by category')
    .sort({ date_created: -1})
    .execAsync()
    .then(function (requests) {
      return res.json(200, requests);
    })
    .error(function(err) {
      return handleError(res, err);
    });
};

// Get a single request
exports.show = function(req, res) {
  var include = req.query['include'];
  if (_.isString(include)) { include = include.split(','); }
  var query = Request.findById(req.params.id);
  if (_.contains(include, 'requested_by')) {
    query = query.populate('requested_by');
  }

  query.populate(
    {
      path: 'category quotes'
    })
    .exec()
    .then(
    function(request) {
      return Quote.populateAsync(request, {
        path: 'quotes.from',
        model: User,
        select: '-hashedPassword -salt'
      });
    })
    .then(
    function(modReq) {
      return res.json(modReq);
    },
    function(err) {
      return handleError(res, err);
    });
};

exports.registerEvent = function(req, res) {
  var requestId = req.params.id,
    addOnce = req.query.once,
    eventName = req.body.event,
    eventData = req.body.data;

  if (!requestId || !eventName || !eventData) { return res.status(400).send('Malformed event registration request.'); }

  Request.findOneAsync({ _id: requestId })
    .then(function(r) {
      var newEvent = {
        event: eventName,
        data: eventData
      };

      if (addOnce && addOnce == 'true') {
        var eventExists = _.any(r.events, { event: eventName, data: eventData });
        if (!eventExists) {r.events.push(newEvent); }
      }
      else {  r.events.push(newEvent); }

      return r.saveAsync()
    })
    .spread(function(updated) {
      res.status(200).json(updated);
    })
    .error(function(err){
      res.status(500).json(err.message);
    });
};

// Requested by me
exports.myRequests = function (req, res) {
  var user = req.user;
  if (!user) return res.json(401);
  Request.find({
    requested_by: user._id
  })
    .populate(
    {
      path: 'category quotes'
    })
    .sort({ date_created: -1})
    .exec()
    .then(
    function(request) {
      return Quote.populateAsync(request, {
        path: 'quotes.from',
        model: User,
        select: '-hashedPassword -salt'
      });
    })
    .then(
    function(modReq) {
      return res.json(modReq);
    },
    function(err) {
      return handleError(res, err);
    });

};

exports.indexByUser = function (req, res) {
  var userId = req.params.userId;

  Request.find({
    requested_by: userId
  })
    .populate('category')
    .sort({ date_created: -1})
    .exec()
    .then(
    function(requests) {
      res.status(200).send(requests);
    },
    function(err) {
      return handleError(res, err);
    });
};



function sendRequestToPros(request, pros) {
  _.remove(pros, function(pro) {
    for (var i = 0; i < pro.incoming_requests.length; i++ ) {
      if (pro.incoming_requests[i].toString() === request._id.toString() ) {
        return true;
      }
    }
    return false;
  });

  var proIds = _.pluck(pros, '_id');
  // Array of promises of all the pros who had their incoming_requests updated
  return Pro.updateAsync({
    _id: { $in: proIds }
  }, {
    $inc: { requestCount: 1 },
    $addToSet: { incoming_requests: request }
  }, { multi: true }).spread(function(numAffected, rawResponse){
    if (numAffected == 0) { return [ 'No Pros Emailed', 'No Pros Texted']; }
    console.log('users %s now have request %s', _.pluck(pros, 'email'), request._id);
    console.log('num Affected: %d', numAffected, rawResponse);
    // Send notification Email and Text to Pro's we sent to
    return [
      emailNotifier.newRequest(request, pros),
      textNotifier.newLead(request._id, pros)
    ];
  });
}
// Creates a new request in the DB.
exports.create = function(req, res) {
  var user = req.user;
  var doesService = false;

  // First check to see if pro does this service,
  // Don't allow fake requests
  if (user._accountType == 'Pro') {
    var reqCategory = req.body.category;
    Pro.findById(user._id, function(err, user) {
      doesService = !!_.find(user.services, function(service) {
        return service.toString() == reqCategory;
      });

      if (doesService) {
        return res.status(403).send('Cannot send request for a service you perform.');
      }
    });
  }

  Request.createAsync(req.body)
    .then(function(requestObj) {
      // add the request to their requests list
      var travelRadius = 50; //KM
      if (requestObj.travel.to_pro || requestObj.travel.to_customer) {
        travelRadius = requestObj.travel.distance || travelRadius;
      }

      return Pro.find({
        _accountType: 'Pro',
        services: requestObj.category,
        "location.lngLat": {
          $near: {
            $geometry: {
              type: "Point",
              coordinates: requestObj.location.lngLat
            },
            $maxDistance: travelRadius * 1000
          }
        },
        _id: { $ne: user._id }
      })
        .limit(50)
        .execAsync()
        .then(function(pros) {
          // Let Admins know we got a new request
          Category.findByIdAsync(requestObj.category, { name: 1 })
            .then(function(cat) {
              textNotifier.notifyAdmins("New Request For " + cat.name);
            });

          return sendRequestToPros(requestObj, pros);
        });
    })
    .spread(function(emailResponse, textResponse) {

//      console.log("New Request Email response: ", emailResponse);
//      console.log("New Request Text Response", textResponse);
      res.status(201).json('Request Created');
    })
    .error(function(err) { handleError(res, err); })
    .catch(function(err) { handleError(res, err); });
};

// Given a list of pros' emails in body.pros,
// adds a request to their incoming requests and notifies them
exports.sendToPros = function(req, res) {

  var prosEmails = req.body.pros;
  var requestId = req.body.requestId;
  if (!prosEmails) { return res.status(400).json('No pro emails provided to request in "pros" parameter'); }

  var prosPromise = Pro.findAsync({ email: { $in: prosEmails } });
  var requestPromise = Request.findOneAsync({ _id: requestId });


  Promise.all([prosPromise, requestPromise])
    .spread(function(pros, request) {
      if (!pros) { return res.status(404).json('No pros with those emails found'); }
      if (!request) { return res.status(404).json('No request with that ID found'); }
      return sendRequestToPros(request, pros);
    })
    .spread(function(emailResponse, txtResponse){
      console.log('Email response: ', emailResponse);
      console.log('Text response: ', txtResponse);
      res.status(200).json({ emailResponse: emailResponse, textResponse: txtResponse });
    })

    .error(function(err) {
      console.error(err);
      return res.status(500).json(err)
    })
    .catch(function(err) {
      console.error(err);
      return res.status(500).json(err)
    });
};

// Updates an existing request in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Request.findByIdAsync(req.params.id)
    .then(function (request) {
      if(!request) { return res.send(404); }
      var preUpdate = request.toObject();
      var updated = _.assign(request, req.body);

      var deactivated = preUpdate.status == 'active' && updated.status == 'inactive';
      if (deactivated && _.isUndefined(req.body.inactive_date)) { updated.inactive_date = new Date(); }
      // If updated request has a new hired pro, set request and quote statuses and reasons
      if (deactivated && !_.isUndefined(updated.hired_pro) ) {
        updated.inactive_reason = 'hired-tasky-pro';
        var requestSavePromise = updated.saveAsync();
        var quotesSavePromise = Quote.findAsync({ request: req.params.id })
          .then(function(quotesForRequest){
            // Get All quotes for request
            var promises = [];
            // Check every quote
            _.forEach(quotesForRequest, function(quote){
              // Save its last status
              quote.last_status = quote.status;
              quote.status_changed_on = new Date();
              // if its from the newly hired pro, set the quote status to hired
              if (quote.from.toString() == updated.hired_pro) {
                quote.status = 'hired';
                promises.push(quote.saveAsync());
              }
              else {
                // if its from someone else, wasn't right fit
                quote.status = 'rejected';
                quote.reject_reason = 'not-right-fit';
                promises.push(quote.saveAsync());
              }
            });

            return Promise.all(promises);
          });

        return Promise.join(requestSavePromise, quotesSavePromise, function(requestVal, quotesVal) {
          // We gotta return updated request to client so return new request
          return requestVal;
        })
      }
      else {
        return updated.saveAsync();
      }
    })
    .spread(function(request){
      return res.json(200, request);
    })
    .error(function(err) {
      return handleError(res, err);
    });
};

// Deletes a request from the DB.
exports.destroy = function(req, res) {
  Request.findByIdAsync(req.params.id)
    .then(function (request) {
      if (!request) { return res.send(404); }
      return request.removeAsync()
    })
    .then(function() {
      return Pro.updateAsync({ _accountType: 'Pro' },
        { $pull: { incoming_requests: req.params.id }
        }, { multi: true});
    })
    .then(function() {
      return res.send(204);
    })
    .error(function(err) {
      return handleError(res, err);
    });
};

function handleError(res, err) {
  return res.send(500, err);
}