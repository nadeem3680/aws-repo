'use strict';

var mongoose = require('mongoose'),
//    extend = require('mongoose-schema-extend'),
  Question = require('../question/question.model'),
  Quote = require('../quote/quote.model'),
  Schema = mongoose.Schema,
  EventSchema = require('../helper-schemas').Event,
  _ = require('lodash');



var RequestSchema = new Schema({
  category:{ // Only used to populate category if many category details needed
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true,
    index: true
  },
  category_name: String, // Otherwise just use this TODO: Fill this throughout app
  requested_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    index: true
  },
  discarded_by: {
    type: [{
      type: Schema.Types.ObjectId,
      ref: 'Pro'
    }],
    default: []
  },

  questions: [ {
    question: String,
    answer: Schema.Types.Mixed
  }],
  status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
  },
  inactive_date: Date,
  inactive_reason: {
    type: String,
    enum: [
      'hired-tasky-pro',
      'hired-outside-pro',
      'did-it-myself',
      'did-not-like-quotes',
      'change-of-plans'
    ]
  },
  hired_pro: {
    type: Schema.Types.ObjectId
  },
  quotes: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Quote'
      }
    ],
    default: []
  },
  schedule_type: {
    type: String,
    enum: ['flexible', 'asap', 'date', 'other'],
    required: true
  },
  schedule_details: {
    date: Date,
    time: String,
    duration: String,
    description: String
  },
  travel: {
    to_pro: Boolean,
    to_customer: Boolean,
    remote: Boolean,
    distance: Number
  },
  location: {
    subLocality: String,
    lngLat:
      {
        type: [ Number ] ,
        index: '2dsphere'
      }
  },
  date_created: {
    type: Date,
    default: Date.now
  },
  events: {
    type: [ EventSchema ],
    default: []
  }
});


RequestSchema.pre('remove', function(next) {
  Quote.removeAsync({ _id: { $in: this.quotes }})
    .error(function(err) {
      console.error('There was a problem removing quotes for request %s when deleting request', this._id);
    });
  next();
});


RequestSchema.methods = {
  asHumanString: function(isHtml){
    var makeQuestion = function(text) {
      if (isHtml) { return '<strong class="question">' + text + '</strong><br>'; }
      else { return text + '\n'; }
    };

    var makeAnswer = function(text) {
      if (isHtml) { return '<p class="answer">' + text + '</p><br>'; }
      else { return '> ' + text + '\n\n'; }
    };

    var str = '';

    // Where?
    str += makeQuestion('Where?');
    if (isHtml) {
      str += makeAnswer(this.location.subLocality + '<br><p><img src="https://maps.googleapis.com/maps/api/staticmap?center=' + this.location.lngLat[1] + ',' + this.location.lngLat[0] + '&size=200x200&zoom=11&markers=color:blue%7C' + this.location.lngLat[1] + ',' + this.location.lngLat[0] + '"/>');
    }
    else {
      str += '> ' + this.location.subLocality + '\n\n';
    }


    // When?
    str += makeQuestion('When?');
    switch(this.schedule_type) {
      case 'flexible':
        str += makeAnswer('Flexible');
        break;
      case 'asap':
        str += makeAnswer('As soon as possible');
        break;
      case 'other':
        str += makeAnswer(this.schedule_details.description);
        break;
      case 'date':
        var dateStr = '';
        dateStr +=  'On ' + this.schedule_details.date.toDateString();
        if (this.schedule_details.time) { dateStr += ' at ' + this.schedule_details.time; }
        if (this.schedule_details.duration) { dateStr += ' for ' + this.schedule_details.duration; }
        str += makeAnswer(dateStr);
    }

    // How will meeting happen?
    str += makeQuestion('How will the meeting happen?');
    var travelStr = [];
    if (this.travel.to_pro) { travelStr.push('Customer may travel to pro') }
    if (this.travel.to_customer) { travelStr.push('Pro may travel to customer')}
    if (this.travel.remote) { travelStr.push('Work may be done remotely') }
    str += makeAnswer(travelStr.join(' OR '));


    // Category-specific questions
    _.each(this.questions, function(response) {
      str += makeQuestion(response.question);
      if (typeof response.answer !== 'string') { // Must be an array
        str += makeAnswer(response.answer.join(', '));
      }
      else { str += makeAnswer(response.answer); }

    });

    return str;
  }
};

module.exports = mongoose.model('Request', RequestSchema);
