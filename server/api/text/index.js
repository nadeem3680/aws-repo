'use strict';

var express = require('express');
var controller = require('./text.controller');

var router = express.Router();

// Route that runs when users text back twilio number
router.post('/noreply', controller.noReply);
// Route that runs when users call twilio number
router.post('/nocalls', controller.noCalls);

module.exports = router;