'use strict';

var _ = require('lodash');
var twilioModule = require('twilio');
var twilio = twilioModule(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
var Customer = require('../user/customer/customer.model');
var Pro = require('../user/pro/pro.model');
var User = require('../user/user.model');
var Request = require('../request/request.model');
var Promise = require('bluebird');

var taskyPhone = process.env.NODE_ENV == 'production' ? '+16477244442' : '+15005550006';


exports.notifyAdmins = function(message) {
  if (process.env.NODE_ENV === "production")
  [ '+16473000796', //Arash B
    '+16477392144', // Borna
    '+16478390921' // Arash Sadr
  ].forEach(function(receiver) {
      twilio.sendMessage({
        to: receiver,
        from: taskyPhone,
        body: message
      });
    });
};

// When user replies to notification number,
// lets user know via text that he shouldn't
exports.noReply= function(req,res) {
  var resp = new twilioModule.TwimlResponse();
  resp.message("TASKY.ME: Sorry, you can't reply to this number. If you have any questions call 877-777-5738");
  res.writeHead(200, {
    'Content-Type':'text/xml'
  });
  res.end(resp.toString());
};

// When user replies to notification number,
// lets user know via text that he shouldn't
exports.noCalls= function(req,res) {
  var resp = new twilioModule.TwimlResponse();
  resp.say({voice:'woman'}, 'Hello from Tasky! This number is not our support line. In a moment we\'ll text you the number to Tasky support.');
  resp.sms("TASKY.ME: You can reach Tasky support at 1-877-777-57 38");

  res.writeHead(200, {
    'Content-Type':'text/xml'
  });
  res.end(resp.toString());
};

/**
 *
 * @param requestId: ID of request to populate category and requestor with
 * @param pros: Array of pros
 * @returns Promise resolved with array of
 * {
 *  value: twilio response value
 *  status: Twilio return status
 * }
 */
exports.newLead = function(requestId, pros) {
  _.remove(pros, function(pro){
    var noPhone = !pro.notificationPhone;
    var doesntWantTexts = pro.preferences.text.newLead === false;
    var notActivated = !pro.status.active;

    return noPhone || notActivated || doesntWantTexts
  });

  if (!pros.length) {
    console.warn('WARNING: request %s was SMS notified to 0 pros', requestId);
    return Promise.resolve(false);
  }

  return Request.findById(requestId)
    .populate('category requested_by').execAsync()
    .then(function(reqObj) {
      var cust = reqObj.requested_by;
      var messagePromises = [];
      pros.forEach(function(pro) {
        messagePromises.push (twilio.sendMessage({
          to: pro.notificationPhone,
          from: taskyPhone,
          body: 'TASKY.ME: New lead from ' + cust.firstName + ' ' + cust.lastName[0] + ". for a " + reqObj.category.name + ' project!\n' +
            'To review the request and send a quote, login to tasky.me'
        }));
      });

      return Promise.settle(messagePromises)
        .then(function(promiseInspectors) {
          var result = [];
          promiseInspectors.forEach(function(p) {
            result.push({
              value: p.isFulfilled() ? p.value() : p.reason(),
              status: p.isFulfilled() ? 'OK' : 'FAILED'
            });
          });

          return result;
        });
    });

};

// Send a text to user X when the first Message was sent on a quote
exports.newMessage = function(fromUser, toUserId, msg) {
  var myName = fromUser.name || fromUser.firstName;
  User.findById(toUserId).lean().execAsync().then(function(toUser) {
    if (toUser.preferences.text.newMessage === true && toUser.notificationPhone) {
      twilio.sendMessage({
        to: toUser.notificationPhone,
        from: taskyPhone,
        body:  "TASKY.ME: " + myName + ' has started a conversation on your quote: \n' +
          msg.message + '\n' +
          'To reply, login to tasky.me'
      });
    }
  })
    .error(function(err){
      console.error('Could not send notification text:', err);
    })
    .catch(function(err) {
      console.error('Could not send notification text:', err);
    });
};

// Send a text to customer X when a pro sends a quote
exports.newQuote = function(fromPro, toCustomerId, request) {
  Request.findById(request).populate('category').lean().execAsync()
    .then(function(r) {
      Customer.findById(toCustomerId).lean().execAsync()
        .then(function(customer){
          if (customer.preferences.text.newQuote === true && customer.notificationPhone) {
            twilio.sendMessage({
              to: customer.notificationPhone,
              from: taskyPhone,
              body: 'TASKY.ME: You have just received a new quote for your ' + r.category.name + ' project!'
            },function(err, responseData) {
              if (err) {
                console.error(err);
              }
            });
          }
        });
    });
};


function handleError(res, err) {
  return res.send(500, err);
}