'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var EventSchema = new Schema({
  event: {
    type: String,
    required: true,
    enum:[
      'pro-viewed-request',
      'customer-viewed-quote',
      'quote-reminder-sent'
    ]
  },
  data: Schema.Types.Mixed,
  date: {
    type: Date,
    default: Date.now
  }
});


exports.Event = EventSchema;

module.exports = exports;