'use strict';

var scheduler = require('./index')();
var Pro = require('../api/user/pro/pro.model');
var Request = require('../api/request/request.model');
var customerMailer = require('../api/mail/customer-mail.controller.js');
var Promise = require('bluebird');
var moment = require('moment');
var _ = require('lodash');

registerOldRequestRemoval();
registerQuoteReminders();


function registerOldRequestRemoval() {

  var removeOldRequests = function(dbDoc, event) {
    /* Get all Last Months Requests older than a week */
    var lastWeek = moment().subtract(1,'week').toDate();
    var lastMonth = moment().subtract(1, 'month').toDate();

    Request.find({
      date_created: {
        $gte: lastMonth,
        $lte: lastWeek
      }
    }).populate('quotes')
      .lean().execAsync()
      .then(function(requests) {
        console.log('Removing Stale requests for %d requests', requests.length);
        // For Each request
        _.forEach(requests, function(request) {
          // Find all pros who received it
          Pro.findAsync({
            incoming_requests: request._id
          })
            .then(function(pros) {
              // For each pro
              _.each(pros, function(pro){
                // If hired_pro isn't this pro, delete from incoming
                var proQuote = _.find(request.quotes, function(quote) { return quote.from == pro._id.toString(); } );
                if (request.hired_pro != pro._id && !proQuote) {
                  console.log('Removing request %s from pro %s', request._id, pro.email);
                  pro.update({
                    $pull: { incoming_requests: request._id }
                  }, function (err) {
                    if (err) {
                      return console.error(err, 'Failed to remove for %s', pro.email);
                    }
                  });
                }
              });
            });
        });
      });
  };


  var oldRequestsEvent = {
    name: 'remove-old-requests',
    cron: '0 4 * * *' // every night at 4am
  };

  console.log('Scheduling Removal of Stale Requests From incoming_requests (Older than a week in the last month) ');
  scheduler.schedule(oldRequestsEvent, function(err) {
    if (err) console.error('error scheduling removal of old requests', err);
  });

  scheduler.on('remove-old-requests', removeOldRequests);
}

function registerQuoteReminders() {
  var sendQuoteReminderEmails = function() {
    var lastWeek = moment().subtract(7,'days').toDate();
    var oneDayAgo = moment().subtract(1,'days').toDate();
    // Get all active requests in the past 2 days
    Request.find({
      status: 'active',
      date_created: {
        $gte: lastWeek, // At most one week old
        $lte: oneDayAgo  // At least one day old
      },
      'events.event' : { $ne: 'quote-reminder-sent'},
      $where: "this.quotes.length > 1" //has at least 1 quote
    })
      .populate('quotes category requested_by')
      .execAsync()
      .then(function(requests) {
        _.forEach(requests, function(r) {
          customerMailer.sendQuoteReminder(r)
            .then( function() {
              r.events.push({ event: 'quote-reminder-sent'});
              r.save();
            });
        });
      });
  };

  var quoteReminderEvent = {
    name: 'quote-reminder-email',
    cron: '*/15 * * * *' // every 15 minutes
  };

  console.log('Scheduling Quote Reminder Emails');
  scheduler.schedule(quoteReminderEvent, function(err) {
    if (err) console.error('error scheduling quote reminder emails', err);
  });

  scheduler.on('quote-reminder-email', sendQuoteReminderEmails);
}