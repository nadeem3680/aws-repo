'use strict';

var Scheduler = require('mongo-scheduler');
var scheduler;

module.exports = function(mongooseConn) {
  if (typeof mongooseConn !== 'undefined') {
    scheduler = new Scheduler(mongooseConn);

    mongooseConn.once('open', function() {
      // Add scheduled Tasks beneath
      require('./credits');
      require('./requests');
    });
  }

  return scheduler;
};