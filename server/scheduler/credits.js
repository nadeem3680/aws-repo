'use strict';

var scheduler = require('./index')();
var Pro = require('../api/user/pro/pro.model');
var Request = require('../api/request/request.model');
var Quote = require('../api/quote/quote.model');
var proMailer = require('../api/mail/pro-mail.controller.js');
var Promise = require('bluebird');
var moment = require('moment');
var _ = require('lodash');

registerQuoteCreditRefunds();



function registerQuoteCreditRefunds() {
  var refundUnseenQuotes = function(dbDoc, event) {
    console.log('Checking quotes in the last 3 days for refunds...');
    var threeDaysAgo = moment().subtract(3,'days').toDate();
    Quote.findAsync({
        refunded: false,
        sent_at: { $gte: threeDaysAgo}}
    )
      .then(function(quotes) {
        // Only keep overdue quotes which haven't been seen
        quotes = _.filter(quotes, function(q) {
          var overdue = moment(q.sent_at).diff(Date.now(), 'hours') < -48;
          var notSeen =  _.keys(q.last_message_seen).length < 2;
          return overdue & notSeen;
        });

        if (!quotes.length) {
          console.log('No overdue quotes in the past 3 days');
          return;
        }

        console.log('%d quotes need to be refunded', quotes.length);

        _.forEach(quotes, function(quote) {
          console.log('Refunding quote %s for request %s', quote._id, quote.request);
          var requestPromise = Request.findOne({ _id: quote.request })
            .populate('category')
            .execAsync();

          var proPromise = Pro.findOneAsync({ _id: quote.from });

          Promise.join(requestPromise, proPromise, function(request, pro) {
            var category = request.category;
            var credits = pro.credits;

            if (!category) {
              console.warn('WARNING: Request %s does not have a valid category', request._id);
              return;
            }


            var refundText = 'refund ' + pro.email + ' for '+ category.credits_required + ' credits';
            pro.credits += category.credits_required;
            pro.save(function(err, pro) {
              if (err) {
                throw new Error('Could not ' + refundText);
              }
              console.log('Successful ' + refundText);
            });

            quote.refunded = true;
            quote.save(function(err, quote) {
              if (err) {
                throw new Error('Error when setting refund flag: %s', refundText);
              }
              console.log('Successfuly set refund Flag for %s\n Pro now has %d credits **', pro.email, pro.credits);
              proMailer.refunded(request, quote, pro)
                .error(function(err){
                  console.error('Problem sending refund notification email: %s', require('util').inspect(err));
                });
            });
          })
            .error(function(err) {
              console.error('Problem with scheduled event', err);
            })
            .catch(function(err) {
              console.error('Problem with scheduled event', err);
            });
        });
      });
  };


  var quoteEvent = {
    name: 'credit-refund',
    cron: '*/15 * * * *' // every 15 minutes (0,15,30,45)
  };

  console.log('Scheduling Quote Refunds (Checking every 15 Minutes)');
  scheduler.schedule(quoteEvent, function(err, jobObj) {
    if (err) console.error('error scheduling Quote Refunds', err);
  });
  scheduler.on('credit-refund', refundUnseenQuotes);
}