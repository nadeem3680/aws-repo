'use strict';

var fs = require('fs');
var args = require('yargs')
  .boolean('create')
  .boolean('up')
  .boolean('down')
  .demand(1)
  .usage('Usage: $0 --[create | up | down] filename\n'+
   '    action: one of --create, --up or --down\n'+
   '    name: the name of the migration file')
  .argv;

var fileName = args._[0],
  path = '../migrations/',
  prefixedFileName = Date.now() + '_' + fileName + '.js',
  filePath = path  + prefixedFileName;

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "development";
}

if (args.create) {
  fs.createReadStream('../migrations/templates/template.js')
    .pipe(fs.createWriteStream(filePath))
    .on('close', function(err) {
      if (err) {
        console.error('There was a problem creating the migration.');
        return -1;
      }
      console.log('Successfully create migration %s', fileName);
  });
  return 0;
}

var db = require('../migrations/config/db');
var migratedPath = path + '.migrated';

if (args.up) {
  require(fileName).up(function(upErr){
    if (upErr) {
      console.error('Migration Failed: ', upErr);
      process.exit(-1);
    }
    fs.appendFile(migratedPath,
    '  UP:  ' + fileName.replace('.js','') + '\n',
      function(writeErr){
        if (writeErr) {
          console.error('Could not migrate UP %s', fileName, writeErr);
          process.exit(-1);
        }
        console.log('Migrated UP %s', fileName);
        process.exit(0);
      }
    );
  });
}

if (args.down) {
  require(fileName).down(function(downErr){
    if (downErr) {
      console.error('Migration Failed: ', downErr);
      process.exit(-1);
    }

    fs.appendFile(migratedPath,
        'DOWN:  ' + fileName.replace('.js','') + '\n',
      function(writeErr){
        if (writeErr) {
          console.error('Could not migrate DOWN %s', fileName, writeErr);
          process.exit(-1);
        }
        console.log('Migrated DOWN %s', fileName);
        process.exit(0);

      }
    );
  });
}
