'use strict';

var models = require('./config/db').model;
var Pro = models('Pro');
var Promise = require('bluebird');
var _ = require('lodash');

function updatePros(value, callback) {
	Pro.findAsync({ _accountType: 'Pro' })
	.error(function (err) {
		console.log('Unable to retrieve pros [', err.message , ']');
	})
	.then(function (pros) {
		var update = [];
		console.log('Updating professionnals to:', value);
		_.forEach(pros, function (pro) {
			update.push(
				Pro.updateAsync(
					{ _id: pro._id },
					value,
					{upsert: true}
				)
			);
		});
		return Promise.all(update);
	})
	.then(function () {
		console.log('Pros updated successfully.');
		callback();
	})
	.error(function (err) {
		console.log('Unable to update pros [', err.message, ']');
		callback(err);
	});
}

module.exports.up = function(done) {
	updatePros({
		status: {
			active: true,
			tutorial: true
		}
	}, done);
};

module.exports.down = function(done) {
	updatePros({
		status: {
			active: false,
			tutorial: false
		}
	}, done);
};