'use strict';

var models = require('./config/db').model;
var User = models('User');

module.exports.up = function(done) {
  User.create({
      provider: 'local',
      role: 'admin',
      name: 'Admin Anghezi',
      email: 'admin@tasky.me',
      password: 't@skyt@sky'
    }
    , function(err){
      console.log('Admin admin@tasky.me created');
      done(err);
    });
};

module.exports.down = function(done) {
  User.remove({
      email: 'admin@tasky.me'
    },
    function(err){
      console.log('Admin admin@taskyme Removed');
      done(err);
    });
};