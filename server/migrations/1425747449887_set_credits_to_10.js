'use strict';

var models = require('./config/db').model;
var Pro = models('Pro');

module.exports.up = function(done) {
  Pro.update({
    _accountType: 'Pro'
  }, {
    credits: 10
  }, { multi: true }, done);
};

module.exports.down = function(done) {
  Pro.update({
    _accountType: 'Pro'
  }, {
    credits: Infinity
  }, { multi: true },
  function(status, numAffected){
    console.log('Number of Pros Affected: %d', numAffected);
    done();
  });
};