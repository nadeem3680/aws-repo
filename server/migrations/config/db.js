'use strict';


var mongoConfig = require('../../config/environment').mongo;
var mongoose = require('mongoose');
var Promise = require('bluebird');
// Promisify all mongoose connections
Promise.promisifyAll(mongoose);
var conn = mongoose.connect(mongoConfig.uri, mongoConfig.options);



require('../../api/user/user.model');
require('../../api/user/pro/pro.model');
require('../../api/user/customer/customer.model');
require('../../api/category/category.model');
require('../../api/payment/payment.model');
require('../../api/request/request.model');
require('../../api/review/review.model');
require('../../api/quote/quote.model');
require('../../api/question/question.model');


module.exports = {
  model: function(modelName) {
    return conn.model(modelName);
  },
  connection: conn
};