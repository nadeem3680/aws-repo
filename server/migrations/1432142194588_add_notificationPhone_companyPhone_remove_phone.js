'use strict';

var models = require('./config/db').model,
  User = models('User'),
  Promise = require('bluebird');

module.exports.up = function(done) {
  User.findAsync({}, {phone: 1, email: 1, _accountType: 1 })
    .then(function(users) {
      var promises = [];
      users.forEach(function(user) {
        console.log('Updating user %s', user.email);
        user.notificationPhone = user.phone;
        if (user._accountType == 'Pro') {
          user.companyPhone = user.phone;
        }

        user.set('phone', undefined);

        promises.push(user.saveAsync());
      });

      Promise.all(promises)
        .error(function(err) {
          console.error(err);
          done('Something went wrong :(');
        })
        .then(function(){
          console.log('Removing phone field');
          User.update({}, {
            $unset: { 'phone': 1 }
          }, { multi: 1 })
            .execAsync()
            .then(function() {
              done();
            })
            .error(function(err) {
              console.error(err);
              done('There was a problem removing "Phone" field.')
            });

        });
    })

};

module.exports.down = function(done) {
  User.updateAsync({},
    {
      $rename: { 'notificationPhone': 'phone' },
      $unset: { companyPhone: 1 }
    }, { multi: true, safe: true, strict: false})
    .then(function() {
      done();
    })
    .error(done);

};