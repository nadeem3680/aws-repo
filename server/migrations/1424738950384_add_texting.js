'use strict';

var models = require('./config/db').model;
var User = models('User');
var Pro = models('Pro');

module.exports.up = function(done) {
  User.updateAsync({},{
    $set: {
      'preferences.text' : {
        newQuote: true
      }
    }
  },{
    multi: true
  }).then(function(){
    return Pro.updateAsync({ _accountType: 'Pro' }, {
        $set: {
          'preferences.text.newLead': true,
          'preferences.text.newMessage': true
        }
      },
      {
        multi: true
      });
  })
    .then(function(out1, out2){
      console.log(out1,out2);
      done();
    })
    .error(done);
};

module.exports.down = function(done) {
  User.updateAsync({},{
      $unset: {
        'preferences.text': true
      }
    },
    {
      multi: true
    })
    .then(function(){
      done();
    })
    .error(done);
};