'use strict';

exports.isNewRoot = false;

exports.root ={
  name:"Garage door services",
  parent:null
};

exports.children = [
  require('./Garage-door-installation'),
  require('./Garage-door-repair')
];