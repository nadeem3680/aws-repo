module.exports = function(parentId) {
  return {
  name: 'Electrical Fixtures',
  route: 'fixtures',
  search_keywords: ['fixtures' ,'elec', 'Chandelier', 'lights', 'Circuit','Ceiling fan','fan','Chandelier','Chandelier Installation','Chandelier replacement','removing Chandelier','fan replacement','ceiling fan replacement','ceiling fan Installation','Circuit replacement','floor lamps','modern lighting','outdoor lighting','fan exhaust','bathroom exhaust replacement','electrical','appliance'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Electrician',
        actor_plural: 'Electricians',
        action: 'help you fix your electrical problems',
        questions:  [
        {
          field_type:"checklist",
          question:"What needs to be done?",
          description:"What needs to be done",
          choices:[
          {
              label:"Installation",
              value:"Installation"
          },
          {
            label:"Complete replacement",
            value:"Complete replacement"
          },
          {
            label:"Move fixture(s) to a different location",
            value:"Move fixture(s) to a different location"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:"What kind of fixture",
          description:"Kind of fixture",
          choices:[
          {
            label:"Interior lights",
            value:"Interior lights"
          },
          {
            label:"Exterior lights",
            value:"Exterior lights"
          },
          {
            label:"Outdoor lights",
            value:"Outdoor lights"
          },
          {
            label:"Ceiling fan",
            value:"Ceiling fan"
          },
          {
            label:"Chandelier",
            value:"Chandelier"
          },
          {
            label:"Circuit",
            value:"Circuit"
          },
          {
            label:"Bath or attic exhaust fan",
            value:"Bath or attic exhaust fan"
          },
          {
            can_describe:true
          },



          ]
        }
        ]
        };
      };