'uses strict';

exports.isNewRoot=false;

exports.root={
  name:"Electrical",
  parent:null
};

exports.children=[
  require('./electrical-and-wiring'),
  require('./lamp-installation'),
  require('./spa-and-hot-tub-installation-and-repair'),
  require('./switch-outlet-repair-and-installation'),
  require('./wiring'),
  require('./ceiling-fan'),
  require('./fan-installation'),
  require('./fan-repair'),
  require('./fixtures')
];