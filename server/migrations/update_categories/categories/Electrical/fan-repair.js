module.exports = function(parentId) {
  return {
  name: 'Fan Repair',
  route: 'fan-repair',
  search_keywords: ['fan' ,'elec', 'fan repair','Electrician','appliance','bath exhaust repair','Ceiling fan','bath exhaust repair','bathroom exhaust','bathroom exhaust repair','bathroom exhaust repairs','ceiling fan repair','celing fan repair','ceiling fan repairs','Wall-mounted fan','Wall-mounted fan repair','Wall-mounted Wall-mounted replacement','ceiling fan replacement','bathroom fan exhaust replacement','exhaust replacement','kitchen exhaust replacement','Attic fan','attic fan repair','kichen exhaust','kitchen fan','kitchen exhaust repair','electric repair','bathroom','bathroom fan exhaust','wall fan','industrial fan','fan fix','electrical repair','kitchen hood','kitchen hood repair'],
        parent:  parentId, 
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Electrician',
        actor_plural: 'Electricians',
        action: 'help you fix your electrical problems',
        questions:  [
       {
          field_type:'checklist',
          question:"What do you need done?",
          description:"What needs to be done",
          choices:[
          {
            label:"Repair",
            value:"Repair"
          },
          {
            label:"Replace",
            value:"Replace"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:"What kind of fan?",
          description:"Kind of fan",
          choices:[
          {
            label:"Ceiling",
            value:"Ceiling"
          },
          {
            label:"Bath exhaust",
            value:"Bath exhaust"
          },
          {
            label:'Kitchen hood',
            value:'Kitchen hood'
          },
          {
            label:"Whole house fan",
            value:"Whole house fan"
          },
          {
            label:"Attic",
            value:"Attic"
          },
          {
            label:'Window',
            value:"Window"
          },
          {
            label:"Wall-mounted",
            value:"Wall-mounted"
          },
          {
            label:"Floor-standing/Box",
            value:"Floor-standing"
          },
          {
            can_describe:true
          }

          ]
        },
        {
          field_type:"checklist",
          question:"Where will the work take place?",
          description:"The work will take place..",
          choices:[
          {
            label:"Indoors",
            value:"Indoors"
          },
          {
            label:"Outdoors",
            value:"Outdoors"
          }
          ]
        }
        ]
      };
    };
       