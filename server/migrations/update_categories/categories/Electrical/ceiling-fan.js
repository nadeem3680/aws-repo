module.exports = function(parentId) {
  return {
  name: 'Ceiling Fan',
  route: 'Ceiling Fan',
  search_keywords: ['Ceiling Fan' ,'Fan', 'Ceiling','ceiling fan isntallation','ceiling fan replacement','appliance repair','appliance installation','fan repairs','pullstring ceilling fan','pull string fan','electricia','appliance'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Electrician',
        actor_plural: 'Electricians',
        action: 'help you fix your electrical problems',
        questions:  [
        {
          field_type:'checklist',
          question:"What do you need done?",
          description:"What needs to be done",
          choices:[
          {
            label:"Install",
            value:"Install"
          },
          {
            label:"Repair",
            value:"Repair"
          },
          {
            label:"Replace",
            value:"Replace"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:"What kind of ceiling fan",
          description:"The kind of ceiling fan ",
          choices:[
          {
            label:"Standard indoor",
            value:"Standard indoor"
          },
          {
            label:"Hugger",
            value:"Hugger"
          },
          {
            label:"Outdoor",
            value:"Outdoor"
          },
          {
            label:"Commercial",
            value:"Commercial"
          },
          {
            label:"I am not sure",
            value:"Customer is not sure"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:"How is the ceiling fan controlled?",
          description:"how the fan is controlled",
          choices:[
          {
            label:"Pull string",
            value:"Pull string"
          },
          {
            label:"Wall switch",
            value:"Wall switch"
          },
          {
            label:"Remote control",
            value:"Remote control"
          },
          {
            label:"I am not sure",
            value:"Customer is not sure"

          },
          {
            can_describe:true
          }

          ]
        }
        ]
      };
    };