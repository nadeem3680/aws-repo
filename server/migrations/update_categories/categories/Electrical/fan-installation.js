module.exports = function(parentId) {
  return {
  name: 'Fan Installation',
  route: 'fan-installation',
  search_keywords: ['fan Installation','fan' ,'elec', 'fan repair','Electrician','appliance','bath exhaust Installation','Ceiling fan','bath exhaust Installation','bathroom exhaust','bathroom exhaust Installation','bathroom exhaust repairs','ceiling fan Installation','celing fan repair','ceiling fan Installation','Wall-mounted fan','Wall-mounted fan Installation','Wall-mounted Wall-mounted replacement','ceiling fan replacement','bathroom fan exhaust replacement','exhaust replacement','kitchen exhaust replacement','Attic fan','attic fan Installation','kichen exhaust','kitchen fan','kitchen exhaust Installation','electrical Installation','bathroom','bathroom fan exhaust','wall fan','industrial fan','fan fix','electrical repair','kitchen hood','kitchen hood Installation'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Electrician',
        actor_plural: 'Electricians',
        action: 'help you fix your electrical problems',
        questions:  [
        {
          field_type:'checklist',
          question:"What do you need done?",
          description:"What needs to be done",
          choices:[
          {
            label:"Installation",
            value:"Installation"
          },
          {
            label:"Replace",
            value:"Replace"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:"What kind of fan?",
          description:"Kind of fan",
          choices:[
          {
            label:"Ceiling",
            value:"Ceiling"
          },
          {
            label:"Bath exhaust",
            value:"Bath exhaust"
          },
          {
            label:"Whole house fan",
            value:"Whole house"
          },
          {
            label:"Attic",
            value:"Attic"
          },
          {
            label:'Window',
            value:"Window"
          },
          {
            label:"Wall-mounted",
            value:"Wall-mounted"
          },
          {
            label:"Floor-standing/Box",
            value:"Floor-standing"
          },
          {
            can_describe:true
          }

          ]
        },
        {
          field_type:"checklist",
          question:"Where will the work take place?",
          description:"The work will take place..",
          choices:[
          {
            label:"Indoors",
            value:"Indoors"
          },
          {
            label:"Outdoors",
            value:"Outdoors"
          }
          ]
        }
        ]
      };
    };
          