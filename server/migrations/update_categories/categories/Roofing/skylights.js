module.exports = function(parentId) {
  return {
    name: 'Skylights Installations & Repair',
    route: 'skylights-installation-and-repair',
    search_keywords: ['skylights' , 'roofer', 'Roof', 'skylight installation', 'skylight repair','window','window installation','door installation','roofing','craftsman','craftsperson','contractor','handyman','contractors','skylights repair','skylights cost','sky','roofing companies','dome skylights','dome skylight','dome','sky','light','sky light'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Roofer',
    actor_plural: 'Roofers',
    action: 'fix your roof',
    questions: [
      {
        field_type:"select",
        question:"What needs to be done?",
        description:"Project type",
        choices:[
        {
            label:"Make new opening and install",
            value:"Make new opening and install"
        },
        {
            label:"Replace",
            value:"Replace"
        },
        {
            label:"Repair",
            value:"Repair"
        }
        ]
    },
    {
        field_type: "select",
        question: 'How steep is your that part of the roof that needs skylight work?',
        description: "Steepness of roof",
        choices: [
          {
            label: "All flat",
            value: "All flat"
          },
          {
            label: "Some parts steep, some parts flat",
            value: "Some parts steep, some parts flat"
          },
          {
            label: "All steep",
            value: "All steep"
          }
        ]
      },
      {
        field_type:"select",
        question:"How tall is the building?",
        description:"How tall the building is",
        choices:[
        {
            label:"Single level",
            value:"Single level"
        },
        {
            label:"Two story tall",
            value:"Two story tall"
        },
        {
            label:"Taller",
            value:"Taller"
        }
        ]
      },
      ]
  };
};