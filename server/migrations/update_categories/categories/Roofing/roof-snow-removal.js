module.exports = function(parentId) {
  return {
    name: 'Roof Snow Removal',
    route: 'Flat-Installation',
    search_keywords: ['Roofing' , 'roofer', 'Roof', 'Roof installation', 'roof Maintenance','roofing companies','gutter','flat','flat roof','snow removal','snow plowing','snow','roof removal','dam roof','ice dam','roof rakes','ice damming','roof shoveling','roof ice','ice on roof','roof ice removal'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Roofer',
    actor_plural: 'Roofers',
    action: 'fix your roof',
    questions: [
      {
        field_type:"text",
        question:"What is the approximate square footage of your roof?",
        description:"The approximate square footage of the roof"

        
    },
    {
        field_type: "select",
        question: "What material is your roof?",
        description: "Roof material",
        choices: [
          {
            label: "Asphalt shingles",
            value: "Asphalt shingles"
          },
          {
            label: "Wood shingles",
            value: "Wood shingles"
          },
          {
            label: "Slate",
            value: "Slate"

          },
          {
            label: "Clay/Concrete",
            value: "Clay/Concrete"
          },
          {
            label: "Metal",
            value: "Metal"
          },
          {
            label: "I am not sure",
            value: "Customer is not sure"
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};