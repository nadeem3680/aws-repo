'use strict';

exports.isNewRoot = false;

exports.root = {
  name: "Roofing",
  parent: null
};

exports.children = [
  require('./roof-repair-and-maintenance'),
  // New
  require('./roof-installation'),
  require('./roof-repair-and-maintenance'),
  require('./roof-snow-removal'),
  require('./skylights')
];