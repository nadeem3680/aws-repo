module.exports = function(parentId) {
  return {
    name: 'Roof Installation',
    route: 'roof-installation',
    search_keywords: ['Roofing' , 'roofer', 'Roof', 'Roof installation', 'roof Maintenance','contractor','handyman','general contractor','framing','carpentry','home remodeling','vent installation','shingle roof installation','flat roof','intall roll','install cedar','roof Asphalt','Asphalt roof','metal roofing','steel roofing','roof'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Roofer',
    actor_plural: 'Roofers',
    action: 'fix your roof',
    questions: [
      {
        field_type: "checklist",
        question: "What kind of roofing installation or replacement do you need?",
        description: "Type of project",
        choices: [
        {
            label:"Install new roof on building that has no roof",
            value:"Install new roof on building that has no roof"
        },
        {
            label:'Replace by removing old roof with a new one',
            value:"Replace by removing old roof with a new one"
        },
        {
            label:"Replace by adding another layer on top the old roof",
            value:"Replace by adding another layer on top the old roof"
        },
        {
            label:"As recommended by professional",
            value:"As recommended by you"
        }
        ]
    },
    {
        field_type:"checklist",
        question:"Why do you need to replace the roof?",
        description:"Reason for replacing the roof",
        choices:[
        {
            label:"Roof is leaking",
            value:"Roof is leaking"
        },
        {
            label:"Roof is moldy",
            value:"Roof is moldy"
        },
        {
            label:"Roof is sagging",
            value:"Roof is sagging"
        },
        {
            label:"Roof is cracked",
            value:"Roof is cracked"
        },
        {
            label:"Roof lifespan is over",
            value:"Roof lifespan is over"
        },
        {
            label:'Complete renovation',
            value:"Complete renovation"
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:"text",
        question:"What is the approximate square footage of your roof?",
        description:"The approximate square footage of the roof"

        
    },
    {
        field_type: "select",
        question: "What material is your roof?",
        description: "Roof material",
        choices: [
          {
            label: "Asphalt shingles",
            value: "Asphalt shingles"
          },
          {
            label: "Wood shingles",
            value: "Wood shingles"
          },
          {
            label: "Slate",
            value: "Slate"

          },
          {
            label: "Clay/Concrete",
            value: "Clay/Concrete"
          },
          {
            label: "Metal",
            value: "Metal"
          },
          {
            label: "I am not sure",
            value: "Customer is not sure"
          }
        ]
      },
      {
        field_type: "select",
        question: "How old is the current roof in years?",
        description: "The approximate age of roof in years",
        choices: [
          {
            label: "0-10",
            value: "0-10"
          },
          {
            label: "11-20",
            value: "11-20"
          },
          {
            label: "21-30",
            value: "21-30"
          },
          {
            label: "31-40",
            value: "31-40"
          },
          {
            label: "More than 40",
            value: "More than 40"
          },
          {
            label: "I am not sure",
            value: "Customer is not sure"
          }
        ]
      },
      {
        field_type: "select",
        question: 'How steep is your roof?',
        description: "Steepness of roof",
        choices: [
          {
            label: "All flat",
            value: "All flat"
          },
          {
            label: "Some parts steep, some parts flat",
            value: "Some parts steep, some parts flat"
          },
          {
            label: "All steep",
            value: "All steep"
          }
        ]
      },
      {
        field_type: "select",
        question: "Do you have an insurance claim on this job?",
        description: "Insurance claim",
        choice: [
          {
            label: "Yes",
            value: "Yes"
          },
          {
            label: "No",
            value: "No"
          }
        ]
      },
      {
        field_type: "select",
        question: "Will you supply the materials and parts?",
        description: "Who supplies the materials and parts",
        choices: [
          {
            label: "Yes, I will provide the materials and parts",
            value: "Customer will provide the materials and parts"
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide parts and material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the parts",
            value: "Customer wants you to supply the parts"
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};