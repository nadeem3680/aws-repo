module.exports = function(parentId) {
  return {
    name: 'Bed Bug Extermination',
    route: 'bed-bug-extermination',
    search_keywords: [ 'pest', 'pest removal','bedbugs','bed bugs','bed bugs extremination',' cockroach', 'ants','Bees','Pest removalists','Rodents removal','cockroach removal','Termites removal','hornets removal','Fleas Removal','Ticks Remvoal','treatment','bed','remove bed','control','registry','bug detection','detection','Inspection','heat','heat bed','spray','killer','remedies'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Pest removalist',
    actor_plural: 'Pest Removalists',
    action: 'help remove pests',
    questions: [
    {
        field_type:'checklist',
        question:'What services do you need?',
        description:'Services needed',
        choices:[
        {
            label:'Inspection',
            value:'Inspection'
        },
        {
            label:'Removal or extermination',
            value:'Removal or extermination'
        },
        {
            label:'Recommendations on preventative measures',
            value:'Recommendations on preventative measures'
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:'select',
        question:'How many rooms are affected?',
        description:'Number of that are affected',
        choices:[
        {
            label:'One',
            value:'One'
        },
        {
            label:'Two',
            value:'Two'
        },
        {
            label:'Three',
            value:'Three'
         },
         {
            label:'Four',
            value:'Four'
         },
         {
            label:'Five',
            value:'Five'
         },
         {
            label:'More than five',
            value:'More than five'
         }
        ]
    },
    {
        field_type:'select',
        question:'How many beds are in the affected rooms?',
        description:'Number of beds in the affected rooms',
        choices:[
        {
            label:'One',
            value:'One'
        },
        {
            label:'Two',
            value:'Two'
        },
        {
            label:'Three',
            value:'Three'
         },
         {
            label:'Four',
            value:'Four'
         },
         {
            label:'Five',
            value:'Five'
         },
         {
            label:'More than five',
            value:'More than five'
         }
        ]
    },
    {
        field_type:'select',
        question:'How many couches are in the affected rooms?',
        description:'Number of couches in the affected rooms',
        choices:[
        {
            label:'One',
            value:'One'
        },
        {
            label:'Two',
            value:'Two'
        },
        {
            label:'Three',
            value:'Three'
         },
         {
            label:'Four',
            value:'Four'
         },
         {
            label:'Five',
            value:'Five'
         },
         {
            label:'More than five',
            value:'More than five'
         }
        ]
    },
    {
        field_type:'select',
        question:'If needed, are you able to vacate the property while services are being performed?',
        description:'If need, is the customer able to vacate the property while services are being performed',
        choices:[
        {
            label:'Yes',
            value:'Yes'
        },
        {
            label:'No',
            value:'No'
        }
        ]
    },
    {
        field_type: 'text',
        question: 'What is the approximate square footage of your property?',
        description:"The approximate square footage of your property"
    }
    ]
};
};
