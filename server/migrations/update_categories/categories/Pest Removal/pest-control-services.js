module.exports = function(parentId) {
  return {
    name: 'Pest Control Services',
    route: 'pest-control-services',
    search_keywords: [ 'pest', 'pest removal','bedbugs','bed bugs',' cockroach', 'ants','Bees','Pest removalists','Rodents removal','cockroach removal','Termites removal','hornets removal','Fleas Removal','Ticks Remvoal','squirrel','bug pest','squirrel traps','traps','control mice','ant pest','mice','extermination','Termites control','Termites treatment','rats','Rodents control','control','rodents','fumigation','repellent','mouse','ultrasonic','skunk'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Pest Removalist',
    actor_plural: 'Pest Removalists',
    action: 'help remove pests',
    questions: [
    {
        field_type:'checklist',
        question:'Which pests?',
        description:'What pests',
        choices:[
        {
            label:'Cockroach',
            value:'Cockroach'
        },
        {
            label:'Bees',
            value:'Bees'
        },
        {
            label:'Ants',
            value:'Ants'
        },
        {
            label:'Rodents',
            value:'Rodents'
        },
        {
            label:'Hornets or Wasps',
            value:'Hornets or Wasps'
        },
        {
            label:'Fleas, Ticks or Mites',
            value:'Fleas,Ticks or Mites'
        },
        {
            label:'Termites',
            value:'Termites'
        },
        {
            label:'Squirrel',
            value:'squirrel'
        },
        {
            label:'Skunk',
            value:'Skunk'
        },
        {
            label:'Raccoon',
            value:'Raccoon'
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:'checklist',
        question:'What services do you need?',
        description:'Services needed',
        choices:[
        {
            label:'Inspection',
            value:'Inspection'
        },
        {
            label:'Removal or extermination',
            value:'Removal or extermination'
        },
        {
            label:'Recommendation or preventative measures',
            value:'Recommendation or preventative measures'
        },
        {
            can_describe:true
        }

        ]

    },
    {
        field_type:'checklist',
        question:'What areas are affected?',
        description:'Areas that are affected',
        choices:[
        {
            label:'Yard',
            value:'Yard'
        },
        {
            label:'Basement',
            value:'Basement'
        },
        {
            label:'Kitchen',
            value:'Kitchen'
        },
        {
            label:'Attic',
            value:'Attic'
        },
        {
            label:'Living room',
            value:'Living room'
        },
        {
            label:'Bathroom',
            value:'Bathroom'
        },
        {
            label:'Bedroom',
            value:'Bedroom'
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type: 'text',
        question: 'What is the approximate square footage of your property?',
        description:"The approximate square footage of your property"
    }
    ]
};
};