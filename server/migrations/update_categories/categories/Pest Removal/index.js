'use strict';

exports.isNewRoot=true;

exports.root = {
  name: "Pest Services",
  parent: null
};

exports.children = [
  require('./bed-bugs-extermination'),
  require('./pest-control-services')
];