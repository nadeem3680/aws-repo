'use strict';

exports.isNewRoot=false;

exports.root={
  name:"Furnace and Heating System",
  parent:null
};

exports.children=[
  require('./furnace-and-heating-installation'),
  require('./furnace-and-heating-repair'),
  // New
  require('./fireplace-repair'),
  require('./fireplace-installation'),
  require('./gas-line-installation-repair'),
  require('./chimney-cleaning'),
  require('./chimney-repair')
];