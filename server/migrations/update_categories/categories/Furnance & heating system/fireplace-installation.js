module.exports = function(parentId) {
  return{
  name: 'Fireplace Installation',
  route: 'fireplace-installation',
  search_keywords: ['fireplace' ,'fireplace installation','hvac','hvac contractor','chimney','furnace','moden fireplace','electric insert','electric fireplace inserts','outdoor fireplaces','dimplex fireplaces','heating system','heating','valor fireplace','gas fireplace installation','electric fireplace installation','electric fireplace','fires','napeleon fireplaces','fireplace mantels','fireplace mantel','pellet stove','wood pellet stove','pellet stoves'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'fireplace specialist',
        actor_plural: 'fireplace specialists',
        action: 'help you with your fireplace installation',
        questions:  [
        {
          field_type:'select',
          question:"What kind of fireplace do you have?",
          description:"Type of fireplace",
          choices:[
          {
            label:"Gas burning with direct vent",
            value:"Gas burning with direct vent"
          },
          {
            label:'Gas burning with no vent',
            value:'Gas burning with no vent'
          },
          {
            label:"Wood burning",
            value:"Wood burning"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'select',
          question:'Are there existing structures in place for a fireplace installation?',
          description:'Existing structures is place for the work or not',
          choices:[
          {
            label:'Yes',
            value:'Yes'
          },
          {
            label:"No",
            value:'No'

          }
          ]
        },
        {
          field_type:'select',
          question:"What kind of fireplace do you have?",
          description:"Type of fireplace",
          choices:[
          {
            label:"Gas burning with direct vent",
            value:"Gas burning with direct vent"
          },
          {
            label:'Gas burning with no vent',
            value:'Gas burning with no vent'
          },
          {
            label:"Wood burning",
            value:"Wood burning"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'text',
          question:'How many fireplaces need work?',
          description:'Number of fireplaces that work?'
        },
        {
          field_type:'checklist',
          question:'Is the fireplace indoors or outdoors?',
          description:'the fireplace is indoors or outdoors',
          choices:[
          {
            label:'Indoors',
            value:"Indoors"
          },
          {
            label:'Outdoors',
            value:'Outdoors'
          }
          ]
        },
        
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        },
        ]
      };
  };