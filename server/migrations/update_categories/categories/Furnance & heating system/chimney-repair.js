module.exports = function(parentId) {
  return{
  name: 'Chimney Repair',
  route: 'chimney-repair',
  search_keywords: ['chimney' ,'chimney repair','fireplace','vent repair','roofing','roofer','fireplace repair','chimney cleaning','brick repair','brick chimney repair','chimney leaking','chimney leaks','leaking chimney','chimney inspection','chimney repairs','roofing companies','roof','roofing company','chimney crown','chimney crown repair','chimney crown replacement'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'fireplace specialist',
        actor_plural: 'fireplace specialists',
        action: 'help you with your chimney repair',
        questions:  [
        {
          field_type:'select',
          question:'How many chimneys need work?',
          description:'Number of chimneys that need work',
          choices:[
          {
            label:'One',
            value:'One'
          },
          {
            label:'Two',
            value:'Two'
          },
          {
            label:'More than two',
            value:'More than two'
          }
          ]
        },
        {
          field_type:'select',
          question:'What is the fuel source of your fireplace or stove?',
          description:'Fuel source of fireplace or stove',
          choices:[
          {
            label:'Gas',
            value:'Gas'
          },
          {
            label:'Pellet',
            value:'Pellet'
          },
          {
            label:'Wood',
            value:'Wood'
          },
          {
            label:'Coal',
            value:'Coal'
          },
          {
            label:'Oil',
            value:'Oil'
          },
          {
            label:'Other',
            value:'Other'
          }
          ]
        },
       {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};