module.exports = function(parentId) {
  return{
  name: 'Gas line',
  route: 'gasline-installation-and-repair',
  search_keywords: ['gasline','gas line','barbecue','stoves','extending gasline','barbecue gas line','furance','hvac','handyman','barbecue gas','gasline barbecue','oven gas line',' stove gas line','fireplace gas line','replacing gas line','gas line installation','oven','stove','fuel line','gas pipe','copper pipe','fuel line repair','fuel linings','braided fuel line','fuel line fittings','pex tubing','gas line','line bbq','bbq line','gas line for bbq','bbq gas line installation','gas line for stove','installing gas line for stove','propane line','propane gas line','flexible propane gas line'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'gas line specialist',
        actor_plural: 'gas line specialists',
        action: 'help you with your gas line installation and repair',
        questions:  [
        {
          field_type:"checklist",
          question:"What appliance are you installing",
          description:"Appliance to be installed",
          choices:[
          {
            label:"Barbecue",
            value:"Barbecue"
          },
          {
            label:"Stove",
            value:"Stove"
          },
          {
            label:"Oven",
            value:"Oven"
          },
          {
            label:"Furnace or fireplace",
            value:"Furnace or fireplace"
          },
          {
            can_describe:true
          }
          ]
        
        },
        {
          field_type:"checklist",
          question:"What is the project",
          description:"Type of project",
          choices:[
          {
            label:"Existing gas line needs repair",
            value:'Existing gas line needs repair'
          },
          {
            label:"Extending the current gas line",
            value:"Extending the current gas line"
          },
          {
            label:"Installing new gas line",
            value:"Installing new gas line"
          },
          {
            label:"Replacing existing gas line",
            value:"Replacing existing gas line"
          },
          {
            can_describe:true
          }
          ]
        },
       {
          field_type:'checklist',
          question:'Is the fireplace indoors or outdoors?',
          description:'the fireplace is indoors or outdoors',
          choices:[
          {
            label:'Indoors',
            value:"Indoors"
          },
          {
            label:'Outdoors',
            value:'Outdoors'
          }
          ]
        },
        
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        },
        ]
      };
  };
          