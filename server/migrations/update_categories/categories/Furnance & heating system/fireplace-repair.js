module.exports = function(parentId) {
  return{
  name: 'Fireplace Repair',
  route: 'fireplace-repair',
  search_keywords: ['fireplace' ,'fireplace repair','fireplace mantels','hvac contractor','hvac','electrical firepalce repair','gas fireplace','outdoor fireplaces','gas fireplace repair','outdoor fireplaces','modern firepalce repair','stone firepalce','wood fireplace repair','gas insert fireplace','mount fireplace'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'fireplace specialist',
        actor_plural: 'fireplace specialists',
        action: 'help you with your fireplace repair',
        questions:  [
        {
          field_type:'select',
          question:"What kind of fireplace do you have?",
          description:"Type of fireplace",
          choices:[
          {
            label:"Gas burning with direct vent",
            value:"Gas burning with direct vent"
          },
          {
            label:'Gas burning with no vent',
            value:'Gas burning with no vent'
          },
          {
            label:"Wood burning",
            value:"Wood burning"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'text',
          question:'How many fireplaces need work?',
          description:'Number of fireplaces that need work?'
        },
        {
          field_type:'checklist',
          question:'Is the fireplace indoors or outdoors?',
          description:'the fireplace is indoors or outdoors',
          choices:[
          {
            label:'Indoors',
            value:"Indoors"
          },
          {
            label:'Outdoors',
            value:'Outdoors'
          }
          ]
        },
        
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
      };
  };