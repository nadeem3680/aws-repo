module.exports = function(parentId) {
  return{
  name: 'Chimney Cleaning',
  route: 'chimney-cleaning',
  search_keywords: ['chimney' ,'chimney cleaning','fireplace','sweeping','sweep','chimney cleaning sweeper','chimney cleaning power sweeper','power sweeping','duct cleaning','duct','janitorial services','roofing','roofer','chimney sweep','chimney swift','chimney fire','commercial cleaning','chimney flue','chimney flue brushes','flue brushes','chimney','air duct cleaning'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'chimney cleaning specialist',
        actor_plural: 'chimney cleaning specialists',
        action: 'help you with your chimney cleaning',
        questions:  [
        {
          field_type:'select',
          question:'What method of cleaning?',
          choices:[
          {
            label:'Sweeping',
            value:'Sweeping'
          },
          {
            label:'Power sweeping',
            value:'Power sweeping'
          },
          {
            label:'I am not sure',
            value:'Customer is not sure what method cleaning she/he needs'
          }
          ]
        },
        {
          field_type:'select',
          question:'How many stories is your property?',
          description:'How many stories the property is',
          choices:[
          {
            label:'Single level',
            value:'Single level'
          },
          {
            label:'Two story building',
            value:'Two story building'
          },
          {
            label:'Taller than a two story building',
            value:'Taller than a two story building'
          }
          ]
        }
        ]
      };
    };