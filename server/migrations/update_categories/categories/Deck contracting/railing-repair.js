module.exports = function(parentId) {
  return {
    name: 'Railing Fence Banisters Repair',
    route: 'railing-repair',
    search_keywords: ['railing' ,'railing repair', 'deck railing', 'cracked  banisters','cracked railin','stair rail','rail','patio railing','iron railings','patio railings','railings','staircase','iron railings','wood railings','spiral staircase','spiral staircase railings','spiral staircase railing repiar','frame rail repair','deck repair','railings for deck','railings for deck repair','decks','patio','deck rail','deck railings','deck railings repair','patio railings repair','driveway','gate','driveway gate','garden gate','wrought railings','interior wrought iron railings','siding repair','iron fence','fence','wood fence','banisters','banister','stair banisters','case iron fence','wood deck railings','wood stair railing','wood stair railings','wrought iron banister','wrought iron banisters','porch railing','metal railings','metal railing','metal rail'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'deck contractor',
    actor_plural: 'deck contractors',
    action: 'help you repair your railing & banisters',
    questions:  [
    {
        field_type:'checklist',
        question:'What needs repair?',
        description:'What needs repair',
        choices:[
        {
            label:'Interior railings',
            value:'Interior railings'
        },
         {
            label:'Exterior railings',
            value:'Exterior railings'
        },
        {
            label:'Outdoor fence',
            value:'Outdoor fence'
        },
        {
            label:'Driveway gate',
            value:'Driveway gate'
        },
        {
            label:'Garden gate',
            value:'Garden gate'
        },
        {
            label:'Patio railings',
            value:'Patio railings'
        },
        {
            label:'Banisters',
            value:'Banisters'
        },
       
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:"select",
        question:"What is the problem?",
        description:"The problem",
        choices:[
        {
            label:"Cracked or broken railing",
            value:"Cracked or broken railing"
        },
        {
            label:"Cracked or broken banisters",
            value:"Cracked or broken banisters"
        },
        {
            label:"Scratches and scuffs",
            value:"Scratches and scuffs"
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:'select',
        question:"Has the damaged made the staircase or the area around unsafe to use?",
        description:"The damaged is so severe that has made the staircase or the area around unsafe to use",
        choices:[
        {
            label:"Yes",
            value:"Yes"
        },
        {
            label:'The damage is moderate. The area is usable but concerns about safty exist',
            value:"The damage is moderate. The area is usable but concerns about safty exist "
        },
        {
            label:'No, cosmetic damage only',
            value:"No, cosmetic damage only "
        }
        ]
    },
    {
        field_type:"select",
        question:"What kind of railings do you have?",
        description:'Kind of railings',
        choices:[
        {
            label:'Wood',
            value:'Wood'
        },
        {
            label:"Concrete",
            value:"Concrete"
        },
        {
            label:'Wrought iron',
            value:'Wrought iron'
        },
        {
            label:"Glass or acrylic",
            value:"Glass or acrylic"
        },
        {
            label:'Plaster',
            value:"Plaster"
        },
        {
            label:'No railing repair needed',
            value:'No railing repair needed'
        }
        
        ]
    },
    {
        field_type:"select",
        question:"What kind of banister do you have?",
        description:'Kind of banister',
        choices:[
        {
            label:'Wood',
            value:'Wood'
        },
        {
            label:'Wrought iron',
            value:'Wrought iron'
        },
        {
            label:"Glass or acrylic",
            value:"Glass or acrylic"
        },
        {
            label:'Plaster',
            value:"Plaster"
        }
        
        ]
    },
    {
          field_type:"select",
          question:"Will you supply the parts?",
          description:"Who is supplying the parts",
          choices:[
          {
            label:"Yes, I will supply the parts",
            value:"Customer wants to supply the parts",

          },
          {
            label:"Yes, I will supply the parts but need guidance from the professional",
            value:"Customer wants to supply the parts but  needs guidance from the professional",

          },

          {
            label:"No, I want the contractor buy the parts",
            value:"Customer wants you to buy the parts"
          },
          ]
        },
          {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
    };
};
