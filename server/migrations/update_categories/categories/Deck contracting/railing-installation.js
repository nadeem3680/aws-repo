module.exports = function(parentId) {
  return {
    name: 'Railing Installation & Remodeling',
    route: 'railing-installation',
    search_keywords: ['railing' ,'railing Installation', 'deck railing Installation', 'railing remodeling','deck','patio','wrought iron railing','iron rail','wood iron','fence','wood fence','blacony','deck rail','balcony rail','balcony railings','deck railings','stairs'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'deck contractor',
    actor_plural: 'deck contractors',
    action: 'help you repair your railing & banisters',
    questions:  [
    {
        field_type:'checklist',
        question:"What railing work do you need?",
        description:'Railing work needed',
        choices:[
        {
            label:"Installing new railing",
            value:"Installing new railing"
        },
        {
            label:'Installing new banisters',
            value:"Installing new banisters"
        },
        {
            label:'Remodeling existing banisters',
            value:"Remodeling existing banisters"
        },
        {
            label:'Remodeling existing railing',
            value:"Remodeling existing railing"
        }
        ]
    },
    {
        field_type:'checklist',
        question:"Where do you need railing installed?",
        description:"Where",
        choices:[
        {
            label:"Deck/balcony/patio",
            value:"Deck/balcony/patio"
        },
        {
            label:"Indoor staircase",
            value:"Indoor staircase"
        },
        {
            label:"Outdoor staircase",
            value:"Outdoor staircase"
        },
        {
            can_describe:true
        }
        ]
        
    },
    {
        field_type:"select",
        question:"What kind of railing do you want?",
        description:"Kind of railing to be installed",
        choices:[
        {
            label:"Wood",
            value:"Wood"
        },
        {
            label:"Concrete",
            value:"Concrete"
        },
         {
            label:"Glass or acrylic",
            value:"Glass or acrylic"
        },
        {
            label:'Plaster',
            value:"Plaster"
        }
        ]
    },
    {
        field_type:"select",
        question:"What kind of banisters do you want?",
        description:"Kind of banisters to be installed",
        choices:[
        {
            label:"Wood",
            value:"Wood"
        },
        {
            label:"Concrete",
            value:"Concrete"
        },
         {
            label:"Glass or acrylic",
            value:"Glass or acrylic"
        },
        {
            label:'Plaster',
            value:"Plaster"
        }
        ]
    },
   {
          field_type:"select",
          question:"Will you supply the parts?",
          description:"Who is supplying the parts",
          choices:[
          {
            label:"Yes, I will supply the parts",
            value:"Customer wants to supply the parts",

          },
          {
            label:"Yes, I will supply the parts but need guidance from the professional",
            value:"Customer wants to supply the parts but  needs guidance from the professional",

          },

          {
            label:"No, I want the contractor buy the parts",
            value:"Customer wants you to buy the parts"
          },
          ]
        },
          {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
    };
};
    