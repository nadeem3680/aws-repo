'use strict';

exports.isNewRoot = true;

exports.root = {
  name:"Patio,Deck & Railing",
  parent:null
};

exports.children = [
  require('./deck-patio-addition'),
  require('./deck-patio-repair'),
  require('./railing-installation'),
  require('./railing-repair')
];