module.exports = function(parentId) {
  return{
  name: 'Deck & Patio Remodeling Addition',
  route: 'deck-patio-addition',
  search_keywords: ['deck remodeling contractor','new deck', 'deck addition', 'patio addition','patio','deck','deck over','deck over deck','fence','railings','railing','building patio','build a deck','build a patio','backyard deck','patio installation','deck contractor','deck builder','wood fence','wooden deck','concrete backyard','concrete deck'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'Deck contractor',
        actor_plural: 'Deck contractors',
        action: 'help you repair & maintain your deck or patio',
        questions:  [
        {
          field_type:"select",
          question:"What is the project",
          description:"Type of project",
          choices:[
          {
            label:"New deck",
            value:"New deck"
          },
          { 
            label:"Addition",
            value:"Addition"

         },
         {
          label:"Remodel",
          value:"Remodel"
         }
          ]
        },
        {
          field_type:'text',
          question:"What is the approximate square footage of your existing or planned outdoor area?",
          description:"The approximate square footage of the existing or planned outdoor area"
        },
       {
        field_type:"checklist",
        question:"What additional services do you need?",
        description:"Additional services needed",
        required:false,
        choices:[
        {
          label:"Lighting",
          value:"Lighting"
        },
        {
          label:"A place for hot-tub",
          value:"A place for hot-tub"
        },
        {
          label:"Roof covering",
          value:"Roof covering"
        },
        {
          label:"Heating and insulation",
          value:"Heating and insulation"
        },
        {
          can_describe:true
        }

        ]
       },
        {
          field_type:"select",
          question:"Will you provide all materials and parts?",
          description:"Customer will supply all materials and parts",
          choices:[ 
           {
            label: "Yes",
            value:"Yes, customer will provide all materials and parts"
          },
          {
            label:"Yes, but I need guidance from the professional",
            value:"Yes, but customer needs guidance from the professional"
          },
          {
            label:"No",
            value:"No, customer wants you to bring your own equipments and supplies"
          }
          ]



        },
        {
          field_type:"text",
          question:"How old is the property",
          description:"Age of the property"
        }
        ]
      };
    };