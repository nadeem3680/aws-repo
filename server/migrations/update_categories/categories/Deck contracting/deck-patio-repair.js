module.exports = function(parentId) {
  return{
  name: 'Deck & Patio Repair Maintenance',
  route: 'deck-patio-repair',
  search_keywords: ['deck repair contractor','deck repair', 'deck maintenance', 'patio maintenance','deck building','deck addition','paio addition','deck patio','patip repair','patio repair','deck contractor','patio maintenance','deck coatig','deck coating','patio coating','deck over'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Deck contractor',
        actor_plural: 'Deck contractors',
        action: 'help you repair & maintain your deck or patio',
        questions:  [
        {
          field_type:"checklist",
          question:"What service do you need?",
          description:"Type of service",
          choices:[
          {
            label:"Repairing broken or damaged railing",
            value:"Repairing broken or damaged railing"
          },
          {
            label:"Routine inspection",
            value:"Routine inspection"
          },
          {
            label:"Repairing broken floorboards",
            value:"Repairing broken floorboards"
          },
          {
            label:"Routine cleaning",
            value:"Routine cleaning"
          },
          {
            label:"Sealing or staining",
            value:"Sealing or staining"
          },
          {
            label:"Repairing or replacing screening",
            value:'Repairing or replacing screening'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"text",
          question:"What is the size of the deck, balcony, or patio in square feet?",
          description:"Size of the deck, balcony, or patio in square feet",

        },
        {
          field_type:"select",
          question:"What floor will the work take place on?",
          description:"What floor",
          choices:[
          {
            label:"Ground level",
            value:"Ground level"
          },
          {
            label:"Second level",
            value:"Second level"
          },
          {
            label:'Above second level',
            value:"Above second level"
          }
          ]
        },
       {
          field_type:"select",
          question:"Will you provide all materials and parts?",
          description:"Customer will supply all materials and parts",
          choices:[ 
           {
            label: "Yes",
            value:"Yes, customer will provide all materials and parts"
          },
          {
            label:"Yes, but I need guidance from the professional",
            value:"Yes, but customer needs guidance from the professional"
          },
          {
            label:"No",
            value:"No, customer wants you to bring your own equipments and supplies"
          }
          ]



        },
        {
          field_type:"text",
          question:"How old is the deck, balcony or patio",
          description:"Age of the deck, balcony or patio"
        }
        ]
      };
    };