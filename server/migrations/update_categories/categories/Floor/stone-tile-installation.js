module.exports = function(parentId) {
  return {
        name: 'Stone & Tile Installation',
        route: 'stone-tile-installation',
        search_keywords: ['stone', 'tile',' stone panel', 'stone Installation', 'marble',' ceramic Installation','Porcelain install','Porcelain installation','natural stone installation','marble tile installation','flooring','carpet','handyman','slate flooring','slate floor installation','slate tiles','commercial flooring','granite tiles','granite tiles','travertine tiles','marble tiles','marble floor'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you install your stone & tile',
        questions: [
           {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Installation",
                value:'Installation'
            },
            {
                label:"Replacement",
                value:"Replacement"
            }
            ]
        },
        {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
         {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Underfloor heating',
                value:"Underfloor heating"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
         {
            field_type:'checklist',
            question:"What kind of material do you want to install?",
            description:"Type of material",
            choices:[
            {
                label:"Granite",
                value:"Granite"
            },
            {
                label:"Slate",
                value:"Slate"
            },
            {
                label:"Marble",
                value:'Marble'
            },
           
            {
                label:"Ceramic",
                value:'Ceramic'
            },
            {
                label:"Porcelain",
                value:"Porcelain"
            },
            {
                label:'Mosaic',
                value:"Mosaic"
            },
            {
                label:"As recommended by professional",
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },

        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        }
            ]
};
};
        