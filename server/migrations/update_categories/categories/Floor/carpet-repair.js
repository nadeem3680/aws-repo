module.exports = function(parentId) {
  return {
        name: 'Carpet Repair',
        route: 'carpet-repair',
        search_keywords: ['carpet, carpet repair','flooring','berber carpet repair','berber carpet','carpet partial replacement','mohawk carpet','carpet runner','shag carpet','carpet tile','carpeting','rug repair','stair carpet','carpet stair treads','carpet running for stairs','vinyl flooring','laminate floor'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Carpet specialist',
        actor_plural: 'Carpet specialists ',
        action: 'help you repair your carpet',
        questions: [
         {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"My carpet needs repair",
                value:'Carpet needs repair'
            },
            {
                label:"Partial replacement",
                value:"Partial replacement"
            },
            {
                label:"Stretch current carpet",
                value:"Stretch current carpet"
            }
            ]
        },
        {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
        {
            field_type:"text",
            question:'How old is the current carpet?',
            description:'The age of the carpet'
        },
        {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },

            {
                label:'Heated floor',
                value: 'Heated floor'
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:"checklist",
            question:"What kind of carpet?",
            description:"Kind of carpet",
            choices:[
            {
                label:"Barber",
                value:"Barber"
            },
            {
                label:"Plush",
                value:"Plush"
            },
            {
                label:"Environmentally friendly",
                value:"Environmentally friendly"
            },
            {
                label:"I am not sure",
                value:"Customer is not sure"
            },
            {
                can_describe:true
            }
            ]
        },
          {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        },
         {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};