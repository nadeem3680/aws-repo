module.exports = function(parentId) {
  return {
        name: 'Stone & Tile Repair',
        route: 'stone-tile-repair',
        search_keywords: ['stone repair', 'tile repair',' tile partial replacement', 'tile regrouting', 'regrouting','marble tile','granite tile','tile care','marble tile treatment','stone floor care','stone floor','backsplash tiles','back splash tiles','grout repair','grout sealer','bathroom tile','handyman','tile contractor','flooring contractor','ceramic tile','ceramic tile replacement','ceramic tile','Porcelain tile','Porcelain tile flooring','Porcelain','grout cleaner','best grout cleaner','tile grout cleaner','natural stone tile care','Mosaic tile partial replacement','ceramic tile partial replacement','natural stone partial replacement','floor floor'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you repair your stone & tile',
        questions: [
         {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Repair",
                value:'Repair'
            },
            {
                label:"Partial replacement",
                value:"Partial replacement"
            },
            {
                label:"Re-grout tiles",
                value:"Re-grout tiles"

            },
            
            {
                label:"Refinishing",
                value:"Refinishing"
            }
            ]
        },
         {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
         {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Underfloor heating',
                value:"Underfloor heating"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
         {
            field_type:'checklist',
            question:"What kind of material do you currently have?",
            description:"Type of material",
            choices:[
            {
                label:"Granite",
                value:"Granite"
            },
            {
                label:"Slate",
                value:"Slate"
            },
            {
                label:"Marble",
                value:'Marble'
            },
           
            {
                label:"Ceramic",
                value:'Ceramic'
            },
            {
                label:"Porcelain",
                value:"Porcelain"
            },
            {
                label:'Mosaic',
                value:"Mosaic"
            },
            {
                label:"As recommended by professional",
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },

         {
            field_type:"checklist",
            question:'Why does your stone or tile need repair?,',
            description:"Type of damage",
            choices:[
            {
                label:"Discoloration",
                value:"Discoloration"
            },
            {
                label:"Loose stones or tiles",
                value:"Loose stones or tiles"
            },
            
            {
                label:"Chipped, cracked or scratched",
                value:"Chipped, cracked or scratched",

            },
            {
                label:"Resealing",
                value:'Resealing'
            },
            {
                label:"Re-grouting",
                value:"Re-grouting"
            },
            {
                label:"Loose boards",
                value:"Loose boards"
            }
            ]
        },

        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        }
            ]
};
};
        