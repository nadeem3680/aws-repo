module.exports = function(parentId) {
  return {
        name: 'Vinyl or Linoleum Repair',
        route: 'vinyl-linoleum-repair',
        search_keywords: ['Vinyl, Linoleum','flooring','Vinyl flooring','Linoleum flooring','Vinyl tile','Linoleum tile','Vinyl repair', 'Vinyl repair', 'Linoleum repair','laminate','kitchen floor','vinyl flooring','luxury vinyl flooring','loose lay vinyl flooring','vinyl floor repair','plank repair','tile installation','flooring','floor care','carpet','carpet installation','marmoleum ','marmoleum flooring','outdoor linoleum','tarkett linoleum','high end linoleum','linoleum repair','linoleum replacement','dented vinyl','dented linoleum','burned vinyl','burned vinyl floor','dented vinyl floor','dented linoleum floor','burned linoleum ','burned linoleum floor','cracked vinyl','cracked vinyl floor','cracked linoleum ','cracked linoleum floor','floor repair','flooring repair','edge curling floor','curled floor','curling vinyl','edge curling vinyl','curled linoleum','edge curled linoleum','loose vinyl floor','loose vinyl','curling vinyl floor','edge curling vinyl floor','tears on vinyl floor','ripped vinyl','ripped linoleum','melted vinyl','melted vinyl','discolored vinyl','vinyl Discoloration','linoleum Discoloration','stained vinyl'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you repair your vinyl and linoleum',
        questions: [
       {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Repair",
                value:'Repair'
            },
            {
                label:"Partial replacement",
                value:"Partial replacement"
            }
            
            ]
        },
        {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
        {

            field_type:'checklist',
            question:"What kind of material do you have?",
            description:"Kind of material needed by customer",
            choices:[
            {
                label:"Vinyl tiles",
                value:"Vinyl tiles"
            },
            {
                label:"Vinyl sheets",
                value:"Vinyl sheets"
            },
            {
                label:"Vinyl planks",
                value:'Vinyl planks'
            },
            {
                label:"Linoleum sheets",
                value:"Linoleum sheets"
            },
            {
                label:"Linoleum tiles",
                value:"Linoleum tiles"
            },
            {
                label:"Linoleum planks",
                value:"Linoleum planks"
            },
            {
                label:"As recommended by professional",
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },
        {
            field_type:"checklist",
            question:'Why does vinyl or linoleum repair?,',
            description:"Type of damage",
            choices:[
            {
                label:"Dented, worn through or cracked",
                value:"Dented, worn through or cracked"
            },
            {
                label:"Tears or rips",
                value:"Tears or rips"
            },
            
            {
                label:"Edge curling or loose",
                value:"Edge curling or loose",

            },
            {
                label:"Water damage",
                value:'Water damage'
            },
            {
                label:"Burned or melted",
                value:"Burned or melted"
            },
            {
                label:"Discoloration or staining",
                value:"Discoloration or staining"
            }
            ]
        },
                {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};
        