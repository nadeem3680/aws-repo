module.exports = function(parentId) {
  return {
        name: 'Carpet Installation',
        route: 'carpet-installation',
        search_keywords: ['carpet', 'carpet installation','flooring','berber carpet installation','carpet runner','cork flooring','outdoor carpet installation','outdoor carpet','carpet replacing','carpet replacement','stair carpet','stair carpet installation','installing carpet','handyman','flooring specialist','carpet specialist'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'Carpet Installer',
        actor_plural: 'Carpet Installers ',
        action: 'help you install your carpet',
        questions: [
        {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"I am installing new carpet",
                value:'Carpet installation'
            },
            {
                label:"I am replacing my carpet",
                value:"Replacing existing carpet"
            }
            ]
        },
        {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
        {
            field_type:"text",
            question:'How old is the house',
            description:'The age of the house'
        },
        {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,

            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Heated floor',
                value:"Heated floor"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:"checklist",
            question:"What kind of carpet material do you need",
            description:"Kind of carpet needed by customer",
            choices:[
            {
                label:"Barber",
                value:"Barber"
            },
            {
                label:"Plush",
                value:"Plush"
            },
            {
                label:"Environmentally friendly",
                value:"Environmentally friendly"
            },
            {
                label:"As recommended by professional",
                value:"As recommended by professional"
            },
            {
                can_describe:true
            }
            ]
        },
          {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};