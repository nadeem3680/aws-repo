'uses strict';

exports.isNewRoot=true;

exports.root={
	name:"Flooring",
	parent:null
};

exports.children=[
require('./carpet-installation'),
require('./carpet-repair'),
require('./hardwood-floor-installation'),
require('./hardwood-floor-repair'),
require('./stone-tile-installation'),
require('./stone-tile-repair'),
require('./vinyl-linoleum-installation'),
require('./vinyl-linoleum-repair')
];