module.exports = function(parentId) {
  return {
        name: 'Hardwood Floor repair',
        route: 'hardwood-floor-repair',
        search_keywords: ['hardwood', 'floor',' hardwood floor', 'hard', 'hard wood, hardwood repair','hardwood refinishing','hardwood floors','flooring','carpet','Laminate flooring','floorboards','armstrong laminate','harwood flooring','cork flooring','parquet flooring','bamboo flooring','bamboo hardwood floor','bamboo hardwood flooring','Engineered hardwood','Engineered hardwood flooring','Engineered hardwood floor','wood flooring','wood floors','canadian hardwood','hardwood floor Installation','hardwood flooring installation','hardwood floor installation','Engineered hard installation','bamboo hardw'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you repair your hardwood',
        questions: [
         {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Repair",
                value:'Repair'
            },
            {
                label:"Partial replacement",
                value:"Partial replacement"
            },

            {
                label:"Refinishing",
                value:"Refinishing"
            }
            ]
        },
         {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
         {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Underfloor heating',
                value:"Underfloor heating"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:"What kind of hardwood do you currently have?",
            description:"Kind of hardwood",
            choices:[
            {
                label:"Solid Wood",
                value:"Solid Wood"
            },
            {
                label:"Engineered hardwood",
                value:"Engineered hardwood"
            },
            {
                label:"Bamboo",
                value:'Bamboo'
            },
            {
                label:"Laminate",
                value:"Laminate"
            },
           
            {
                label:"I am not sure",
                value:'I am not sure'
            },
            {
                can_describe:true
            }
            ]

        },
         {
            field_type:"checklist",
            question:'Why does your floor need repair?,',
            description:"Type of damage",
            choices:[
            {
                label:"Floor is wrapped",
                value:"Floor is wrapped"
            },
            {
                label:"Floor is cupping or buckling",
                value:"Floor is cupping or buckling"
            },
            
            {
                label:"Floor is cracked or splintering",
                value:"Floor is cracked or splintering",

            },
            {
                label:"Floor is stretched or worn",
                value:'Floor is stretched or worn'
            },
            {
                label:"Burned",
                value:"Burned"
            },
            {
                label:"Loose boards",
                value:"Loose boards"
            },
            {
                label:"Discoloration or staining",
                value:"Discoloration or staining"
            }
            ]
        },

        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        }
            ]
};
};
        