module.exports = function(parentId) {
  return {
        name: 'Hardwood Floor Installation',
        route: 'hardwood-floor-installation',
        search_keywords: ['hardwood', 'floor',' hardwood floor', 'hard', 'hard wood, hardwood Installation','hardwood floors','flooring','carpet','Laminate flooring','floorboards','armstrong laminate','harwood flooring','cork flooring','parquet flooring','bamboo flooring','bamboo hardwood floor','bamboo hardwood flooring','Engineered hardwood','Engineered hardwood flooring','Engineered hardwood floor','wood flooring','wood floors','canadian hardwood','hardwood floor Installation','hardwood flooring installation','hardwood floor installation','Engineered hard installation','bamboo hardwood installation'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you install your hardwood',
        questions: [
         {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Installation",
                value:'Installation'
            },
            {
                label:"Replacement",
                value:"Replacement"
            }
            ]
        },
       {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
         {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Underfloor heating',
                value:"Underfloor heating"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:"checklist",
            question:"What kind of floor do you currently have?",
            description:"Current flooring material",
            choices:[
            {
                label:'Hardwood or similar',
                value:"Hardwood or similar"
            },
            {
                label:"Carpet",
                value:"Carpet"
            },
            {
                label:"Linoleum or vinyl",
                value:"Linoleum or vinyl"
            },
            {
                label:"Tiles",
                value:'Tiles'
            },
            {
                label:"Stone",
                value:"Stone"
            },
            {
                label:"Concrete",
                value:"Concrete"
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:"What kind of material do you need?",
            description:"Kind of material needed by customer",
            choices:[
            {
                label:"Solid Wood",
                value:"Solid Wood"
            },
            {
                label:"Engineered hardwood",
                value:"Engineered hardwood"
            },
            {
                label:"Bamboo",
                value:'Bamboo'
            },
           
            {
                label:"As recommended by professional",
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },

        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
            ]
};
};
        