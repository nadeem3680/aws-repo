module.exports = function(parentId) {
  return {
        name: 'Vinyl or Linoleum Installation',
        route: 'vinyl-linoleum-installation',
        search_keywords: ['Vinyl, Linoleum','flooring','Vinyl flooring','Linoleum flooring','Vinyl tile','Linoleum tile','Vinyl install', 'Vinyl installation', 'Linoleum installation','floor installation','carpet','flooring work','floor care','carpet installation','tile installation','Handyman','rubber flooring','cork flooring','luxury vinyl floorin','vinyl laminate flooring','laminate flooring','vinyl tiles','vinyl floor tiles','vinyl tile flooring','floor vinyl tiles','tile adhensive','kitchen floor','resilient floor','resilient flooring','kitchen flooring','vinyl kitchen flooring','vinyl planks','linoleum sheets'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you install your vinyl and linoleum',
        questions: [
        {
            field_type:"select",
            question:"What is the project?",
            description:"Type of project",
            choices:[
            {
                label:"Installation",
                value:'Installation'
            },
            {
                label:"Replacement",
                value:"Replacement"
            }
            ]
        },
       {
            field_type:"text",
            question:"What is the approximate square footage of the area that needs work?",
            description:"The approximate square footage of the work"
        },
       
        {
            field_type:'checklist',
            question:"Does any of the below characteristics apply to your floor?",
            description:"Anything special about the floor",
            required:false,
            choices:[
            {
                label:'Stairs',
                value:'Stairs'
            },
            {
                label:'Underfloor heating',
                value:"Underfloor heating"
            },
            {
                label:"Room(s) have round corners, not in a rectangular shape",
                value:'Room(s) not in rectangular shape'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:"What kind of material do you need?",
            description:"Kind of material needed by customer",
            choices:[
            {
                label:"Vinyl tiles",
                value:"Vinyl tiles"
            },
            {
                label:"Vinyl sheets",
                value:"Vinyl sheets"
            },
            {
                label:"Vinyl planks",
                value:'Vinyl planks'
            },
            {
                label:"Linoleum sheets",
                value:"Linoleum sheets"
            },
            {
                label:"Linoleum tiles",
                value:"Linoleum tiles"
            },
            {
                label:"Linoleum planks",
                value:"Linoleum planks"
            },
            {
                label:"As recommended by professional",
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },
        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};