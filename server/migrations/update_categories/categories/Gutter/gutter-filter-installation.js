module.exports = function(parentId) {
  return {
    name: 'Gutter Filter Installation & Replacement',
    route: 'gutter-filter-installation',
    search_keywords: ['gutter' , 'gutter filter', 'gutter installation','gutter replacement','roof','roofer','roo','gut','gu','roofing','roofing company','eavestrough','rain gutter fliter','gutter downspout','sheerflow gutter filter installation','gutter filter installation','rain gutter filter','leaf guard'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Gutter Technician',
    actor_plural: 'Gutter Technicians',
    action: 'install or replace your gutter filter',
    questions: [
    {
        field_type:"text",
        question:"What is the approximate square footage of your property?",
        description:"The approximate square footage of the property"
    },
   {
        field_type: "select",
        question: "Will you supply the materials and parts?",
        description: "Who supplies the materials and parts",
        choices: [
          {
            label: "Yes, I will provide the materials and parts",
            value: "Customer will provide the materials and parts"
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide parts and material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the parts",
            value: "Customer wants you to supply the parts"
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};