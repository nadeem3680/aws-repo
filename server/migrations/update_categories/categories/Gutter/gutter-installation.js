module.exports = function(parentId) {
  return {
    name: 'Gutter Installation',
    route: 'gutter-installation',
    search_keywords: ['gutter' ,  'gutter installation','roof','roofer','roo','gut','gu','roofing','roofing company','gutter services','rain gutter','window','window installation','house gutters','house gutter installation','raingutters','raingutters installation','installing raingutters','installing gutter','vinyl gutter','install rain','leaf guard','downspout','downspout diverter','downspout existing','gutter system','gutter system installation','vinyl gutter installation'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Gutter Technician',
    actor_plural: 'Gutter Technicians',
    action: 'install or replace your gutter',
    questions: [
      {
        field_type:"checklist",
        question:'Type of project',
        description:'Type of project',
        choices:[
          {
            label:"Replace old gutters with new gutters",
            value:"Replace old gutters with new gutters"

          },
          {
            label:'Install gutter guards',
            value:"Install gutter guards"
          },
          {
            label:'Install new gutters, there is no existing gutters',
            value:"Install new gutters, there is no existing gutters"
          }
        ]
      },
      {
        field_type:"text",
        question:"What is the approximate square footage of your property?",
        description:"The approximate square footage of the property"
      },
      {
        field_type: "select",
        question: "Will you supply the materials and parts?",
        description: "Who supplies the materials and parts",
        choices: [
          {
            label: "Yes, I will provide the materials and parts",
            value: "Customer will provide the materials and parts"
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide parts and material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the parts",
            value: "Customer wants you to supply the parts"
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};
