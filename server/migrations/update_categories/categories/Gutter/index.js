'use strict';

exports.isNewRoot=true;

exports.root = {
  name: "Gutter Services",
  parent: null
};
exports.children = [
  require('./gutter-cleaning'),
  require('./gutter-filter-installation'),
  require('./gutter-installation'),
  require('./gutter-repair')
];