module.exports = function(parentId) {
  return {
    name: 'Gutter Repair & Maintenance',
    route: 'gutter-repair-maintenance',
    search_keywords: ['gutter' , 'cleaning', 'gutter repair','roof','roofer','roo','gut','gu','roofing','eavestrough','downspouts','roof repair','fix gutter','leaf guard','gutter costs','roofing & gutter','fix gutter','gutter replacement','replacing gutter','roof leaks','handyman','precision','rain gutter','rain gutter repair'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Gutter Technician',
    actor_plural: 'Gutter Technicians',
    action: 'repair your gutter',
    questions: [
      {
        field_type:"checklist",
        question:"Reason for the gutter service project?",
        description:"Project type",
        choices:[
        {
            label:"Routine gutter cleaning",
            value:"Routine gutter cleaning"

        },
        {
            label:"Blockage, gutter needs cleaning",
            value:"Clean gutter due to blockage"
        },
        {
            label:"Patch leaking gutters",
            value:"Patch leaking gutters"
        },
        {
            label:"Re-attach fallen or detached gutters",
            value:'Re-attach fallen or detached gutters'
        },
        {
            label:"Remove and dispose current gutters",
            value:"Remove and dispose current gutters"
        }
        ]
    },
   {
        field_type: "select",
        question: "Will you supply the materials and parts?",
        description: "Who supplies the materials and parts",
        choices: [
          {
            label: "Yes, I will provide the materials and parts",
            value: "Customer will provide the materials and parts"
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide parts and material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the parts",
            value: "Customer wants you to supply the parts"
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};
        
