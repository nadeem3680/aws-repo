module.exports = function(parentId) {
  return {
        name: 'Sprinkler System Repair & Maintenance',
        route: 'sprinkler-system-repair',
        search_keywords: ['Sprinkler', 'landscaping','plumbing','automatic sprinkler','handyman','sprinkler system','sprinkler repai','Sprinkler system repair','sprinkler Maintenace','underground sprinkler','underground sprinkler system','lawn sprinkler repair','underground sprinkler repair','orbit','orbit sprinkler repair','hunter','hunter sprinkler','ground','ground sprinkler','plumbing','plumber','Landscaper','landscaping','water sprinkler','water sprinklers','in ground sprinkler system','underground sprinkler system cost','installer','ez','toro','automatic','rainbird','rainbird heads','heads','garden','lawn care'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you repair and maintain your sprinkler system',
        questions: [
        {
            field_type:'checklist',
            question:'What is being watered with your system?',
            description:'What is being watered with the system',
            choices:[
            {
                label:'Grass',
                value:'Grass'
            },
            {
                label:'Trees',
                value:'Trees'
            },
            {
                label:'Flower beds',
                value:'Flower beds'
            },
            {
                label:'Shrubs',
                value:'Shrubs'
            },
            {
                label:'Garden',
                value:'Garden'
            },
            {
                can_describe:true
            }

            
            ]
        },
        {
            field_type:'checklist',
            question:'What sprinkler system problems are you having?',
            description:'Problems to fix',
            choices:[
            {
                label:'No repairs, just maintenance',
                value:'No repairs, just maintenance'
            },
            {
                label:'Timer does not work',
                value:'Timer does not work'
            },
            {
                label:'Timer needs to be reprogrammed',
                value:'Timer needs to be reprogrammed'
            },
            {
                label:'Sprinkler head(s) need to be relocated',
                value:'Sprinkler head(s) need to be relocated'
            },
            {
                label:'System does not turn on',
                value:'System does not turn on'
            },
            {
                label:'Water runs continuously',
                value:'Water runs continuously'
            },
            {
                label:'Sprinkler head is broken',
                value:'Sprinkler head is broken'
            },
            {
                label:'No water to a single sprinkler head',
                value:'No water to a single sprinkler head'
            },
            {
                label:'No water to one area of sprinkler head',
                value:'No water to one area of sprinkler head'
            },
            {
                can_describe:true
            }
            
            ]
        },
        {
            field_type:'checklist',
            question:'What sprinkler system maintenance do you need?',
            description:'Maintenance',
            choices:[
            {
                label:'No maintenance needed',
                value:'No maintenance needed'
            },
            {
                label:'Preparing for winter',
                value:'Preparing for winter'
            },
            {
                label:"Mid-summer checkup",
                value:'Mid-summer checkup'
            },
            {
                label:'Spring activation',
                value:'Spring activation'
            },
            {
                can_describe:true
            }

            ]
        }
        ]
    };
};