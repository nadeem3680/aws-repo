module.exports = function(parentId) {
  return {
        name: 'Tree Removal',
        route: 'tree-removal',
        search_keywords: ['tree', 'tree removal','tree removal services','tree services','removal services','junk removal','large tree removal','large trees','tree services removal','removal tree','handyman','stump removal services','stump removal services','stump removal'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you remove your tree',
        questions: [
        {
            field_type:"select",
            question:"How many trees are you removing?",
            description:"Number of trees to be removed",
            choices:[
            {
                label:"1",
                value:"1"
            },
            {
                label:"2",
                value:"2"
            },
            {
                label:"3",
                value:"3"
            },
            {
                label:"More than 3",
                value:"More than 3"
            }
            ]
        },
        {
            field_type:"text",
            question:"How tall are the trees?",
            description:"Height of tree(s)"
        },
        {
            field_type:"select",
            question:"Are the trees near electrical lines?",
            description:"Near electrical lines?",
            choices:[
            {
                label:"Yes",
                value:"It is near electrical lines"
            },
            {
                label:"No",
                value:"It is not near electrical lines"
            }
            ]
        },
        {
            field_type:"checklist",
            question:"What type of trees?",
            description:"Type of tree(s)",
            choices:[
            {
                label:"Oak",
                value:"Oak"
            },
            {
                label:"Maple",
                value:"Maple"
            },
            {
                label:"Hemlock",
                value:"Hemlock"
            },
            {
                label:"Pine",
                value:"Pine"
            },
            {
                label:"Cedar",
                value:"Cedar"
            },
            {
                can_describe:true
            }
            ]

        },
        {
            field_type:"checklist",
            description:"Position of the tree",
            question:"Where is the tree?",
            choices:[
            {
                label:"Front yard",
                value:"Front yard"
            },
            {
                label:"Back yard",
                value:"Back yard"
            },
            {
                label:"Side yard(s)",
                value:"Side yard(s)"
            }
            ]
        }
        ]
    };
};
