module.exports = function(parentId) {
  return {
        name: 'Tree & Shrub Planting Maintenance',
        route: 'tree-shrub-planting-maintenance',
        search_keywords: ['tree', 'tree maintenance','Tree trimming','tree pruning','stup grinding','shrub plant', 'shrub maintenance','tree pruning','arborist','cherry tree maintenance','apple tree maintenance','tree services','tree service','big tree','tree stump','removal','tree company','tree cutting company','tree care company','tree service company','tree companies','tree stump removal','money tree','tree arborist','orange tree','city tree','planting new trees','new tree care','trimming services','trimming service','tree trimming','shrub trimming service','quality tree service','quality shrub service','shrub trimming','trimming shrubs','trimming a shrub'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you maintain your garden',
        questions: [
        {
            field_type:"checklist",
            question:'What kind of work do you need done?',
            description:"Project",
            choices:[
            {
                Label:"Shrub trimming",
                value:"Shrub trimming"
            },
            {
                Label:'Tree trimming',
                value:'Tree trimming'
            },
            {
                Label:'Basic maintenance',
                value:'Basic maintenance'
            },
            {
                Label:'Shrub removal',
                value:"Shrub removal"
            },
            {
                Label:"Insect control",
                value:"Insect control"
            },
            {
                Label:'Stump grinding',
                value:"Stump grinding"
            },
            {
                Label:"Stump removal",
                value:"Stump removal"
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:"checklist",
            question:"What type of trees?",
            description:"Type of tree(s)",
            choices:[
            {
                label:"Oak",
                value:"Oak"
            },
            {
                label:"Maple",
                value:"Maple"
            },
            {
                label:"Hemlock",
                value:"Hemlock"
            },
            {
                label:"Pine",
                value:"Pine"
            },
            {
                label:"Cedar",
                value:"Cedar"
            },
            {
                can_describe:true
            }
            ]

        },
        {
            field_type:"select",
            question:"How many trees?",
            description:"Number of trees",
            choices:[
            {
                label:'0',
                value:"0"
            },
            {
                label:"1",
                value:"1"
            },
            {
                label:"2",
                value:"2"
            },
            {
                label:"3",
                value:"3"
            },
            {
                label:"More than 3",
                value:"More than 3"
            }
            ]
        },
        {
            field_type:"select",
            question:"How many shrubs?",
            description:"Number of shrubs",
            choices:[
            {
                label:'0',
                value:"0"
            },
            {
                label:"1",
                value:"1"
            },
            {
                label:"2",
                value:"2"
            },
            {
                label:"3",
                value:"3"
            },
            {
                label:'4',
                value:'4'
            },
            {
                label:"More than 4",
                value:"More than 4"
            }
            ]
        },
        {
            field_type:"checklist",
            description:"Position of the tree",
            question:"Where is the tree?",
            choices:[
            {
                label:"Front yard",
                value:"Front yard"
            },
            {
                label:"Back yard",
                value:"Back yard"
            },
            {
                label:"Side yard(s)",
                value:"Side yard(s)"
            }
            ]
        }
        ]
    };
};
