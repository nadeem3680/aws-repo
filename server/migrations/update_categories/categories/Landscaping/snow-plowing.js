module.exports = function(parentId) {
  return {
    name: 'Snow Plowing',
    route: 'snow-plowing',
    search_keywords: ['snow', 'snow plowing','snow removal','boss snow plows','fisher snow','pickup snow','artic snow plows','removal','Landscaper','landscaping','warn','snow plowing company','snow service','plow tractor','quad','blade','plow service','roof'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Landscaper',
    actor_plural: 'Landscapers',
    action: 'help you get rid of snow',
    questions: [
      {
        field_type:'select',
        question:'When do you need snow plowing service?',
        description:"When the service is needed",
        choices:[
          {
            label:"I need snow plowing immediately",
            value:" Customer needs snow plowing IMMEDIATELY",

          },
          {
            label:"I need the service for future",
            value:"Customer needs the service for near future"
          }
        ]
      },
      {
        field_type:"select",
        question:"Do you want recurring service?",
        description:'Recurring',
        choices:[
          {
            label:'Yes, when it is snowing I expect the service',
            value:"Yes, customer expects the service when there is snow"
          },
          {
            label:'No, it is one time project',
            value:'No, it is one time project'
          }
        ]
      },
      {
        field_type:"checklist",
        question:"What surfaces?",
        description:"Surfaces",
        choices:[
          {
            label:"Driveway",
            value:"Driveway"
          },
          {
            label:"Sidewalk",
            value:"Sidewalk"
          },
          {
            label:"Parking lot",
            value:'Parking lot'
          },
          {
            label:'Road',
            value:"Road"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"Describe how steep the surface is?",
        description:"Steepness"

      }
    ]
  };
};