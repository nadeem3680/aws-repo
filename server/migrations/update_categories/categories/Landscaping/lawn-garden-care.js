module.exports = function(parentId) {
  return {
        name: 'Lawn & Garden Care',
        route: 'lawn-garden-care',
        search_keywords: ['Garden', 'Landscaping','Garden care','cutting grass','mowing','planting','Flower','flower bed','maintenance','Trimming','edging','weed','weed prevention','Fertilizer','placement','Insect','Insect control','Aeration','seeding','Landscaper','mulching','leaf','leaf removal','gutter','Fence','Masonry','Sprinkler','care'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you maintain your garden',
        questions: [
        {
            field_type:"checklist",
            question:"What kind of lawn care services do you need?",
            description:"Services needed",
            choices:[
            {
                label:"Flower bed maintenance",
                value:"Flower bed maintenance"
            },
            {
                label:"Planting",
                value:"Planting"
            },
            {
                label:"Mowing",
                value:'Mowing'
            },
            {
                label:"Weeding",
                value:"Weeding"
            },
            {
                label:'Trimming and edging',
                value:"Trimming and edging"           
            },
            {
                label:'Weed prevention',
                value:"Weed prevention"
            },

            {
                label:"Edging",
                value:"Edging"
            },
            {
                label:'Fertilizer application & placement',
                value:"Fertilizer application & placement"
                
            },
            
             {
                label:'Insect control',
                value:"Insect control"
            },
            {
                label:"Aeration",
                value:'Aeration'
            },
            {
                label:"Seeding",
                value:"Seeding"
            },
            {
                label:"Mulching",
                value:"Mulching"
            },
            {
                label:'Leaf clean-up',
                value:"Leaf clean-up"
            }

            ]
        },
        {
            field_type:"text",
            question:"How big is the lawn?",
            description:"How big the lawn is"

        },
        {
            field_type:"checklist",
            question:'Will you need additional services?',
            description:'Any additional services',
            required:false,
            choices:[
            {
                label:"Sprinkler system repair",
                value:'Sprinkler system repair'
            },
            {
                label:"Masonry repair/maintenance",
                value:"Masonry repair/maintenance"
            },
            {
                label:"Fence repair/maintenance",
                value:"Fence repair/maintenance"
            }
            ]
        },
        {
            field_type:'select',
            question:"How often do you need regular maintenance",
            description:"How often will the service be needed",
            choices:[
            {
                label:"Just once",
                value:"Just once"
            },
            {
                label:'Once a week',
                value:"Once a week"
            },
            {
                label:'Every other week',
                value:'Every other week'
            },
            {
                label:"2 or 3 times a month",
                value:"2 or 3 times a month"
            },
            {
                label:'Once a month',
                value:"One a month"
            },
            {
                label:'As needed',
                value:"As needed"
            }
            ]
        }
        ]
    };
};
            