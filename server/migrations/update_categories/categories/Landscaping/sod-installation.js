module.exports = function(parentId) {
  return {
        name: 'Sod Installation',
        route: 'sod-installation',
        search_keywords: ['sod', 'sod Installation','grass','back yard grass','green','grass seed','seeding','lawn fertilizer','lawn seed','grass sod','planting sod','planting grass','planting','plant','sod care','lawn','roll','laying','la','ro','so','lawn rolling','roll out grass','turf roll','sod intaller','fescue','fescue grass','tall fescue grass','fescue sod','Landscaper','landscaping','sprinkler','bermuda','carpet grass','Centipede'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you maintain your garden',
        questions: [
    
        {
            field_type:'checklist',
            question:'How big is the lawn?',
            description:'How big the lawn is',
            choices:[
            {
                label:'Small(less than 1000 sq ft)',
                value:'Small(less than 1000 sq ft)'
            },
            {
                label:'Medium (1000-5000 sq ft)',
                value:'Medium (1000-5000 sq ft)'
            },
            {   
                label:'Large (5000-10,000 sq ft)',
                value:'Large (5000-10,000 sq ft)'
            },
            {
                label:'Very large (10,000+ sq ft)',
                value:'Very large (10,000+ sq ft'
            },
            {
                label:'I am not sure',
                value:'I am not sure'
            }
            ]
        },
        {
            field_type:'checklist',
            question:'What needs to be cleared before the sod is installed?',
            description:'Things to clear before installing the sod',
            choices:[
            {
                label:'No need for this, I just need installing',
                value:'No work needed prior to sod Installation'
            },
            {
                label:'Old grass',
                value:'Old grass'
            },
            {
                label:'Weed',
                value:'Weed'
            },
            {
                label:'Gravel',
                value:'Gravel'
            },
            {
                label:'Concrete',
                value:'Concrete'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'select',
            question:'What kind of sod do you want installed?',
            description:'Type of sod to be installed',
            choices:[
            {
                label:'Bermuda',
                value:'Bermuda'
            },
            {
                label:'Carpetgrass',
                value:'Carpetgrass'
            },
            {
                label:'Centipede',
                value:'Centipede'
            },
            {
                label:'St. Augustino',
                value:'St. Augustino'
            }
            

            ]
        },
        {
            field_type:'select',
            question:'Who will supply the sod?',
            description:'Who will supply the sod',
            choices:[
            {
                label:'I will provide the sod',
                value:'Customer'
            },
            {
                label:'Professional will provide the sod',
                value:'Professional will provide the sod'
            }
            ]
        },
        {
            field_type:'select',
            question:'Will you need regular lawn maintenance?',
            description:'Regular lawn maintenance',
            choices:[
            {
                label:'No,It is a one time project',
                value:'No regular lawn maintenance needed'
            },
            {
                label:'Yes, I will need regular lawn maintenance',
                value:'Yes, Customer will need regular lawn maintenance'
            }
            ]
        }
        ]
    };
};