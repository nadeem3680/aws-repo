'use strict';

exports.isNewRoot = true;

exports.root = {
  name:"Landscaping and Garden Care",
  parent:null
};

exports.children=[
  require('./landscaping'),
  require('./lawn-garden-care'),
  require('./snow-plowing'),
  require('./sod-installation'),
  require('./sprinkler-system-installation'),
  require('./sprinkler-system-repair'),
  require('./tree-removal'),
  require('./tree-shrub-planting-maintenance')
];