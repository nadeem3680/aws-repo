module.exports = function(parentId) {
  return {
        name: 'Sprinkler System Installation',
        route: 'sprinkler-system-installation',
        search_keywords: ['Sprinkler', 'landscaping','plumbing','automatic sprinkler','handyman','sprinkler system','sprinkler repai','Sprinkler system repair','sprinkler Maintenace','underground sprinkler','underground sprinkler system','lawn sprinkler repair','underground sprinkler repair','orbit','orbit sprinkler repair','hunter','hunter sprinkler','ground','ground sprinkler','plumbing','plumber','Landscaper','landscaping','water sprinkler','water sprinklers','in ground sprinkler system','underground sprinkler system cost','installer','ez','toro','automatic','rainbird','rainbird heads','heads','garden','lawn care'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'Landscaper',
        actor_plural: 'Landscapers',
        action: 'help you install your sprinkler system',
        questions: [
        {
            field_type:'select',
            question:'What is the project?',
            description:'Type of the project',
            choices:[
            {
                label:'I want to install a new sprinkler system on a bare yard',
                value:'Sprinkler system installation on a bare yard'
            },
            {
                label:'I want to install a new sprinkler system on an existing yard',
                value:'Sprinkler system installation on an existing yard'
            },
            {
                label:'I want to update or add to an existing sprinkler system',
                value:'Updating or adding to an existing sprinkler system'
            }
            ]
        },
        {
            field_type:'checklist',
            question:'What kind of watering system',
            description:'What kind of watering system',
            choices:[
            {
                label:'Lawn',
                value:'Lawn'
            },
            {
                label:'Flower bed',
                value:'Flower bed'
            },
            {
                label:'Shrubs',
                value:'Shrubs'
            },
            {
                label:'Trees',
                value:'Trees'
            },
            {
                label:'Garden',
                value:'Garden'
            },
            {
                can_describe:true
            }
            ]
        },
         {
            field_type:"text",
            question:"What is the approximate square footage of area that needs to be watered?",
            description:"The approximate square footage of the area that needs to be watered  "
        },
        {
            field_type:'select',
            question:'Do you want a manual or automatic sprinkler system?',
            description:'Manual or automatic',
            choices:[
            {
                label:'Manual',
                value:'Manual'
            },
            {
                label:'Automatic',
                value:'Automatic'
            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'
            }

            ]
        },
        {
            field_type:'select',
            description:'Type of property',
            question:'What type of property?',
            choices:[
            {
                label:'Home',
                value:'Home'
            },
            {
                label:'Commercial ',
                value:'Commercial'
            }
            ]
        }
    ]
};
};