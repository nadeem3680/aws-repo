module.exports = function(parentId) {
  return {
    name: 'Landscaping',
    route: 'landscaping',
    search_keywords: ['landscaper', 'landscaping','Garden care','flowers','paradise','lawn maintenance','flower','beautiful garden','creating a garden','backyard','front','backyard landscaping','landscaping stone','landscaping company','rock','garden design','landscape architect','land'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 4,
    actor: 'Landscaper',
    actor_plural: 'Landscapers',
    action: 'help you have a beautiful garden',
    questions: [
      {
        field_type:"checklist",
        question:"What is the project?",
        description:"Project",
        choices:[
          {
            label:'Completely replace an existing garden',
            value:"Completely replace an existing garden"
          },

          {
            label:"Create a new garden on bare ground",
            value:'Create a new garden on bare ground'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"What is the square footage of your garden?",
        description:"Size of the garden in square footage"
      },
      {
        field_type:'select',
        question:"Will you supply the plants?",
        description:"Who will supply the plants",
        choices:[
          {
            label:"I will supply the plants & flowers",
            value:"Customer will supply the plants"
          },
          {
            label:'I will need the professional help me buy the plants',
            value:'Customer needs your help & advice on buying the plants'
          }
        ]
      },
      {
        field_type:"select",
        question:'Do you already have plans or design?',
        description:'design and plans',
        choices:[
          {
            label:"No existing plans, but I have an idea what I want",
            value:"No existing plans, but customer has an idea what she/he wants"
          },
          {
            label:"No existing plans, need design suggestions",
            value:"No existing plans, need design suggestions"
          },
          {
            label:"Already have plans",
            value:"Customer has a plan or design"
          }
        ]
      },
      {
        field_type:"text",
        question:"Describe your gardening project in more detail. What plants do you want? How many?",
        description:"Project in more detail"
      },
      {
        field_type:"select",
        question:"Will you need a sprinkler or drip system installed?",
        description:"Sprinkle or drip system",
        choices:[
          {
            label:"I already have sprinkler or drip system installed",
            value:"I already have sprinkler or drip system installed"
          },
          {
            label:"I need sprinkler installed",
            value:"Customer needs sprinkler installed"
          },
          {
            label:'I need drip installed',
            value:"Customer needs sprinkler installed"
          },
          {
            label:"I have sprinkler or drip system but they need repair",
            value:"Customer has sprinkler or drip system but they need repair "
          }
        ]
      },
      {
        field_type:'select',
        question:"Will you need regular service or lawn care maintenance",
        description:"How often customer needs regular service or lawn care maintenance",
        choices:[
          {
            label:"No regular maintenance",
            value:"No regular maintenance"
          },
          {
            label:'Once a week',
            value:"Once a week"
          },
          {
            label:'Every other week',
            value:'Every other week'
          },
          {
            label:"2 or 3 times a month",
            value:"2 or 3 times a month"
          },
          {
            label:'Once a month',
            value:"One a month"
          },
          {
            label:'As needed',
            value:"As needed"
          }
        ]
      }
    ]
  };
};