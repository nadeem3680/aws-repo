module.exports = function(parentId) {
  return{
    name: 'Vent and Duct Cleaning',
    route: 'vent-and-duct-cleaning',
    search_keywords: ['cleaning', 'clean', 'cleaning service','Window cleaning','Hood','Hood Cleaning','Duct Cleaning','Vent Cleaning','Vent Repair','Duct Repair','Duct','Vent'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Specialist',
    actor_plural: 'Specialists',
    action: 'Help you clean your vent and duct',
    questions:  [
      {
        field_type:'checklist',
        question:'What needs work?',
        description:'What needs work',
        choice:[
          {
            label:'Vent',
            value:'Vent'
          },
          {
            label:'Duct',
            value:'Duct'
          }
        ]
      },
      {
        field_type:'checklist',
        question:'What problems do you have?',
        description:'Type of problem',
        choices:[
          {
            label:'No problem, just need cleaning',
            value:'No problem, just need cleaning'
          },
          {
            label:'Poor air quality',
            value:'Poor air quality'
          },
          {
            label:'Poor air flow',
            value:'Poor air flow'
          },
          {
            label:'Concerns of mold',
            value:'Concerns of mold'
          },
          {
            label:'Noisy',
            value:'Noisy'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:'select',
        question:'How many vents or ducts need work?',
        description:'Number of vents or ducts that need work',
        choices:[
          {
            label:'1-5',
            value:'1-5'
          },
          {
            label:'6-10',
            value:'6-10'
          },
          {
            label:'11-15',
            value:'11-15'
          },
          {
            label:'16-20',
            value:'16-20'
          }

        ]

      },
      {
        field_type:'checklist',
        question:'What kind of duct or vent needs work?',
        description:'Kind of duct or vent that needs work',
        choices:[
          {
            label:'Heating & cooling',
            value:'Heating & cooling'
          },
          {
            label:'Dryer',
            value:'Dryer'
          },
          {
            label:'Exhaust(Kitchen/bathroom)',
            value:'Exhaust(Kitchen/bathroom'
          },
          {
            can_describe:true
          }
        ]

      },
      {
        field_type:'checklist',
        question:'What material are your ducts?',
        description:'Material',
        choices:[
          {
            label:'Rigid sheet material',
            value:'Rigid sheet material'
          },
          {
            label:'Flexible non-metallic',
            value:'Flexible non-metallic'
          },
          {
            label:'Fiberglass',
            value:'Fiberglass'
          },
          {
            label:'I am not sure',
            value:'Customer is not sure'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};