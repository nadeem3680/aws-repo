'use strict';

exports.isNewRoot=true;

exports.root = {
  name:"Siding",
  parent:null
};

exports.children = [
  require('./siding')
];