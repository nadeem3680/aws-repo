module.exports = function(parentId) {
  return {
    name: 'Siding Installation, Replacement and Removal',
    route: 'siding',
    search_keywords: ['siding' , 'siding removal', 'siding installation', 'siding replacement','window','window installation','deck','patio','window repair','fence','window','vinyl','brick','damaged sidings','handyman','contractor','roofer','roofing','door','aluminum','home repair','home improvment','house siding','log siding','vinyl siding','brick siding','aluminum siding','shake siding','cedar siding','cedar shake siding'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Siding Technician',
    actor_plural: 'Siding Technicians',
    action: 'install,replace & remove your siding',
    questions: [
      {
        field_type:"checklist",
        question:"What siding services do you need?",
        description:"Project type",
        choices:[
          {
            label:"Complete removal of existing sidings",
            value:"Complete removal of existing sidings"
          },
          {
            label:"Complete installation of new sidings",
            value:"Complete installation of new sidings"
          },
          {
            label:"Partial replacement of damaged sidings",
            value:"Partial replacement of damaged sidings"
          },
          {
            label:"Repaint existing siding",
            value:"Repaint existing siding"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"What is the approximate square footage of the area that needs work?",
        description:"The approximate square footage of the area that needs work?"

      },
      {
        field_type:"text",
        question:"What is your current siding material and what do you want the new siding material to be?",
        description:"Current & new siding material"
      },
      {
        field_type:"text",
        question:"How many windows do have?",
        description:"Number of windows"

      },

      {
        field_type: "select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices: [
          {
            label: "Single level house",
            value: "Single level house"
          },
          {
            label: "Two stories house",
            value: "Two stories house"
          },

          {
            label: "Multi unit building",
            value: "Multi unit building"
          },
          {
            label: "Office/business",
            value: "Office/business"
          },
          {
            label: "Commercial",
            value: "Commercial"
          }
        ]
      }
    ]
  };
};
