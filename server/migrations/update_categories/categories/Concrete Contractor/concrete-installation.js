module.exports = function(parentId) {
  return {
    name: 'Concrete Installation',
    route: 'concrete-installation',
    search_keywords: ['concrete' ,'concrete Installation', 'Driveway concrete', 'Concrete foundation','concrete stairs','concrete sidewalk','concrete finishing','cement','concrete driveway','poured concrete','patio concrete','sidewalk concrete','sidewalk','driveway','garage floor concrete','garage floor','laying concrete','carpet installing','installation over','paver','paving','stone patio','concrete steps','concrete forms','polished concrete','stamped asphalt'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 5,
    actor: 'concrete contractor',
    actor_plural: 'concrete contractors',
    action: 'help you install concrete',
    questions:  [
      {
        field_type:"select",
        question:"What is the project?",
        description:"Scope of project",
        choices:[
          {
            label:"Pour new concrete",
            value:"Pour new concrete"
          },
          {
            label:"Replace my existing concrete",
            value:"Replace my existing concrete"
          }
        ]
      },
      {
        field_type:'checklist',
        question:"Where do you need concrete Installation?",
        description:'Where customer needs concrete Installation',
        choices:[
          {
            label:"Sidewalk",
            value:"Sidewalk"
          },
          {
            label:"Driveway",
            value:"Driveway"
          },
          {
            label:"Floor",
            value:"Floor"
          },
          {
            label:"Foundation",
            value:"Foundation"
          },
          {
            label:"Stairs",
            value:"Stairs"
          },
          {
            label:"Wall",
            value:"Wall"
          },
          {
            label:"Patio & porch",
            value:'Patio & porch'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"What is the approximate square footage of the area that needs concrete installation?",
        description:"The approximate square footage of the area that needs concrete installation"
      },
      {
        field_type:"checklist",
        question:"Will your project need any special applications or finishes?",
        description:"Special applications or finishes",
        choices:[
          {
            label:"Smooth finish",
            value:"Smooth finish"
          },
          {
            label:"Textured finish",
            value:"Textured finish"
          },
          {
            label:"Decorative",
            value:"Decorative"
          },
          {
            label:"Special color or stain",
            value:"Special color or stain"
          },
          {
            label:"Sealing",
            value:"Sealing"
          },
          {
            label:"Lightweight concrete",
            value:'Lightweight concrete'
          },
          {
            label:'I am not sure',
            value:"Customer is not sure"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices:[
          {
            label:"Home",
            value:"Home"
          },
          {
            label:"Multi unit building",
            value:"Multi unit building"
          },
          {
            label:"Office/business",
            value:"Office/business"
          },
          {
            label:"Commercial",
            value:"Commercial"
          }
        ]
      }
    ]
  };
};
    