module.exports = function(parentId) {
  return {
    name: 'Foundation Repair',
    route: 'foundation-repair',
    search_keywords: ['foundation' ,'foundation repair', 'concrete', 'Concrete repair','paver','pavers','concrete paver','concrete pavers','concrete stone','concrete stone patio','concrete forms','repair cement','concrete sealer','cement','concrete resurfacing','foundation repair','stucco repair','quikrete','garage floor coating','step repair','slab repair','spall repair','patio concrete','cement fix','drivway cracks','foundation cracks','sika concrete','basement repair','basement'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'concrete contractor',
    actor_plural: 'concrete contractors',
    action: 'help you with concrete repair',
    questions:  [
   {
    field_type:"checklist",
    question:"What is wrong the foundation?",
    description:"The problem",
    choices:[
    {
        label:"Uneven or sloping floor",
        value:"Uneven or sloping floor"

    },
    {
        label:'Cracked floor or foundation',
        value:"Cracked floor or foundation"
    },
    {
        label:"Cracked wall",
        value:"Cracked wall"
    },
    {
        label:"Part of foundation is washed out, flood damage",
        value:"Part of foundation is washed out, floor damage"
    },
    {
        label:"Cracks or gaps between the wall and floor or ceiling",
        value:"Cracks or gaps between the wall and floor or ceiling"
    },
    {
        can_describe:true
    }
    ]
   },
   {
    field_type:"select",
    question:"What kind of foundation do you have?",
    description:"What kind of foundation",
    choices:[
    {
        label:"Full basement",
        value:"Full basement"
    },
    {
        label:'Slab on grade',
        value:"Slab on grade"
    },
    {
        label:"Pile & grinder",
        value:"Pile & grinder"
    },
    {
        label:"Crawl space",
        value:"Crawl space"
    },
    {
        label:"I need the contractor to help me identify",
        value:"Customer needs your help identify"
    }
    ]
   },
   {
    field_type:'select',
    question:"What kind of property do you have?",
    description:"Kind of home",
    choices:[
    {
        label:"Victorian",
        value:"Victorian"
    },
    {
        label:"Split-level",
        value:"Split-level"
    },
    {
        label:"Ranch",
        value:"Ranch"
    },
    {
        label:"Colonial",
        value:"Colonial"
    },
    {
        label:"Bungalow",
        value:"Bungalow"
    },
    {
        label:"I am not sure",
        value:"I am not sure"
    }
    ]
},
{
    field_type:'select',
    question:"Is there any trees, shrubs or any other obstacles within 4 feet of the building?",
    description:"Obstacles 4 feet of the building",
    choices:[
    {
        label:"Yes, there is but will be cleared prior to work",
        value:"Customer will clear all the obstacles within 4 feet of building"
    },
    {
        label:"Yes, But I need help to removing these",
        value:"Yes, Customer needs your help removing these"
    },
    {
        label:"No, there is no obstacles",
        value:"No, there is no obstacles"
    }
    ]
},
 {
          field_type:"select",
          question:"Have you obtained the required permits",
          description:"Permits",
          choices:[
          {
            label:"Yes, I have the required permits",
            value:"Yes, Customer has the required permits"
          },
          {
            label:"Yes, I am in the process of obtaining the required permits",
            value:"Yes, Customer is in the process of obtaining the required permits"
          },
          {
            label:"No permits required",
            value:"No permits required"
          },
          {
            label:"I am not sure, I need help with permits",
            value:"Customer is not sure about the permits and needs your help"
          }
          ]
        },
        {
            field_type:"text",
            question:"Provide the approximate square footage of your property",
            description:"The approximate square footage of the property"
        }
        ]
    };
};
