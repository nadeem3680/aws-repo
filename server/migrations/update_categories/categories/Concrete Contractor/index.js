'use strict';

exports.isNewRoot=true;

exports.root = {
	name:"Concrete contractor",
	parent:null
};

exports.children= [
require('./concrete-installation'),
require('./concrete-repair'),
require('./foundation-installation'),
require('./foundation-repair')
];