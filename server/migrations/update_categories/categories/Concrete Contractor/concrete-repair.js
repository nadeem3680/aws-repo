module.exports = function(parentId) {
  return {
    name: 'Concrete Repair',
    route: 'concrete-repair',
    search_keywords: ['concrete' ,'concrete repair', 'Driveway concrete', 'Concrete repair','paver','pavers','concrete paver','concrete pavers','concrete stone','concrete stone patio','concrete forms','repair cement','concrete sealer','cement','concrete resurfacing','foundation repair','stucco repair','quikrete','garage floor coating','step repair','slab repair','spall repair','patio concrete','cement fix','drivway cracks','foundation cracks','sika concrete','basement repair','basement'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'concrete contractor',
    actor_plural: 'concrete contractors',
    action: 'help you with concrete repair',
    questions:  [
      {
        field_type:'checklist',
        question:"Where do you need concrete repair?",
        description:'Where customer needs concrete repair',
        choices:[
          {
            label:"Sidewalk",
            value:"Sidewalk"
          },
          {
            label:"Driveway",
            value:"Driveway"
          },
          {
            label:"Floor",
            value:"Floor"
          },
          {
            label:"Foundation",
            value:"Foundation"
          },
          {
            label:"Stairs",
            value:"Stairs"
          },
          {
            label:"Wall",
            value:"Wall"
          },
          {
            label:"Patio & porch",
            value:'Patio & porch'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"What is the approximate square footage of the area that needs repair?",
        description:"The approximate square footage of the area that needs concrete repair"

      },
      {
        field_type:"checklist",
        question:"Will your project need any special applications or finishes?",
        description:"Special applications or finishes",
        choices:[
          {
            label:"Smooth finish",
            value:"Smooth finish"
          },
          {
            label:"Textured finish",
            value:"Textured finish"
          },
          {
            label:"Decorative",
            value:"Decorative"
          },
          {
            label:"Special color or stain",
            value:"Special color or stain"
          },
          {
            label:"Sealing",
            value:"Sealing"
          },
          {
            label:"Lightweight concrete",
            value:'Lightweight concrete'
          },
          {
            label:'I am not sure',
            value:"Customer is not sure"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices:[
          {
            label:"Home",
            value:"Home"
          },
          {
            label:"Multi unit building",
            value:"Multi unit building"
          },
          {
            label:"Office/business",
            value:"Office/business"
          },
          {
            label:"Commercial",
            value:"Commercial"
          }
        ]
      }
    ]
  };
};
    