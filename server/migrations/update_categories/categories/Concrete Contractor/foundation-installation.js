module.exports = function(parentId) {
  return {
    name: 'Foundation Installation',
    route: 'foundation-installation',
    search_keywords: ['foundation' ,'foundation repair', 'concrete', 'Concrete repair','foundation','basement foundation','basement','home foundation','team foundation','foundation contractor','foundation construction','foundation contractors'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'concrete contractor',
    actor_plural: 'concrete contractors',
    action: 'help you with concrete repair',
    questions:  [
   {
    field_type:"select",
    question:"Why are you installing a new foundation?",
    description:"The reason for a new foundation",
    choices:[
    {
        label:"New property",
        value:"New property"
    },
    {
        label:'Addition to existing building',
        value:"Addition to existing building"
    }

    ]
},{
    field_type:'select',
    question:"What kind of property do you have?",
    description:"Kind of home",
    choices:[
    {
        label:"Victorian",
        value:"Victorian"
    },
    {
        label:"Split-level",
        value:"Split-level"
    },
    {
        label:"Ranch",
        value:"Ranch"
    },
    {
        label:"Colonial",
        value:"Colonial"
    },
    {
        label:"Bungalow",
        value:"Bungalow"
    },
    {
        label:"I am not sure",
        value:"I am not sure"
    }
    ]
},
{
 field_type:"select",
    question:"What kind of foundation do you need?",
    description:"What kind of foundation",
    choices:[
    {
        label:"Full basement",
        value:"Full basement"
    },
    {
        label:'Slab on grade',
        value:"Slab on grade"
    },
    {
        label:"Pile & grinder",
        value:"Pile & grinder"
    },
    {
        label:"Crawl space",
        value:"Crawl space"
    },
    {
        label:"I need the contractor to help me identify",
        value:"Customer needs your help identify"
    }
    ]
   },
   {
    field_type:'select',
    question:"Is there any trees, shrubs or any other obstacles within 4 feet of the building?",
    description:"Obstacles 4 feet of the building",
    choices:[
    {
        label:"Yes, there is but will be cleared prior to work",
        value:"Customer will clear all the obstacles within 4 feet of building"
    },
    {
        label:"Yes, But I need help to removing these",
        value:"Yes, Customer needs your help removing these"
    },
    {
        label:"No, there is no obstacles",
        value:"No, there is no obstacles"
    }
    ]
},
{
    field_type:"select",
    question:"How many stories will be on your foundation",
    description:"Number of stories to be on the foundation",
    choices:[
    {
        label:"Single level",
        value:"Single level"
    },
    {
        label:"Two story building",
        value:"Two story building"
    },
    {
        label:"Taller than a two story building",
        value:"Taller than a two story building"
    }
    ]
},
{
          field_type:"select",
          question:"Have you obtained the required permits",
          description:"Permits",
          choices:[
          {
            label:"Yes, I have the required permits",
            value:"Yes, Customer has the required permits"
          },
          {
            label:"Yes, I am in the process of obtaining the required permits",
            value:"Yes, Customer is in the process of obtaining the required permits"
          },
          {
            label:"No permits required",
            value:"No permits required"
          },
          {
            label:"I am not sure, I need help with permits",
            value:"Customer is not sure about the permits and needs your help"
          }
          ]
        },
        {
            field_type:"text",
            question:"Provide the approximate square footage of your property",
            description:"The approximate square footage of the property"
        }
        ]
    };
};

