'use strict';

exports.isNewRoot=false;

exports.root = {
  name: "Painting",
  parent: null
};

exports.children = [
  require('./interior-painting'),
  require('./exterior-painting')
];