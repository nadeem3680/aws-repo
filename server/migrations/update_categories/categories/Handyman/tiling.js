module.exports = function(parentId) {
  return {
    name: 'Tiling',
    route: 'tiling',
    search_keywords: ['bathroom', 'tiling','bathroom tiling', 'tile','kitchen tiling','kitchen','bathroom','handyman','natural stone','Porcelain','ceramic','marble','stone tile','mosaic','Porcelain tile','Porcelain tiles','mosaic tiles','ceramic tiles','carpert','carpet tiles','bathroom floor','flooring','vynil tile','wall tiling','tiling walls','tiling bathroom walls','outdoor tiles','outdoor tiling','kitchen mosaic tiles','glass backsplash','tile installation','install tile','bathroom tile installation','marble installation','white tile','laying tiles','lay tile','how to lay tiles','handyman company','contractor'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 2,
    actor: 'Handyman',
    actor_plural: 'Handymen',
    action: 'help you with your tiling project',
    questions: [
      {
        field_type:"checklist",
        question:"What room(s) are you tilling?",
        description:"Room(s) needs tiling",
        choices:[
          {
            label:"Kitchen",
            value:"Kitchen"
          },
          {
            label:"Bathroom",
            value:"Bathroom"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"select",
        question:"What is the project?",
        description:"Type of project",
        choices:[
          {
            label:"Repair/Refinish",
            value:'Repair/Refinish'
          },
          {
            label:"Replacement",
            value:"Replacement"
          },
          {
            label:"Install",
            value:"Install"
          }
        ]
      },
      {
        field_type:"checklist",
        question:"What type of tile?",
        description:"Type of tile",
        choices:[
          {
            label:"Ceramic",
            value:"Ceramic"
          },
          {
            label:"Natural stone",
            value:"Natural stone"
          },
          {
            label:"Porcelain",
            value:"Porcelain"
          },
          {
            label:"Mosaic",
            value:"Mosaic"
          },
          {
            label:"I am not sure",
            value:"Customer is not sure"
          }

        ]
      },
      {
        field_type:"checklist",
        question:"Where?",
        description:"Where",
        choices:[
          {
            label:"Floor",
            value:"Floor"
          },
          {
            label:"Countertop/Vanity",
            value:"Countertop/Vanity"
          },
          {
            label:"Wall",
            value:"Wall"
          },
          {
            label:'Tub/Shower enclosure',
            value:"Tub/Shower enclosure"
          }
        ]
      }
    ]
  };
};
