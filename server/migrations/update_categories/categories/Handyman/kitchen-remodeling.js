module.exports = function(parentId) {
  return{
  name: 'Kitchen Remodeling',
  route: 'kitchen-remodeling',
  search_keywords: ['Kitchen contractor','Kitchen', 'major home repair', 'expand the kitchen','kitchen remodeling','contractor','general contractor','handyman','kitchen cabinets','kitchen designs','home renovation','how to renovate a kitchen','how to remodel a kitchen','plumber','contemporary kitchen','diy kitchen','diy kitchen cabinets','custom kitchen','custom kitchens','kitchen planning','kitchen plans','kitchen floor plans','kitchen Countertop','custom kitchen plans','appliance','kitchen appliance'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you remodel your kitchen',
        questions:  [
        {
        	field_type:"checklist",
        	question:'What is the project?',
        	description:"Project",
        	choices:[
        	{
        		label:"Update the kitchen's look",
        		value:"Update the kitchen's look"
        	},
        	{
        		label:"Expand the kitchen",
        		value:"Expand the kitchen"
        	},
        	{
        		label:"Replace appliances",
        		value:'Replace appliances'
        	},
        	{
        		label:"Fix water damages",
        		value:"Fix water damages"
        	},
        	{
        		can_describe:true
        	}
        	]
        },
        {
        	field_type:"checklist",
        	question:"What kitchens features will you be adding or replacing?",
        	description:"Kitchen features that customer will be adding or replacing",
        	choices:[
        	{
        		label:"I am not adding or replacing any kitchen features",
        		value:'Customer is not adding or replacing any kitchen features'
        	},
        	{
        		label:'Cabinets',
        		value:"Cabinets"
        	},
        	{
        		label:'Appliances',
        		value:'Appliances'
        	},
        	{
        		label:"Countertop",
        		value:"Countertop"
        	},
        	{
        		label:"Sink",
        		value:"Sink"
        	},
        	{
        		label:'Island',
        		value:'Island'
        	},
        	{
        		label:"lights",
        		value:'lights'
        	},
        	{
        		can_describe:true
        	}
        	]	
        },
        {
            field_type:"select",
            question:'Do you already have plans or design?',
            description:'design and plans',
            choices:[
            {
                label:"No existing plans, but I have an idea what I want",
                value:"No existing plans, but customer has an idea what she/he wants"
            },
            {
                label:"No existing plans, need design suggestions",
                value:"No existing plans, customer needs design suggestions"
            },
            {
                label:"Already have plans",
                value:"Customer has a plan or design"
            }
            ]
        },
        {
        	field_type:"select",
        	question:"Will the sink or dish washer be relocated?",
        	description:"Will the sink or dish washer be relocated",
        	choices:[
        	{
        		label:'Yes',
        		value:'Yes'
        	},
        	{
        		label:'No',
        		value:'No'
        	}
        	]
        },
        {
        	field_type:"checklist",
        	question:"What cabinetry work do you need?",
        	description:"Type of cabinetry work do you",
        	choices:[
        	{
        		label:'No cabinetry work needed',
        		value:"No cabinetry work needed"
        	},
        	{
        		label:'Refinishing existing cabinets',
        		value:"Refinishing existing cabinets"
        	},
        	{
        		label:'Replace existing cabinets',
        		value:"Replace existing cabinets"
        	},
        	{
        		label:'Build cabinets in a location that did not previously have them',
        		value:"Build cabinets in a location that did not previously have them"
        	},
        	{
        		can_describe:true
        	}
        	]
        },
        {
        	field_type:"select",
        	question:"Will you need the kitchen painted?",
        	description:"Will the kitchen be painted",
        	choices:[
        	{
        		label:'Yes, I will need the kitchen painted',
        		value:"Yes"
        	},
        	{
        		label:'No',
        		value:'No'
        	}
        	]
        },
        {
            field_type:"text",
            question:"Provide the approximate square footage of the project and other details",
            description:"The approximate square footage and other details"
        },
        {
        	field_type:'text',
        	question:"How old the property is?",
        	description:"Age of the property"
        }
        ]
    };
};
