module.exports = function(parentId) {
  return{
  name: 'Home Remodeling',
  route: 'home-remodeling',
  search_keywords: ['renovation','house renovation', 'major home repair', 'home remodeling','contractor','genral contractor','handyman','frame','carpentry','basement','basement renovation','basement remodeling','bathroom remodeling','interior design','interior designer','home renovations','home design','custom home','renovation ideas','remodeling ideas','basement design','remodeling ideas','home remodeling','remodel my home','remodel my basement','home improvement','home improvements','house remodeling','custom house','contractors','contracting companies','renovation companies','companies remodeling','kitchen remodeling','remodeling my kitchen','renovation my kitchen','changing my floor','renovation software','home interior','home additions','home addition plans','home addition','house decoration','better homes','interior home improvement','interior home improvements','interior renovation','interior','house addition','house additions','building home','house building','building','mr handyman'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you remodel your home',
        questions:  [
        {
          field_type:'checklist',
          question:"Reason for the remodeling",
          description:"Reason for the remodeling",
          choices:[
          {
            label:'Add property value',
            value:'Add property value'
          },
          {
            label:"Add functionality to home",
            value:'Add functionality to home'
          },
          {
            label:'Disaster damage',
            value:'Disaster damage'
          },
          {
            label:"Energy efficiency",
            value:"Energy efficiency"
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'text',
          question:"When do you want to start the work?",
          description:"When customer wants the work start",

        },
        {
          field_type:"checklist",
          question:"What items will be renovated",
          description:"Items that need renovation",
          choices:[
          {
            label:"Siding",
            value:"Siding"
          },
          {
            label:"Flooring",
            value:"Flooring"
          },
          {
            label:'Roof',
            value:'Roof'
          },
          {
            label:"Windows",
            value:"Windows"
          },
          {
            label:"Doors",
            value:"Doors"
          },
          {
            label:'Cabinet & counters',
            value:'Cabinet & counters'
          },
          {
            label:"Electrical & lighting",
            value:"Electrical & lighting"

          },
          {
            label:"Pips & plumbing",
            value:"Pips & plumbing"
          },
          {
            label:"Appliances",
            value:"Appliances"
          },
          {
            label:"Heating & cooling",
            value:"Heating & cooling"
          },
          {
            can_describe:true
          }
          ]
        },
        {
            field_type:"select",
            question:'Do you already have plans or design?',
            description:'design and plans',
            choices:[
            {
                label:"No existing plans, but I have an idea what I want",
                value:"No existing plans, but customer has an idea what she/he wants"
            },
            {
                label:"No existing plans, need design suggestions",
                value:"No existing plans, customer needs design suggestions"
            },
            {
                label:"Already have plans",
                value:"Customer has a plan or design"
            }
            ]
        },
        {
          field_type:"select",
          question:"Have you obtained the required permits",
          description:"Permits",
          choices:[
          {
            label:"Yes, I have the required permits",
            value:"Yes, Customer has the required permits"
          },
          {
            label:"Yes, I am in the process of obtaining the required permits",
            value:"Yes, Customer is in the process of obtaining the required permits"
          },
          {
            label:"No permits required",
            value:"No permits required"
          },
          {
            label:"I am not sure, I need help with permits",
            value:"Customer is not sure about the permits and needs your help"
          }
          ]
        },
        {
          field_type:"text",
          question:"Please describe your project in more detail",
          description:"Project in more detail"

        },
        {
            field_type:"text",
            question:"Provide the approximate square footage of your property",
            description:"The approximate square footage of the property"
        },
        {
          field_type:'text',
          question:"How old the property is?",
          description:"Age of the property"
        }


          ]
        };
      };