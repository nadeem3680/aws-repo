module.exports = function(parentId) {
  return{
    name: 'General contracting',
    route: 'general-contractor',
    search_keywords: ['general contractor', 'handyman', 'renovation', 'major home repair', 'broken glass','win','wind','windo','renovation','remodeling','repairman','home renovation','roofer','roofing','custom house','home building','renovation costs','interior improvement','home additions','house remodeling','carpentry','home frame','basement repair','home repair','home repairs','garage addition','kitchen remodeling','kitchen improvements','kitchen','basement','contractors','contracting company','construction','store renovation','home builders','home builder','custom home builder','store remodeling','contracting company','construction company','commercial general','commercial general contractor','licensed contractor'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 4,
    actor: 'Handyman',
    actor_plural: 'Handymen',
    action: 'help you repair,install and renovate',
    questions:  [
      {
        field_type:"select",
        question:"What is the job?",
        description:"What project is",
        choices:[
          {
            label:'One or more small projects',
            value:"One or more small projects"
          },
          {
            label:"Major repair",
            value:'Major repair'
          },
          {
            label:"Major remodeling or renovation",
            value:"Major remodeling or renovation"
          },
          {
            label:"Addition to building",
            value:"Addition to building"
          }
        ]
      },
      {
        field_type:"checklist",
        question:"What areas need work?",
        description:'What needs work',
        choices:[
          {
            label:"Family room",
            value:"Family room"
          },
          {
            label:"Living room",
            value:"Living room"
          },
          {
            label:"Kitchen",
            value:"Kitchen"
          },
          {
            label:"Bedrooms",
            value:"Bedrooms"
          },
          {
            label: "Basement",
            value:"Basement"
          },
          {
            label:"Garage",
            value:"Garage"
          },
          {
            label:"Home Office",
            value:"Home Office"
          },
          {
            label:'Business Office',
            value:'Business Office'
          },
          {
            label:'Shop',
            value:'Shop'
          },
          {
            con_describe:true
          }
        ]
      },
      {
        field_type:"checklist",
        question:"How far along are you?",
        description:"How far along the customer is",
        choices:[
          {
            label:"I'm just starting",
            value:"Customer is just starting"
          },
          {
            label:"I have sketches and/or basic idea of project",
            value:"Customer has sketches or basic idea of project"
          },
          {
            label:"I have professional drawings",
            value:"Customer has a complete set of plans (professional drawing)"
          },
          {
            label:"I have permits pulled",
            value:"Customer has a permits pulled"
          }
        ]
      },
      {
        field_type:"text",
        question:"Provide the approximate square footage of the project and other details",
        description:"The approximate square footage and other details"
      },
      {
        field_type:"select",
        question: "What type of property do you have?",
        description: "Type of property",
        choices:[
          {
            label:"Home",
            value:"Home"
          },
          {
            label:"Multi unit building",
            value:"Multi unit building"
          },
          {
            label:"Office/business",
            value:"Office/business"
          },
          {
            label:"Commercial",
            value:"Commercial"
          },


        ]
      },
    ]
  };
};


        
