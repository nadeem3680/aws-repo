module.exports = function(parentId) {
  return {
  name: 'Handyman',
  route: 'handyman',
  search_keywords: ['handyman','han','hand','handy','handym','handyma','plumbing','plumber','electrician','painting','home renovation','renovation','remodeling','repairman','fix','fix pipes','fix toilet','general contractor','contractor'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'handyman',
        actor_plural: 'handymen',
        action: 'help you repair, install and assemble',
        questions:  [
        {
          field_type:"text",
          question:"Describe your project?",
          description:"The project"
        },
        ]
      };
    };