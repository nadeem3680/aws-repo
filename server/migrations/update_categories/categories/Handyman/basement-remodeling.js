module.exports = function(parentId) {
  return{
  name: 'Basement Remodeling',
  route: 'Basement-remodeling',
  search_keywords: ['renovation','house renovation', 'major home repair', 'home remodeling','basement','basement remodeling','contractor','genral contractor','handyman','frame','carpentry','basement','basement renovation','basement remodeling','bathroom remodeling','interior design','interior designer','home renovations','home design','custom home','renovation ideas','remodeling ideas','basement design','remodeling ideas','home remodeling','remodel my home','remodel my basement','home improvement','home improvements','house remodeling','custom house','contractors','contracting companies','renovation companies','companies remodeling','kitchen remodeling','remodeling my kitchen','renovation my kitchen','changing my floor','renovation software','home interior','home additions','home addition plans','home addition','house decoration','better homes','interior home improvement','interior home improvements','interior renovation','interior','house addition','house additions','building home','house building','building','mr handyman'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'contractor',
        actor_plural: 'contractors',
        action: 'help you remodel your basement',
        questions:  [
        {
          field_type:"checklist",
          question:"Why are you remodeling your basement",
          description:"The reason for remodeling",
          choices:[
          {
            label:"Finish an unfinished basement",
            value:"Finish an unfinished basement"
          },
          {
            label:"Finish a partially finished basement",
            value:'Finish a partially finished basement'
          },
          {
            label:"Remodel a finished basement",
            value:"Remodel a finished basement"
          },
          {
            label:"Fix water damage",
            value:"Fix water damage"
          },
          {
            can_describe:true
          }
          ]
        },
        {
         field_type:'text',
          question:"When do you want to start the work?",
          description:"When customer wants the work start",

        },
        {
            field_type:"select",
            question:'Do you already have plans or design?',
            description:'design and plans',
            choices:[
            {
                label:"No existing plans, but I have an idea what I want",
                value:"No existing plans, but customer has an idea what she/he wants"
            },
            {
                label:"No existing plans, need design suggestions",
                value:"No existing plans, customer needs design suggestions"
            },
            {
                label:"Already have plans",
                value:"Customer has a plan or design"
            }
            ]
        },
        {
           field_type:'checklist',
           question:"Will you be adding or replacing any of basement rooms?",
           description:"Rooms to be replaced or added",
           choice:[
           {
            label:'I am not adding or replacing any basement rooms',
            value:"I am not adding or replacing any basement rooms"
           },
           {
            label:"Bathroom(s)",
            value:"Bathroom(s)"
           },
           {
            label:"Bedroom(s)",
            value:"Bedroom(s)"
           },
           {
            label:'Office',
            value:"Office"
           },
           {
            label:"Home theater",
            value:'Home theater'
           },
           {
            label:"Kitchen",
            value:'Kitchen'
           },
           {
            label:'Exercise area',
            value:'Exercise area'
           },
           {
            label:"Storage area",
            value:"Storage area"
           },
           {
            label:"Laundry",
            value:'Laundry'
           },
           {
            label:"Separate apartment",
            value:"Separate apartment"
           },
           {
            can_describe:true
           }
           ]
        },
        {
          field_type:'checklist',
          question:"What features will you be adding or replacing",
          description:"Basement features to be added or replaced",
          choices:[
          {
            label:'I am not adding or replacing any room features',
            value:'I am not adding or replacing any room features'
          },
          {
            label:'Flooring',
            value:'Flooring'
          },
          {
            label:'Wall(s)',
            value:"Wall(s)"
          },
          {
            label:'Window(s)',
            value:"Window(s)"
          },
          {
            label:'Door(s)',
            value:'Door(s)'
          },
          {
            label:'Ceiling',
            value:"Ceiling"
          },
          {
            label:'Trim',
            value:"Trim"
          },
          {
            label:'Built-in shelving',
            value:"Built-in shelving"
          },
          {
            label:"Lights",
            value:"Lights"
          },
          {
            label:'Fireplace',
            value:'Fireplace'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:'Will any of these features be relocated?',
          description:"Features to be relocated",
          choices:[
          {
            label:"No feature will be moved to a new location",
            value:'No features will be moved to a new location '
          },
          {
            label:'Outlets',
            value:"Outlets"
          },
          {
            label:'Switches',
            value:'Switches'
          },
          {
            label:'Lighting',
            value:'Lighting'
          },
          {
            label:'Heating and cooling vents',
            value:'Heating and cooling vents'
          },
          {
            can_describe:true
          }
          ]
        },
         {
          field_type:"select",
          question:"Will you need the basement painted?",
          description:"Will the basement be painted",
          choices:[
          {
            label:'Yes, I will need the kitchen painted',
            value:"Yes"
          },
          {
            label:'No',
            value:'No'
          }
          ]
        },
        {
            field_type:"text",
            question:"Provide the approximate square footage of the project and other details",
            description:"The approximate square footage and other details"
        },
        {
          field_type:'text',
          question:"How old the property is?",
          description:"Age of the property"
        }
        ]
    };
};
