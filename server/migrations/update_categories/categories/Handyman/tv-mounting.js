module.exports = function(parentId) {
  return {
        name: 'TV Mounting',
        route: 'tv-mounting',
        search_keywords: ['tv', 'tv mounting','handyman','appliance','tv installation','tv mount installation','tv wall mount installation','wall mouting tv','tv brackets','tv mounting','tv ceiling mount','wall mount tv stands','handyman','fireplace tv','mount fireplace','vesa mount','mounting tv over fireplace','lcd tv mount','lcd tv','lcd','screen mount','screen mounting'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'Handyman',
        actor_plural: 'Handymen',
        action: 'help you mount your TV',
        questions: [
        {
            field_type:"select",
            question:"What material is your wall made of?",
            description:"Wall material",
            choices:[
            {
                label:"Brick",
                value:"Brick"
            },
            {
                label:"Drywall",
                value:"Drywall"
            },
            {
                label:"Concrete",
                value:"Concrete"
            },
            {
                label:"I do not want a mount wall",
                value:"Customer does not want a wall mount"
            },
            {
                label:"I am not sure",
                value:"Customer is not sure"
            }
            ]
        },
        {
            field_type:"select",
            question:"Do you have a mount or stand?",
            description:"Customer has mount or stand",
            choices:[
            {
                label:"Yes",
                value:"Yes"
            },
            {
                label:"No, help me select a mount/stand",
                value:"No, help me select a mount/stand"
            }
            ]
        },
        {
            field_type:"select",
            description:"Wires to be concealed",
            question:"Do you want to conceal your wires?",
            choices:[
            {
                label:"Yes",
                value:"Yes"
            },
            {
                label:"No",
                value:"No"
            }
            ]
        },
       {
        field_type:"checklist",
        question:"What additional devices will be connected to the TV?",
        description:"Additional devices to connected to TV",
        required:false,
        choices:[
        {
            label:"Cable/satellite box",
            value:"Cable/satellite box"
        },
        {
            label:"Sound system",
            value:"Sound system"
        },
        {
            label:"DVD/BluRey player",
            value:"DVD/BluRey player"
        },
        {
            can_describe:true
        }
        ]
       }
       ]
   };
};
      


       