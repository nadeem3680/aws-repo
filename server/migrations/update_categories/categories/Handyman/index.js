'use strict';

exports.isNewRoot = false,
  exports.root = {
    name:"Handyman",
    parent:null
  };

exports.children=[
  require('./handyman'),
// New
  require('./basement-remodeling'),
  require('./general-contractor'),
  require('./home-remodeling'),
  require('./kitchen-remodeling'),
  require('./tiling'),
  require('./tv-mounting')
];