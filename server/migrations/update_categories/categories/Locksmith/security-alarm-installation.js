module.exports = function(parentId) {
  return {
        name: 'Security & Alerts Installation',
        route: 'security-alarm-installation',
        search_keywords:['security','home security','window sensors','sensors','smoke','camera','Reposition','Reprogram','buzzer','intercom','security alert','smoke detector','smoke alarm','carbon monoxide sensor','home survailince','home camera','security company','cheap security','wireless security','diy','alarm','commercial','store camera','monitoring','wireless','brinks','adt','video','lock','locksmith','survailince','security company','home security companies','home security company'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'locksmith',
        actor_plural: 'locksmiths',
        action: 'help you install your security & alert system',
        questions: [
        {
            field_type:'checklist',
            question:'What features do want with your security & alert system?',
            description:'Features that customer wants with the security & alert system',
            choices:[
            {
                label:'Cameras',
                value:'Cameras'
            },
            {
                label:'Camera monitoring system + DVR',
                value:'Camera monitoring system + DVR'
            },
            {
                label:'Remote camera viewing access',
                value:'Remote camera viewing access'
            },
            {
                label:'Window and doors sensors',
                value:'Window and doors sensors'
            },
            {
                label:'Motion sensors',
                value:'Motion sensors'
            },
            {
                label:'Smoke alarms',
                value:'Smoke alarms'
            },
            {
                label:'Carbon monoxide sensors',
                value:'Carbon monoxide sensors'
            },
            {
                label:'Door buzzer and intercom system',
                value:'Door buzzer and intercom system'
            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'
            },
            {
                can_describe:true
            }
            ]

        },
        {
            field_type:'select',
            question:'How many cameras do you need installed?',
            description:'Number of cameras needed by the customer',
            choices:[
            {
                label:'0',
                value:'0'
            },
            {
                label:'1',
                value:'1'
            },
            {
                label:'2',
                value:'2'

            },
            {
                label:'3',
                value:'3'

            },
            {
                label:'4',
                value:'4'

            },
            {
                label:'5',
                value:'5'

            },
            {
                label:'More than 5',
                value:'More than 5'

            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'

            }


            ]
        },
        {
            field_type:'select',
            question:'How many rooms do you need security for?',
            description:'Number of rooms that need security system',
            choices:[
            {
                label:'1',
                value:'1'
            },
            {
                label:'2',
                value:'2'

            },
            {
                label:'3',
                value:'3'

            },
            {
                label:'4',
                value:'4'

            },
            {
                label:'5',
                value:'5'

            },
            {
                label:'More than 5',
                value:'More than 5'

            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'

            }

            ]
        },
        {
            field_type:'select',
            question:'How many entrance do you need security for?',
            description:'Number of entrance that need security system',
            choices:[
            {
                label:'1',
                value:'1'
            },
            {
                label:'2',
                value:'2'

            },
            {
                label:'3',
                value:'3'

            },
            {
                label:'4',
                value:'4'

            },
            {
                label:'5',
                value:'5'

            },
            {
                label:'More than 5',
                value:'More than 5'

            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'

            }

            ]
        },
        {
            field_type:'select',
            question:'How many windows do you need security for?',
            description:'Number of windows that need security system',
            choices:[
            {
                label:'1',
                value:'1'
            },
            {
                label:'2',
                value:'2'

            },
            {
                label:'3',
                value:'3'

            },
            {
                label:'4',
                value:'4'

            },
            {
                label:'5',
                value:'5'

            },
            {
                label:'More than 5',
                value:'More than 5'

            },
            {
                label:'As recommended by professional',
                value:'As recommended by professional'

            }

            ]
        },
        {
            field_type:'text',
            question:'What is the approximate square footage of your property?',
            description:'The approximate square footage of the property'
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};

