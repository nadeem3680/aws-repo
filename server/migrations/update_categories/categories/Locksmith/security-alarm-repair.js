module.exports = function(parentId) {
  return {
        name: 'Security & Alerm Repair Modification',
        route: 'security-alarm-repair',
        search_keywords: ['security','home security','window sensors','sensors','smoke','camera','Reposition','Reprogram','buzzer','intercom','security alert','smoke detector','smoke alarm','carbon monoxide sensor','home survailince','home camera','security company','cheap security','wireless security','diy','alarm','commercial','store camera','monitoring','wireless','brinks','adt','video','lock','locksmith','survailince','security company','home security companies','home security company'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'locksmith',
        actor_plural: 'locksmiths',
        action: 'help you install your security & alert system',
        questions: [
        {
            field_type:'checklist',
            question:'What needs repair?',
            description:'What needs repair',
            choices:[
            {
                label:'No repairs needed',
                value:'No repairs needed'
            },
            {
                label:'Cameras',
                value:'Cameras'
            },
            {
                label:'Camera monitoring system + DVR',
                value:'Camera monitoring system + DVR'
            },
            
            {
                label:'Window and doors sensors',
                value:'Window and doors sensors'
            },
            {
                label:'Motion sensors',
                value:'Motion sensors'
            },
            {
                label:'Smoke alarms',
                value:'Smoke alarms'
            },
            {
                label:'Carbon monoxide sensors',
                value:'Carbon monoxide sensors'
            },
            {
                label:'Door buzzer and intercom system',
                value:'Door buzzer and intercom system'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:'What Modification do you need?',
            description:'Modification customer needs',
            choices:[
            {
                label:'No modification needed',
                value:'No modification needed'
            },
            {
                label:'Reposition existing cameras',
                value:'Reposition existing cameras'
            },
            {
                label:'Reprogram system software',
                value:'Reprogram system software'
            },
            {
                label:'Reposition existing sensors',
                value:'Reposition existing sensors'
            },
            {
                label:'Replace my existing components with most updated ones',
                value:'Replace my existing components with most updated ones'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:'What additions do you need for your existing system?',
            description:'Additions customer wants to the existing system',
            choices:[
            {
                label:'No additions',
                value:'No additions'
            },
           {
                label:'Cameras',
                value:'Cameras'
            },
            {
                label:'Camera monitoring system + DVR',
                value:'Camera monitoring system + DVR'
            },
            
            {
                label:'Window and doors sensors',
                value:'Window and doors sensors'
            },
            {
                label:'Motion sensors',
                value:'Motion sensors'
            },
            {
                label:'Smoke alarms',
                value:'Smoke alarms'
            },
            {
                label:'Carbon monoxide sensors',
                value:'Carbon monoxide sensors'
            },
            {
                label:'Door buzzer and intercom system',
                value:'Door buzzer and intercom system'
            },
            {
                can_describe:true
            }
            ]
        },
            {
            field_type:'text',
            question:'What is the approximate square footage of your property?',
            description:'The approximate square footage of the property'
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};

