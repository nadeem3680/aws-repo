module.exports = function(parentId) {
  return {
        name: 'Locksmith',
        route: 'locksmith',
        search_keywords: ['locksmith','home security','security alert','smoke detector','smoke alarm','carbon monoxide sensor','home survailince','key','keys','lock','Doorknob','doorknow','door','door installation','auto locksmith','lock picking','locksmith services','locksmith toronto','commercial locksmith','home','door lock','window'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'locksmith',
        actor_plural: 'locksmiths',
        action: 'help you install your locking system',
        questions: [
        {
            field_type:'checklist',
            question:'What service do you need?',
            description:'Services needed by the customer',
            choices:[
            {
                label:'Install or replace current lock system',
                value:'Install or replace current lock system'
            },
            {
                label:'Re-key lock for different keys to work',
                value:'Re-key lock for different keys to work'
            },
            {
                label:'Repair broken lock',
                value:'Repair broken lock'
            },
            {
                label:'Locked out',
                value:'Locked out'
            },
            {
                label:'Copy keys',
                value:'Copy keys'
            },
            {
                can_describe:true
            }

            ]
        },
        {
            field_type:'checklist',
            question:'What kind of lock needs work?',
            description:'Type of lock needed',
            choices:[
            {
                label:'Deadbolt',
                value:'Deadbolt'
            },
            {
                label:'Doorknob',
                value:'Doorknob'
            },
            {
                label:'Mailbox',
                value:'Mailbox'
            },
            {
                label:'Office furniture like filing cabinet',
                value:'Office furniture like filing cabinet'
            },
            {
                label:'Electronic combination pad',
                value:'Electronic combination pad'
            },
            {
                label:'Keyless remote',
                value:'Keyless remote'
            },
            {
                can_describe:true
            }
            ]
        },
        {
            field_type:'checklist',
            question:'Where is the lock?',
            description:'Where the lock',
            choices:[
            {
                label:'Vehicle',
                value:'Vehicle'
            },
            {
                label:'Home',
                value:'Home'
            },
            {
                label:'Multi-unit building',
                value:'Multi-unit building'
            },
            {
                label:'Office/business',
                value:'Office/business'
            },
            {
                label:'Commercial',
                value:'Commercial'
            },
            {
                can_describe:true
            }
            ]
        }
        ]
    };
};