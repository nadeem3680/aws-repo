'use strict';

exports.isNewRoot = true;

exports.root = {
	name: "Home Security",
	parent: null
};

exports.children=[
require('./locksmith'),
require('./security-alarm-installation'),
require('./security-alarm-repair')
];