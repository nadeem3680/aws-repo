module.exports = function(parentId) {
  return {
    name: 'Long Distance Moving',
    route: 'long-distance-moving',
    search_keywords: ['Moving' ,'Mov', 'M', 'Moving out',"moving service","two guys one truck",'movers','moving boxes','moving quote','storage','moving companies','haul','hauling','moving truck','moving van','trucks','van','piano moving','house','moving house','moving to vancouver','moving to toronto','moving to new york','ny moving','international moving','distance','long distance','long','movers long','van lines','Furniture','junk removal'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'Mover',
    actor_plural: 'Movers',
    action: 'help you move long distance',
    questions:  [
    {
        field_type:"select",
        question:"What are you moving?",
        description:"What needs to be moved",
        choices:[
          {
            label:"Just a few items",
            value:"Just a few items"
          },
          {
            label:"Studio or one bedroom apartment",
            value:"Studio or one bedroom apartment"
          },
          {
            label:"2 or 3 bedroom apartment",
            value:"2 or 3 bedroom apartment"
          },
          {
            label:"2 or 3 bedroom house",
            value:"2 or 3 bedroom house"
          },
          {
            label:"3 or 4 bedroom house",
            value:"3 or 4 bedroom house"
          },
          {
            label:"More than 4 bedroom house",
            value:"More than 4 bedroom house"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"checklist",
        question:"What other services do you need?",
        description:"Other services needed",
        choices:[
          {
            label:"Packing",
            value:"Packing"
          },
          {
            label:"Furniture assembly and disassembly",
            value:"Furniture assembly and disassembly"
          },
          {
            label:"Move large items through stairs",
            value:"Move large items through stairs"
          },

          {
            can_describe:true
          }
        ]
      },
        {
        field_type:"select",
        question:"How many small/medium boxes/items need to be moved?",
        description:"Number of small and medium boxes or items to be moved",
        choices:[
          {
            label:"No small/medium item",
            value:"None"
          },
          {
            label:"1-10",
            value:"1-10"
          },
          {
            label:"10-20",
            value:"10-20"
          },
          {
            label:"20-50",
            value:"20-50"
          },
          {
            label:"50-80",
            value:"50-80"
          },
          {
            label:"More than 80",
            value:"More than 80"
          }
        ]
      },
      {
        field_type:"select",
        question:"How many large items to be moved?",
        description:"Number of items or items that need to be moved",
        choices:[
          {
            label:"No small/medium item",
            value:"0"
          },
          {
            label:"1-10",
            value:"1-10"
          },
          {
            label:"10-20",
            value:"10-20"
          },
          {
            label:"20-30",
            value:"20-30"
          },
          {
            label:"30-50",
            value:"30-50"
          },
          {
            label:"More than 50",
            value:"More than 50"
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"text",
        question:"Please enter the postal code of the location you are moving to or city name",
        description:"Postal code the location the customer is moving to or city name"
      }
    ]
  };
};