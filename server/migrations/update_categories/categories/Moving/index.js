'use strict';

exports.isNewRoot=false;

exports.root ={
  name:"Moving",
  parent:null
};

exports.children = [
  require('./moving'),
  // new
  require('./long-distance-moving')
];