module.exports = function(parentId) {
  return {
    name: 'Home Staging',
    route: 'home-staging',
    search_keywords: ['interior' , 'design', 'designer', 'interior design', 'home staging', 'staging','real estate staging','home staging','sell my house','sale staging','staging a home for sale','selling a house','staging company','architecture company','interior architecture','home staging furniture','furniture','furniture for staging','stager','house stager','home stager','stagers','real estate home staging','real estate','real estate stagers'],
    parent: parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 4,
    actor: 'interior designer',
    actor_plural: 'interior designers',
    action: 'help you design a perfect space',
    questions: [
    {
        field_type:"checklist",
        question:"Why do you need staging?",
        description:"The reason for the request",
        choices:[
        {
            label:"Selling home",
            value:"Selling home"
        },
        {
            label:"Renting out home",
            value:"Renting out home"
        },
        {
            label:'Re-styling',
            value:'Re-styling'
        },
        {
            can_describe:true
        }
        ]
    },
    {
        field_type:"checklist",
        question:'Which rooms need staging?',
        description:"Rooms to be stagged",
        choices:[
        {
            label:"Entire interior of home",
            value:'Entire interior of home'
        },
        {
            label:"Exterior of home",
            value:"Exterior of home"
        },
        {
            label:'Bedrooms',
            value:'Bedrooms'
        },
        {
            label:"Living room",
            value:'Living room'
        },
        {
            label:'Dining room',
            value:"Dining room"
        },
        {
            label:"Kitchen",
            value:"Kitchen"
        },
        {
            label:"Bathroom(s)",
            value:"Bathroom(s)"
        }
        ]
    },
    {
        field_type: "text",
        question: "What is the approximate square footage of the area that needs staging?",
        description: "Approximate square footage of the area that needs staging"
   }
   ]
};
};