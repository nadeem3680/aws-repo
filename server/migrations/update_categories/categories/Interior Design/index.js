'use strict';

exports.isNewRoot = false;

exports.root = {
  name:"Interior Designing",
  parent: null
};

exports.children = [
  require('./interior-design'),
  require('./home-staging')
];