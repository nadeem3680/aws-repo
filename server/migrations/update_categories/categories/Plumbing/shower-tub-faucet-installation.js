module.exports = function(parentId) {
  return {
        name: 'Shower & Bathtub Faucet  Installation',
        route: 'shower-tub-faucet-installation',
        search_keywords: [ 'plumber','shower installation', 'bathtube installation','shower faucet installation','shower faucet','tub faucet','tub faucet installation','shower veleves','tub faucet','velves','faucet replacement','replacing shower','bathroom faucet','bathroom shwoer faucet','bath faucets','handyman','tub faucets','fix shower','repair bathroom','bathtub faucet','fauce','plumbing','plumber','old bathtub faucet','bathtub','old bathub faucet replacement','replacing bathtub','plumbing fixture','plumbing fixtures','leaky faucet','leaky bathtub','monitor','tub shower','cartridge','shower faucet replacement'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'help you install your shower & tub installation',
        questions: [
        {
            field_type:"select",
            question:'What is the project?',
            description:'Type of project',
            choices:[
            {
                label:'Replace',
                value:'Replace'
            },
            {
                label:'Just intall',
                value:'Just install'
            }
            ]
        },
        
        {
            field_type:'select',
            question:'How many faucets are you installing',
            description:'Number of faucets to be installed',
            choices:[
            {
                label:'One',
                value:'One'
            },
            {
                label:'Two',
                value:'Two'
            },
            {
                label:'Three',
                value:'Three'

            },
            {
                label:'More than three',
                value:'More than three'
            }

            ]

        },
        {
            field_type:'checklist',
            question:'Anything specific about the project',
            description:'other details',
            choices:[
            {
                label:'Both shower & tub faucets need installation',
                value:'Both shower & tub faucets need installation'
            },
            {
                label:"Single handle that controls both hot and cold water",
                value:"Single handle that controls both hot and cold water"
            },
            {
                label:"Separate handles to control hot and cold water",
                value:"Separate handles to control hot and cold water"
            },
            {
                label:"There is one or more jet shower system to be installed",
                value:'There is one or more jet shower system to be installed'
            },
            {
                can_describe:true
            }
            ]
        }
        ]
    };
};