module.exports = function(parentId) {
  return{
        name: 'Shower/tub Faucet Repair',
        route: 'shower-tub-faucet-repair',
        search_keywords: [ 'plumber','faucet repair', 'shower repair','shower faucet','fa','fau','fauc','fauce','faucet','tub faucet','tub','tub faucet repair','shower faucet repair','shower veleves','tub faucet','velves','faucet replacement','replacing shower','bathroom faucet','bathroom shwoer faucet','bath faucets','handyman','tub faucets','fix shower','repair bathroom','bathtub faucet','fauce','plumbing','plumber','old bathtub faucet','bathtub','old bathub faucet replacement','replacing bathtub','plumbing fixture','plumbing fixtures','leaky faucet','leaky bathtub','monitor','tub shower','cartridge','shower faucet replacement'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'fix your shower faucet',
        questions: [
        {
            field_type:'select',
            question:"What's the project?",
            description:'Type of project',
            choices:[
            {
                label:'Repair',
                value:'Repair'
            },
            {
                label:'Replace',
                value:'Replace'
            }
            ]
        },
        {


                field_type:"checklist",
                question: "What kind of problems do you have?",
                description: "Type of problems",
                choices:[
                {
                    label:'No repair, I need replacement',
                    value:'No repair, I need replacement'
                },
                {
                        label:"Clogged sink",
                        value:"Clogged sink"
                },
                {
                        label:"Shower or bathtub drains slowly",
                        value:"Shower or bathtub drains slowly"
                },
                {
                        label:"Shower handles are leaking",
                        valves:'Shower handles are leaking'
                },
                {

                        label:"Leaky or dripping faucet",
                        value:"Leaky or dripping faucet"
                },
                {

                        label:"Weak pressure from faucet",
                        value:"Weak pressure from faucet"
                },
                {

                        label:" Shower-tub or faucet problem hand-in-hand with plumbing problems",
                        value:"Shower-tub or faucet problem hand-in-hand with plumbing problems"
                },
                
                {
                        can_describe: true
                }



                ]
        },
        {
            field_type:'select',
            question:'How many shower faucet need work?',
            description:'Number of shower faucets that need work',
            choices:[
            {
                label:'One',
                value:'One'
            },
            {
                label:'Two',
                value:'Two'
            },
            {
                label:'Three',
                value:'Three'
            },
            {
                label:'More than three',
                value:'More than three'
            }
            ]
        },
        {
            field_type:'text',
            question:'Please describe the problem you are experiencing in a few words',
            description:'The problem'
        }
        ]
    };
};