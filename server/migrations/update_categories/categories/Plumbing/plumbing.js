module.exports = function(parentId) {
  return{
        name: 'General Plumbing',
        route: 'general-plumbing',
        search_keywords: [ 'Plumbing', 'installation sink','replace pip',' replace pips', 'Sink installation', 'sink repair','bathroom remodeling', 'dish washer installation','pl','plum','plumb','handyman','plumbing problems','general plumbing','plumbing help','bathroom','kitchen','clogged','clog'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'help you with your plumbing problems',
        questions: [
        {
                field_type:"checklist",
                question: "What kind of work do you need?",
                description: "Works needed by the customer",
                choices:[
                {
                        label:"Install",
                        value:"Install"
                },
                {
                        label:"Repair",
                        value:"Repair"
                },
                {
                        label:"Replace",
                        value:"Replace"
                },
                {
                        can_describe: true
                }



                ]
        },
        {
            field_type:'text',
            question:"What fixtures are you expriencing problem with? Describe your problem",
            description:'What fixtures'

        },
        
        {
                field_type:"select",
                question: "How long has this problem existed?",
                description: "The problem has been around for",
                choices:[
                {
                        label:"Just recently",
                        value:"Just recently"
                },
                {
                        label:"Over the past week",
                        value:"Over the past week"
                },
                {
                        label:"Periodically over the past month or longer",
                        value:"Periodically over the past month or longer"
                },

                ]
        },
        {
                field_type:"checklist",
                question: "Which room require plumbing work?",
                description: "Project is taking place in",
                choices:[
                {
                        label:"Bathroom",
                        value:"Bathroom"
                },
                {
                        label:"Kitchen",
                        value:"Kitchen"
                },
                {
                        label:"Laundry Room", 
                        value:"Laundry Room"
                },
                
                {
                        label:"Basement", 
                        value:"Basement"
                },
                {
                        label:"Entire building", 
                        value:"Entire building"
                },
                {
                        label:"Outdoors", 
                        value:"Outdoors"
                },
                {
                        can_describe: true
                },
                
                ]
        },
         {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants you to supply the parts and materials"
                },

                ]
        },
        
        {
                field_type:"select",
                question: "Is any part of the building flooded?",
                description: "If the building is flooded or not",
                choices:[
                {
                        label:"Yes",
                        value:"Yes"
                },
                {
                        label:"No",
                        value:"No"
                },
                ]
        },
        ]
};
};

        
