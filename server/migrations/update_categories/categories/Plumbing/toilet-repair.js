module.exports = function(parentId) {
  return {
        name: 'Toilet Repair',
        route: 'toilet-repair',
        search_keywords: [ 'plumber','toilet installation', 'toilet replacement','toilet','caroma toilet','toilet Repair','handyman','tank','toilet tank','leaks','running toilet','tank repair','problems','unclog toilet','valves','toilet flange ','toilet flange replacement'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'help you repair your toilet',
        questions: [
        {
            field_type:'checklist',
            question:'What kind of problem?',
            description:'The problem',
            choices:[
            {
                label:'Slugging flush',
                value:'Slugging flush'
            },
            {
                label:'Suction sounds in the tub and sink',
                value:'Suction sounds in the tub and sink'
            },
            {
                label:'Phantom flush',
                value:'Phantom flush'
            },
            {
                label:'Toilet valves have problem',
                valves:'Toilet valves have problem'
            },
            {
                label:'Tank leaks',
                value:'Tank leaks'
            },
            {
                label:'Toilet flange replacement',
                value:'Toilet flange replacement'
            },
            {
                label:'Toilet replacement',
                value:'Toilet replacement'
            },
            {
                label:'Flush handle is broken',
                value:'Flush handle is broken'
            },
            {
                label:'Change toilet seat',
                value:'Change toilet seat'
            },
            {
                label:'Bowl water level drops',
                value:'Bowl water level drops'
            },
            {
                can_describe:true
            }

            ]
        },
        ]
        };
    };