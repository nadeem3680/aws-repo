module.exports = function(parentId) {
  return {
        name: 'Toilet Installation or Replacement',
        route: 'toilet-installation',
        search_keywords: [ 'plumber','toilet installation', 'toilet replacement','toilet','install toilet','replace toilet','toilet flange','bathroom','handyman','plumbing','contractor','repairman','repair man'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 2,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'help you install your toilet',
        questions: [
        {
            field_type:'select',
            question:'What is the project?',
            description:'Type of project',
            choices:[
            {
                label:'Replace',
                value:'Replace'
            },
            {
                label:'Install',
                value:'Install'
            }
            ]
        },
        {
                field_type:"select",
                question: "How many showers or bathtubs need installation?",
                description: "Number of showers or bathtubs that need installation",
                choices:[
                {
                        label:"One",
                        value:"One"
                },
                {
                        label:"Two",
                        value:"Two"
                },
                {
                        label:"Three",
                        value:"Three"
                },
                {
                        label:"More than three",
                        value:"More than three"
                },



                ]
        },
        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No, I want the professional to supply the parts and materials",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
         {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
    };
};