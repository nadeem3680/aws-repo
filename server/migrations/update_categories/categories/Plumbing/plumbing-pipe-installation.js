module.exports = function(parentId) {
  return {
        name: 'Pipe Installation',
        route: 'pipe-installation',
        search_keywords: [ 'plumber','pipe repair', 'plumbing pipe repair','replace pipe',' replace pipes','handyman','forzen pipes','leaking','leak','burst pipes','nosiy pipes','clogged drains','clogged','slow drain','odor','piping','plumbing fixtures','dishwasher','toilet','shower','bathtub','washing mashin','repair man','repairman','appliance','Refrigerator','shower base','bathroom','kitchen','emergency','home pipe repiar','residential pipe','sewer repair','pipe fitting','clamp','leak detection','roof leak','leaking','dripping','plumbing problem','stop leak','stop it leak repair','pipe installation','new pipe installation','home pipe replacement','sewer installation','pipe replacement','broken pipe','p','pi','pip'],
        parent: parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'plumber',
        actor_plural: 'plumbers',
        action: 'help you with your pipe installation',
        questions: [
        {
                field_type:"checklist",
                question: "Which appliances are affected?",
                description: "appliances that are affected",
                choices:[
                {
                        label:"Toilet",
                        value:"Toilet"
                },
                {
                        label:"Sink",
                        value:"Sink"
                },
                {
                        label:"Disposal",
                        value:"Disposal"
                },
                {
                        label:"Shower or bathtub",
                        value:"Shower or bathtub"
                },
                {
                        label:"Dishwasher",
                        value:"Dishwasher"
                },
                {
                        label:"Refrigerator",
                        value:"Refrigerator"
                },
                {
                        label:"Washing machine",
                        value:"Washing machine"
                },
                {
                        can_describe: true

                }

                ]
        },
        {
                field_type:"checklist",
                question: "What material are the pipes you need?",
                description: "Type of pipes",
                choices:[
                {
                        label:"Copper",
                        value:"Copper"
                },
                {
                        label:"Iron",
                        value:"Iron"
                },
                {
                        label:"Steel",
                        value:"Steel"
                },
                {
                        label:"PVC",
                        value:"PVC"
                },
                {
                        label:"CPVC",
                        value:"CPVC"
                },
                {
                        label:"PEX",
                        value:"PEX"
                },
                {
                        label:"As recommended by professional",
                        value:"As recommended by professional"
                },
                
                {
                        can_describe: true
                        
                }

                ]
        },
        {
                field_type:"select",
                question: "Will you supply all the necessary parts and materials?",
                description: "Customer provides materials and parts or not",
                choices:[
                {
                        label:"Yes, I will provide the materials and parts",
                        value:"Yes, customer provides all the materials and parts"
                },
                {
                        label:"Yes, but I need the professional advice",
                        value:"Yes, but customer needs your advice"
                },
                {
                        label:"No",
                        value:"No, customer wants to supply the parts and materials"
                },

                ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
};
};