'use strict';

exports.isNewRoot=false;

exports.root = {
  name:"Craftswork",
  parent:null
};

exports.children = [
  require('./door-installation'),
  require('./door-repair'),
  require('./window-repair'),
  require('./fine-woodworking'),
  require('./finish-carpentry-trim-moulding'),
  require('./framing-carpentry'),
  require('./window-repair')
];