odule.exports = function(parentId) {
  return{
  name: 'Window Installation',
  route: 'window-installation',
  search_keywords: ['window','window install', 'window glass', 'sliding window','win','wind','windo', 'casement window', 'eliptical window','bay window','home window installation','pella window','window replacement','home winow','double window','window casement','casement','window trim','door trim','vinyl window','vinyl window installation','house windows','windows for house','door','door replacement','glass window','window glass'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'Crafts-person',
        actor_plural: 'Craftspeople',
        action: 'repair or install your windows',
        questions:  [
        {
          field_type:"checklist",
          question:"What kind of window need to be installed?",
          description:"The kind of window to be installed",
          choices:[
            {
              label:"Fixed, does not open",
              value:"Fixed, does not open"
            },
            {
              label:"Double hung, slides up and down",
              value:"Double hung"
            },
            {
              label:"Casement, pushed open on a hinge",
              value:"Casement",

            },
            {
              label:"Sliding",
              value:"Sliding"
            },
            {
              label:"Arched or elliptical",
              value:"Arched or elliptical"
            },
            {
              label:"As recommended by the professional",
              value:"As recommended by the professional"
            },
            {
              can_describe:true
            }
          ]
        },
        {
          field_type:"select",
          question:"How many windows?",
          description:"Number of windows",
          choices:[
          {
            label:"One",
            value:"One"
          },
          {
            label:"Two",
            value:"Two"
          },
          {
            label:"Three",
            value:"Three"
          },
          {
            label:"More than four",
            value:"More than four"
          }
          ]
        },
        {
          field_type:"select",
          question:"Are you replacing existing windows?",
          description:"Type of job",
          choices:[
          {
            label:"Yes, I'm replacing existing window",
            value:"Customer is replacing existing window"
          },
          {
            label:"Yes, I'm replacing existing window but of a new size",
            value:"Customer is replacing existing window but of a different size"
          },
          {
            label:"No, the opening needs to be created ",
            value:"An opening needs to be created"
          },
          {
            label:"No, but the opening is already there",
            value:"Opening(s) are there which need new window(s)",

          },

          ]
        },
        {
          field_type:"select",
          question:"Will you supply the window(s)?",
          description:"Who is supplying the window(s)",
          choices:[
          {
            label:"Yes, I will supply the window(s)",
            value:"Customer wants to supply the window(s)",

          },
          {
            label:"No, I want the contractor buy the window(s)",
            value:"Customer wants you to buy the windows"
          },



          ]
        },
        {
          field_type:"checklist",
          question:"What floor will the windows be on?",
          description:"The floors where the windows will be on ",
          choices:[
          {
            label:"First floor",
            value:"First floor"
          },
          {
            label:"First floor but a ladder is needed",
            value:"First floor but a ladder is needed"
          },
          {
            label:"Second floor",
            value:"Second floor"
          },
          {
            label:"Second floor,and no ladder is needed",
            value:"Second floor,is accessible from lower floor or balcony and no ladder is needed"
          },
          {
            label:"Higher than second floor",
            value:"Higher than second floor"
          },
          {
            label:"Ground level/basement",
            value:"Ground level/basement"
          }

          ]
        },
        {
                field_type:"select",
                question: "What type of property do you have?",
                description: "Type of property",
                choices:[
                {
                        label:"Home",
                        value:"Home"
                },
                {
                        label:"Multi unit building",
                        value:"Multi unit building"
                },
                {
                        label:"Office/business",
                        value:"Office/business"
                },
                {
                        label:"Commercial",
                        value:"Commercial"
                },


                ]
        }
        ]
    };
};

