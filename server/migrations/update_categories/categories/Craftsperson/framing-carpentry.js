module.exports = function(parentId) {
  return{
    name: 'Framing Carpentry',
    route: 'framing-carpentry',
    search_keywords: ['woodworking','carpentry', 'fine woodworking','frame','framing','framing carpentry','roof framing','home renovation','general contractor','framed wall','trim carpentry','home construction','floor plans','home desings','modular homes','home builders','home builder','carpenter','framing house','residential construction','home renovation project','home renovation','framing a basement','basement framing','construction carpentry','wood framing','framing doors','framing companies','framing floors','framing an addition','framing costs','framing project'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 4,
    actor: 'carpenter',
    actor_plural: 'carpenters',
    action: 'help you with your carpentry needs',
    questions:  [
      {
        field_type:'select',
        question:'What do you need framed?',
        description:"What to be framed",
        choices:[
          {
            label:"Entire house",
            value:'Entire house'
          },
          {
            label:'Wall To an existing room',
            value:'Wall to an existing room'
          },
          {
            label:'New room',
            value:'New room'
          },
          {
            label:'Roof',
            value:'Roof'
          },
          {
            label:'Re-frame a wall to accommodate new door or window',
            value:'Re-frame a wall to accommodate new door or window'
          },
          {
            label:'Shed or standalone',
            value:'Shed or standalone'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:"select",
        question:'Do you already have plans or design?',
        description:'design and plans',
        choices:[
          {
            label:"Already have plans",
            value:"Customer has a plan or design"
          },
          {
            label:"No existing plans, but I have an idea what I want",
            value:"No existing plans, but customer has an idea what she/he wants"
          },
          {
            label:"No existing plans, need design suggestions",
            value:"No existing plans, customer needs design suggestions"
          }

        ]
      },
      {
        field_type:"text",
        question:"What is the approximate square footage of your property and other details?",
        description:"The approximate square footage of the property and other details"
      },
      {
        field_type:'select',
        question:'Will you need to run plumbing or electrical lines through this wall?',
        description:'Will you need to run plumbing or electrical line through this wall',
        choices:[
          {
            label:'Yes, I will need plumbing or electrical line installed',
            value:'Yes, Customer needs plumbing or electrical line installed'
          },
          {
            label:'No',
            value:'No'
          },
          {
            label:'As recommended by professional',
            value:'As recommended by professional'
          }
        ]

      },
      {
        field_type:'select',
        question:"Do you need drywall applied to the framing",
        description:"Drywall to be applied to the framing",
        choices:[
          {
            label:'Yes, I need drywall applied',
            value:"Yes, Customer needs drywall applied"
          },
          {
            label: "No, I don't need drywall applied",
            value: "No, Customer doesn't need drywall applied"
          },
          {
            label:'As recommended by professional',
            value:"As recommended by professional"
          }

        ]
      }
    ]
  };
};
