module.exports = function(parentId) {
  return{
  name: 'Finish Carpentry Trim & Crown Moulding',
  route: 'finish-carpentry-trim-moulding ',
  search_keywords: ['woodworking','carpentry', 'fine woodworking','Trim','Carpentry crown moulding','door casing','wainscoting', 'chair rail', 'picture rail','wood framing','molding','trim molding','trim moulding','moulding trim','carpentry','wood trim','door trim','trim installation','door moulding','door molding','window molding','wall moulding','wall molding','wood molding','wood moulding','interior trim molding','trim and mouldings','wainscoting trim','garage door trim','crown moulding','crown molding','casing','door casing','window casing'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'carpenter',
        actor_plural: 'carpenters',
        action: 'help you with your woodworking needs',
        questions:  [
        {
          field_type:'checklist',
          question:'What kind of finish carpentry do you need?',
          description:'Type of work needed',
          choices:[
          {
            label:"Crown moulding",
            value:"Crown moulding"
          },
          {
            label:"Casing(door & window)",
            value:"Casing(door & window)"
          },
          {
              label:'Baseboards',
              value:"Baseboards"
          },
          {
            label:'Wainscoting (lower area of the wall)',
            value:'Wainscoting(lower area of the wall)'
          },
          {
            label:'Chair rail,picture rail or other wall moulding',
            value:'Chair rail,picture rail or other wall moulding'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:'What type of wall are you placing this moulding or trim on?',
          description:'Type of wall that moulding or trim will be placed on',
          choices:[
          {
            label:'Drywall',
            value:'Drywall'
          },
          {
            label:'Plaster',
            value:'Plaster'
          },
          {
            label:'Brick',
            value:'Brick'
          },
          {
            label:'Wood',
            value:'Wood'
          },
          {
            can_describe:true
          }
          ]
        },

      {
        field_type: "select",
        question: "Will you supply the wood?",
        description: "Who supplies the wood",
        choices: [
          {
            label: "Yes, I will provide the materials",
            value: "Customer will provide the materials "
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the materials",
            value: "Customer wants you to supply the materials"
          }
        ]
      },
      {
        field_type:"select",
        question:"Will you need help designing the moulding or trim?",
        description:"Will you need to help the customer with moulding or trim design",
        choices:[
        {
          label:'No, I just need installation',
          value:"No, Customer only needs installation"
        },
        {
          label:"Yes, I need help with design",
          value:'Yes, Customer needs help with design'
        }
        ]
      },
      {
        field_type:'select',
        question:"What moulding or trim material do you want?",
        description:'Moulding or trim material customer wants',
        choices:[
        {
          label:'Solid wood',
          value:'Solid wood'
        },
        {
          label:'Engineered wood',
          value:'Engineered wood'
        },
        {
          label:'Vinyl',
          value:'Vinyl'
        },
        {
          label:'As recommended by professional',
          value:'As recommended by professional'
        },
        {
          can_describe:true
        }
        ]
      },
      {
        field_type:"text",
        question:"What is the approximate length of moulding or trim you need installed?",
        description:"The approximate length of moulding or trim customer need installed"
    }
    ]
  };
};

