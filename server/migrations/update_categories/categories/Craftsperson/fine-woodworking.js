module.exports = function(parentId) {
  return{
  name: 'Fine Woodworking',
  route: 'fine-woodworking',
  search_keywords: ['woodworking','carpentry', 'fine woodworking','cabinet','woodsmith','woodwork','woodcraft' ],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 3,
        actor: 'carpenter',
        actor_plural: 'carpenters',
        action: 'help you with your woodworking needs',
        questions:  [
        {
          field_type:"checklist",
          question:'What kind of work do you need?',
          description:'Type of project',
          chioces:[
          {
            label:'New woodworking piece',
            value:"New woodworking piece"
          },
          {
            label:'Repair an existing piece',
            value:'Repair an existing piece'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:"checklist",
          question:'What do you want built or repaired?',
          description:"What to build or repair",
          choices:[
          {
            label:"Crown mouldings",
            value:'Crown mouldings'
          },
          {
            label:"mouldings",
            value:'mouldings'
          },
          {
            label:'Cabinets',
            value:'Cabinets'
          },
          {
            label:'Window or door casing',
            value:'Window or door casing'
          },
          {
            label:'Wainscoting',
            value:'Wainscoting'
          },
          {
            label:'Fireplace surrounds',
            value:'Fireplace surrounds'
          },
          {
            label:'Furniture and architectural millwork',
            value:'Furniture and architectural millwork'
          },
          {
            can_describe:true
          }

          ]
        },
        {
          field_type:'checklist',
          question:"What repair services do you need?",
          description:"Repair services needed",
          choices:[
          {
            label:'No repairs needed, this is a new build',
            value:'No repairs needed, this is a new build'
          },
          {
            label:'Fix small crack(s)',
            value:"Fix small crack(s)"
          },
          {
            label:'Fix large crack(s)',
            value:'Fix large crack(s)'
          },
          {
            label:"Refinish",
            value:"Refinish"
          },
          {
            label:'Repair rotten wood',
            value:'Repair rotten wood'
          },
          {
            label:'Replace missing or damaged piece',
            value:'Replace missing or damaged piece'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'text',
          question:"Please describe your project in a few words",
          description:"Project description"
        },
        {
            field_type:"select",
            question:'Do you already have plans or design?',
            description:'design and plans',
            choices:[
            {
                label:"No existing plans, but I have an idea what I want",
                value:"No existing plans, but customer has an idea what she/he wants"
            },
            {
                label:"No existing plans, need design suggestions",
                value:"No existing plans, customer needs design suggestions"
            },
            {
                label:"Already have plans",
                value:"Customer has a plan or design"
            }
            ]
        },
        {
          field_type:'checklist',
          question:"What kind of wood will the project be made of?",
          description:'Type of wood customer wants the project to be made of',
          choices:[
          {
            label:'Maple',
            value:"Maple"
          },
          {
            label:'Cedar',
            value:"Cedar"
          },
          {
            label:"Hickory",
            value:"Hickory"
          },
          {
            label:"Cherry",
            value:'Cherry'
          },
          {
            label:'Walnut',
            value:'Walnut'
          },
          {
            label:'As recommended by professional',
            value:'As recommended by professional'
          },
          {
            can_describe:true
          }
          ]
        },
         {
        field_type: "select",
        question: "Will you supply the wood?",
        description: "Who supplies the wood",
        choices: [
          {
            label: "Yes, I will provide the materials",
            value: "Customer will provide the materials "
          },
          {
            label: "Yes, but I will need guidance from the professional",
            value: "Customer will provide material but needs your guidance"
          },
          {
            label: "No, I want the professional to supply the materials",
            value: "Customer wants you to supply the materials"
          }
        ]
      },
      {
        field_type:"select",
        question:"What kind of finishing would you like on the piece?",
        description:"Finishing to be used",
        choices:[
        {
          label:'Paint',
          value:"Paint"
        },
        {
          label:'Stain',
          value:'Stain'
        },
        {
          label:'Oil',
          value:'Oil'
        },
        {
          label:'No finishing needed',
          value:'No finishing needed'
        }
        ]
      }
      ]
    };
  };
