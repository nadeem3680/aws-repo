// Machinery?
module.exports = function(parentId) {
  return{
    name: 'Backhoe & Bobcat Services',
    route: 'backhoe-bobcat-services',
    search_keywords: ['bobcat services','bobcat','Backhoe','backhoe services','back','b','bob','bac','mass excavation','footing','Trenching','footing','concrete removal','dirt removal','clearing job site','junk removal','demolition','grading','swimming pool','drilling','hauling','boring','pool removal'],
    parent:  parentId,
    travel_types: ['tocustomer'],
    scheduling_type: 'appointment',
    credits_required: 3,
    actor: 'demolition & excavation expert',
    actor_plural: 'demolition & excavation experts',
    action: 'help you demolish and excavate',
    questions:  [
      {
        field_type:"checklist",
        question:"What kind of project?",
        description:"Type of project",
        choices:[
          {
            label:'Dirt removal',
            value:'Dirt removal'
          },
          {
            label:'Concrete removal',
            value:'Concrete removal'
          },
          {
            label:'Grading',
            value:'Grading'
          },
          {
            label:'Footing',
            value:'Footing'
          },
          {
            label:'Trenching',
            value:'Trenching'
          },
          {
            label:'Mass excavation',
            value:'Mass excavation'
          },
          {
            label:'Clearing',
            value:'Clearing'
          },
          {
            label:'Demolition',
            value:'Demolition'
          },
          {
            can_describe:true
          }
        ]
      },
      {
        field_type:'checklist',
        question:'Do you need anything else?',
        description:"Anything else customer may need",
        choices:[
          {
            label:'Nothing else',
            value:'Nothing else'
          },
          {
            label:'Drilling/Boring',
            value:'Drilling/Boring'
          },
          {
            label:'Hauling/removal',
            value:'Hauling/removal'
          },
          {
            label:'Pool Removal',
            value:'Pool Removal'
          },
          {
            can_describe:true
          }

        ]
      },
      {
        field_type:'checklist',
        question:'Anything else specific?',
        description:'Anything else specific',
        choices:[

          {
            label:'Septic(new)',
            value:'Septic(new)'
          },
          {
            label:'Septic(upgrade)',
            value:'Septic(upgrade)'
          },
          {
            label:"Pool",
            value:"Pool"
          },
          {
            label:"There is a fire hydrant on my property",
            value:'There is a fire hydrant on the property'
          },

          {
            label:'There are city electrical wirings within 5 feet of the property',
            value:'There are city electrical wirings within 5 feet of the property'
          },
          {
            label:"I want environmental clean-up",
            value:'Customer wants environmental clean-up'
          }

        ]
      }
    ]
  };
};

        