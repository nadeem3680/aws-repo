module.exports = function(parentId) {
  return{
  name: 'Demolition',
  route: 'demolition',
  search_keywords: ['demolition','demolition plan','demolition worker','swimming pool','demolition company','demolition waste','Residential demolition'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'demolition expert',
        actor_plural: 'demolition experts',
        action: 'help you demolish and excavate',
        questions:  [
       
        {
          field_type:'checklist',
          question:'Building type',
          description:"Building type",
          choices:[
          {
            label:'House',
            value:'House'
          },
          {
            label:'Apartment',
            value:'Apartment'
          },
          {
            label:'Commercial building/complex',
            value:'Commercial building/complex'
          },
          {
            label:'Garage',
            value:'Garage'
          },
          {
            label:'Outbuilding',
            value:'Outbuilding'
          },
          {
            label:'Deck',
            value:'Deck'
          },
          {
            can_describe:true
          }
          ]
        },
        {
          field_type:'checklist',
          question:'Project type',
          description:'Project type',
          choices:[
          {
            label:'Total',
            value:'Total'
          },
          {
            label:'Partial',
            value:'Partial'
          },
          {
            label:'Strip out interior',
            value:'Strip out interior'
          },
          {
            label:'Pool',
            value:'Pool'
          },
          {
            label:'Environmental clean-up',
            value:'Environmental clean-up'
          },
          {
            can_describe:true
          }
          ]

        }
        ]
      };
    };