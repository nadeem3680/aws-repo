module.exports = function(parentId) {
  return{
  name: 'Demolition & Excavation',
  route: 'demolition-excavation',
  search_keywords: ['demolition','excavation','demolition plan','demolition worker','swimming pool','demolition company','demolition waste','Residential demolition'],
        parent:  parentId,
        travel_types: ['tocustomer'],
        scheduling_type: 'appointment',
        credits_required: 4,
        actor: 'demolition expert',
        actor_plural: 'demolition experts',
        action: 'help you demolish and excavate',
        questions:  [
        
        {
          field_type:'text',
          question:"What is the approximate square footage of the property?",
          description:"The approximate square footage of the property"
        },
        {
          field_type:"select",
          question:'What kind of property?',
          description:'What kind of property',
          choices:[
          { 
            label:'Residential',
            value:'Residential'
          },
          {
            label:'Commercial',
            value:'Commercial'
          }
          ]
        },
        {
          field_type:'checklist',
          question:'Which of the following applies to your property',
          description:"Other thing you may need to know, if there is any",
          required:false,
          choices:[
          {
            label:'Septic(new)',
            value:'Septic(new)'
          },
          {
            label:'Septic(upgrade)',
            value:'Septic(upgrade)'
          },
          {
            label:"Pool",
            value:"Pool"
          },
          {
            label:"There is a fire hydrant on my property",
            value:'There is a fire hydrant on the property'
          },
          
          {
            label:'There are city electrical wirings within 5 feet of the property',
            value:'There are city electrical wirings within 5 feet of the property'
          },
          {
            label:"I want environmental clean-up",
            value:'Customer wants environmental clean-up'
          }

          ]
        }
        ]
      };
    };
