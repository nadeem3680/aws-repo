'use strict';

exports.isNewRoot = true;

exports.root = {
	name:"Demolition & Excavation",
	parent:null
};

exports.children = [
require('./backhoe-bobcat-services'),
require('./demolition'),
require('./demolition-excavation')


]