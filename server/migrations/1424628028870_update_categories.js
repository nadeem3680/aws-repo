'use strict';

var models = require('./config/db').model;
var Category = models('Category');
var _ = require('lodash');
var Promise = require('bluebird');
var path = require('path');

var sys = require('sys');
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stdout) }

var dbName = process.env.MONGO_DB_NAME || 'tasky-dev';


module.exports.up = function(done) {
  // Backup existing categories collection
  var pathToBackup =path.join(__dirname, '/update_categories/backup');
  console.log('Backing up to %s/%s', pathToBackup, dbName);
  exec('mongodump --out ' + pathToBackup + ' --collection categories --db ' + dbName, puts);

  // Search given search term
  var categories = [
    require('./update_categories/categories/Appliance'),
    require('./update_categories/categories/Cleaning'),
    require('./update_categories/categories/Concrete Contractor'),
    require('./update_categories/categories/Craftsperson'),
    require('./update_categories/categories/Deck contracting'),
    require('./update_categories/categories/Demolition'),
    require('./update_categories/categories/Electrical'),
    require('./update_categories/categories/Floor'),
    require('./update_categories/categories/Furnance & heating system'),
    require('./update_categories/categories/Garage door'),
    require('./update_categories/categories/Gutter'),
    require('./update_categories/categories/Handyman'),
    require('./update_categories/categories/Interior Design'),
    require('./update_categories/categories/Landscaping'),
    require('./update_categories/categories/Locksmith'),
    require('./update_categories/categories/Moving'),
    require('./update_categories/categories/Painting'),
    require('./update_categories/categories/Pest Removal'),
    require('./update_categories/categories/Plumbing'),
    require('./update_categories/categories/Roofing'),
    require('./update_categories/categories/Siding')
  ];

  var outStandingOperations = [];

  for (var i = 0; i < categories.length; i++) {
    var category = categories[i];

    outStandingOperations.push(
      Category.findOneAsync({name: category.root.name }).bind(category)
        .then(function (parentCat) {
          // If the category exists, then just skip the creation
          if (parentCat) {
            console.log("Updating root category [", this.root.name, "]");
            return parentCat;
          }
          // Create the category
          // Thanks to .bind(), the context (this) is the root category
          console.log("Creating root category [", this.root.name, "]");
          return Category.createAsync(this.root);
        })
        .error(function (err) {
          console.log('Unable to retrieve or create Category [', this.root.name , ']');
        })
        .then(function (parentCat) {
          // Saving reference to root instance
          this.root = parentCat;

          // Attach parentId to root category
          return _.map(this.children, function(child) {
            return child(parentCat._id);
          });
        })
        .then(function (children) {
          this.children = children;

          // Look for children categories to update
          var promiseArray = _.map(this.children, function (child) {
            // Find children
            return Category.findOneAsync({ route: child.route })
          });

          return Promise.all(promiseArray);
        })
        .error(function (err) {
          console.log("Failed to retrieve children categories [", err.message ,"]");
        })
        // Update children already there
        .then(function (childrenInDb) {
          // Get rid of children not found.
          childrenInDb = _.compact(childrenInDb);
          // for each found child, extract it from this.children and add the promise arrya
          var updateList = [];
          _.each(childrenInDb, function (child) {
            var routes = _.pluck(this.children, 'route');

            var pos = routes.indexOf(child.route);
            if (pos === -1) return;
            child = this.children.splice(pos, 1)[0];
            updateList.push(Category.updateAsync(
              { route: child.route },
              {
                route: child.route,
                name: child.name,
                actor: child.actor,
                actor_plural: child.actor_plural,
                parent: this.root._id,
                other_services: child.other_services,
                credits_required: child.credits_required,
                travel_types: child.travel_types,
                scheduling_type: child.scheduling_type,
                questions: child.questions,
                search_keywords: child.search_keywords
              },
              { upsert: true }
            ));
          }, this);
          // this children should only contain child to create now.
          console.log("Updating", updateList.length, "categories");
          return Promise.all(updateList);
        })
        .error(function (err) {
          console.log("Unable to update existing children [", err.message, "]");
        })
        // Create child left
        .then(function (childrenUpdated) {
          console.log("Creating", this.children.length, "categories");
          return Category.createAsync(this.children);
        })
        .error(function (err) {
          console.log("Unable to create children [", err.message, "]");
          done();
        })
    );
  }

  Promise.all(outStandingOperations).then(function(){
    console.log("Categories updated successfully");
    done();
  })
};

module.exports.down = function(done) {
  var pathToBackup = path.join(__dirname, '/update_categories/backup/', dbName, 'categories.bson');
  Category.removeAsync({}).then(function(args){
    console.log("Removed", args[0], "categories.");
    exec('mongorestore --collection categories --db ' + dbName + ' ' + pathToBackup, puts);
    done();
  });
};
