'use strict';

var models = require('./config/db').model;
var Quote = models('Quote');
var Request = models('Request');
var Promise = require('bluebird');

// Since we added message notifiction badge (number of new messages), the messages already sent in the db are marked as
// new messages (because last_message_seen is not set)
module.exports.up = function(done) {
  Quote.findAsync({})
    .then(function(quotes) {
      var promises = [];
      quotes.forEach(function(q) {
        var quotePromise = new Promise(function(resolve, reject) {
          // It's pretty annoying to get the request user ID via the Request
          // TODO: ADD sent_to field on quote model
          Request.findByIdAsync(q.request).bind({ quote: q })
            .then(function(request) {
              var q = this.quote;
              var customerId = request.requested_by;
              var proId = q.from;
              var lastMessageID = q.messages[q.messages.length - 1]._id;

              q.last_message_seen = {};
              q.last_message_seen[customerId] = q.last_message_seen[proId] = lastMessageID;
              q.markModified('last_message_seen');
              return q.saveAsync();
            })
            .then(function() {
              done()
            })
            .error(reject)
            .catch(reject);
        });

        promises.push(quotePromise);
      });

      Promise.all(promises).then(done, console.error);
    });
};

module.exports.down = function(done) {
  // Not sure how to do down for this one
  throw Error('Not Implemented');
};